/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-03-19 18:06:22
 * @LastEditTime : 2024-03-19 19:14:57
 */
import fs from 'node:fs'
import path from 'node:path'
import picocolors from 'picocolors'

const { green } = picocolors

function readDir(dirPath, parentName = '') {
  const stats = fs.statSync(dirPath)
  const name = path.basename(dirPath)
  const fullName = parentName ? `${parentName}/${name}` : name
  const node = { name: parentName ? fullName : '' }

  if (stats.isDirectory()) {
    node.children = fs.readdirSync(dirPath).map(child =>
      readDir(path.join(dirPath, child), fullName))
  }

  return node
}

const structure = JSON.stringify(readDir('./main-app/views'), null, 2)
const structure2 = JSON.stringify(readDir('./micro-app/views'), null, 2)

const floder = {
  'main-app': JSON.parse(structure.replaceAll('views/', '')).children,
  'micro-app': JSON.parse(structure2.replaceAll('views/', '')).children,
}

fs.writeFile('main-app/folder.json', JSON.stringify(floder), 'utf8', (err) => {
  if (err) { throw err }
  console.info(green('Iking-Admin: [文件已更新! 请刷新页面后查看]'))
})
