/*
 * @Author       : wfl
 * @LastEditors: wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-02-29 14:41:18
 * @LastEditTime: 2024-05-09 20:19:12
 */
import fs from 'node:fs'
import path from 'node:path'
import { defineConfig, loadEnv } from 'vite'
import dayjs from 'dayjs'
import { replaceIconfont } from './vite/plugins/replace-iconfont'
import logger from './vite/plugins/logger'
import pkg from './package.json'
import createVitePlugins from './vite/plugins'

// https://vitejs.dev/config/
export default async ({ mode, command }) => {
  replaceIconfont()
  const env = loadEnv(mode, './env')
  // 全局 scss 资源
  const scssResources = []
  fs.readdirSync('main-app/assets/styles/resources').forEach((dirname) => {
    if (fs.statSync(`main-app/assets/styles/resources/${dirname}`).isFile()) {
      scssResources.push(`@use "main-app/assets/styles/resources/${dirname}" as *;`)
    }
  })
  return defineConfig({
    base: './',
    envDir: './env',
    // 开发服务器选项 https://cn.vitejs.dev/config/#server-options
    server: {
      open: false,
      port: 9000,
      host: true,
      proxy: {
        '/api': {
          target: env.VITE_APP_PROXY_URL,
          changeOrigin: command === 'serve' && env.VITE_OPEN_PROXY === 'true',
          rewrite: path => path.replace(/^\/api/, ''),
        },
        '/ws': {
          target: env.VITE_APP_SSOCKET_PROXY_URL,
          changeOrigin: command === 'serve' && env.VITE_OPEN_PROXY === 'true',
          rewrite: path => path.replace(/^\/ws/, ''),
        },
        '/jmreport': {
          target: 'http://localhost:8080',
          changeOrigin: true,
        },
      },
    },
    customLogger: logger,
    logLevel: 'error',
    // 构建选项 https://cn.vitejs.dev/config/#server-fsserve-root
    build: {
      outDir: mode === 'production' ? 'dist' : `dist-${mode}`,
      sourcemap: env.VITE_BUILD_SOURCEMAP === 'true',
      // 启用/禁用 gzip 压缩大小报告;压缩大型输出文件可能会很慢，禁用该功能会提高项目的构建性能
      reportCompressedSize: false,
      chunkSizeWarningLimit: 1000,
      terserOptions: {
        compress: {
          // 关闭对eval的警告信息
        },
      },
      rollupOptions: {
        onwarn() {},
        output: {
          chunkFileNames: 'ik-[name]-[hash].js',
          // intro: '/* eslint-disable no-eval */',
          manualChunks: {
            'monaco-editor': ['monaco-editor'],
            // 'tinymce': ['tinymce'],
            'element-plus': ['element-plus'],
            'iking-form-pro': ['iking-form-pro'],
            'iking-web-ui-pro': ['iking-web-ui-pro'],
          },
        },
      },
    },
    define: {
      __SYSTEM_INFO__: JSON.stringify({
        pkg: {
          version: pkg.version,
          dependencies: pkg.dependencies,
          devDependencies: pkg.devDependencies,
        },
        lastBuildTime: dayjs().format('YYYY-MM-DD HH:mm:ss'),
      }),
    },
    optimizeDeps: {
      include: ['vue', 'element-plus', 'pinia', 'axios', 'lodash-es', 'iking-form-pro', 'iking-web-ui-pro', '@vueuse/core', 'sortablejs', 'vuedraggable', '@element-plus/icons-vue', 'unocss', 'iking-slider-valid', 'pinyin-pro', '@imengyu/vue3-context-menu', 'iking-utils-pro', '@unocss/preset-mini/utils', 'scule', 'path-to-regexp', 'dayjs', 'mitt', 'nprogress', 'postcss-scss', 'splitpanes'],
    },
    plugins: createVitePlugins(env, command === 'build', mode),
    resolve: {
      alias: [
        { find: '@main', replacement: path.resolve(__dirname, './main-app/') },
        { find: '@micro', replacement: path.resolve(__dirname, './micro-app/') },
        { find: '#', replacement: path.resolve(__dirname, './types/') },
        { find: '@t', replacement: path.resolve(__dirname, './types/') },
        { find: '@as', replacement: path.resolve(__dirname, './main-app/assets/') },
        { find: '@e', replacement: path.resolve(__dirname, './main-app/enums/') },
        { find: '@api', replacement: path.resolve(__dirname, './main-app/api/') },
        { find: '@apim', replacement: path.resolve(__dirname, './main-app/api/modules/') },
        { find: '@theme', replacement: path.resolve(__dirname, './themes/') },
        { find: '@sys', replacement: path.resolve(__dirname, './vite/') },
      ],
      extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.vue'],
    },
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: scssResources.join(''),
        },
      },
    },
  })
}
