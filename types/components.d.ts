/* eslint-disable */
/* prettier-ignore */
// @ts-nocheck
// Generated by unplugin-vue-components
// Read more: https://github.com/vuejs/core/pull/3399
export {}

declare module 'vue' {
  export interface GlobalComponents {
    ApprovalDetail: typeof import('./../main-app/components/BusinessCom/ApproveDetail/ApprovalDetail.vue')['default']
    ApprovalEndorse: typeof import('./../main-app/components/BusinessCom/ApproveDetail/ApprovalEndorse.vue')['default']
    Avatar_svg: typeof import('./../main-app/components/svg/avatar_svg.vue')['default']
    Close_svg: typeof import('./../main-app/components/svg/close_svg.vue')['default']
    CloseIcon: typeof import('./../main-app/components/BaseCom/IkBaseDialog/CloseIcon.vue')['default']
    Dep_svg: typeof import('./../main-app/components/svg/dep_svg.vue')['default']
    Ellipsis: typeof import('./../main-app/components/BaseCom/Ellipsis/Ellipsis.vue')['default']
    Empty_svg: typeof import('./../main-app/components/svg/empty_svg.vue')['default']
    Error_403: typeof import('./../main-app/components/svg/error_403.vue')['default']
    Error_404: typeof import('./../main-app/components/svg/error_404.vue')['default']
    Error_505: typeof import('./../main-app/components/svg/error_505.vue')['default']
    IconComponent: typeof import('./../main-app/components/BaseCom/IkOrgItems/Icon/IconComponent.vue')['default']
    IconType: typeof import('./../main-app/components/BaseCom/IkOrgItems/Icon/IconType.vue')['default']
    IkBadge: typeof import('./../main-app/layouts/ui-kit/IkBadge.vue')['default']
    IkBaseDialog: typeof import('./../main-app/components/BaseCom/IkBaseDialog/index.vue')['default']
    IkBasicsDetails: typeof import('./../main-app/components/BaseCom/IkBasicsDetails.vue')['default']
    IkBatchActionBar: typeof import('./../main-app/components/BaseCom/IkBatchActionBar/index.vue')['default']
    IkBtnAdd: typeof import('./../main-app/components/BaseCom/IkButton/IkBtnAdd.vue')['default']
    IkBtnContent: typeof import('./../main-app/components/BaseCom/IkBtnContent/index.tsx')['default']
    IkBtnExport: typeof import('./../main-app/components/BaseCom/IkButton/IkBtnExport.vue')['default']
    IkBtnImport: typeof import('./../main-app/components/BaseCom/IkButton/IkBtnImport.vue')['default']
    IkBtnLineHeight: typeof import('./../main-app/components/BaseCom/IkButton/IkBtnLineHeight.vue')['default']
    IkBtnRefresh: typeof import('./../main-app/components/BaseCom/IkButton/IkBtnRefresh.vue')['default']
    IkBtnSetting: typeof import('./../main-app/components/BaseCom/IkButton/IkBtnSetting.vue')['default']
    IkBtnSort: typeof import('./../main-app/components/BaseCom/IkButton/IkBtnSort.vue')['default']
    IkButton: typeof import('./../main-app/layouts/ui-kit/IkButton.vue')['default']
    IkCheckList: typeof import('./../main-app/layouts/ui-kit/IkCheckList.vue')['default']
    IkChip: typeof import('./../main-app/components/BaseCom/IkChip/index.vue')['default']
    IkCollapseCard: typeof import('./../main-app/components/BaseCom/IkCollapseCard/index.vue')['default']
    IkColorfulCard: typeof import('./../main-app/components/BaseCom/IkColorfulCard/index.vue')['default']
    IkColorText: typeof import('./../main-app/components/BaseCom/IkColorText.vue')['default']
    IkContent: typeof import('./../main-app/components/LayoutCom/IkContent/index.vue')['default']
    IkContentEmpty: typeof import('./../main-app/components/LayoutCom/IkContent/IkContentEmpty.vue')['default']
    IkCopyright: typeof import('./../main-app/components/BaseCom/IkCopyright/index.vue')['default']
    IkCurrencyInput: typeof import('./../main-app/components/BaseCom/IkCurrencyInput/index.vue')['default']
    IkDefaultEmpty: typeof import('./../main-app/components/LayoutCom/IkDefaultEmpty.vue')['default']
    IkDetail: typeof import('./../main-app/components/LayoutCom/IkDetail/index.vue')['default']
    IkDialog: typeof import('./../main-app/components/LayoutCom/IkPageFull/IkDialog.tsx')['default']
    IkDragList: typeof import('./../main-app/components/BaseCom/IkDragList/IkDragList.vue')['default']
    IkDragSort: typeof import('./../main-app/components/BaseCom/IkDragSort/index.vue')['default']
    IkDropdown: typeof import('./../main-app/layouts/ui-kit/IkDropdown.vue')['default']
    IkDropdownMenu: typeof import('./../main-app/layouts/ui-kit/IkDropdownMenu.vue')['default']
    IkEmpty: typeof import('./../main-app/components/BaseCom/IkEmpty/index.vue')['default']
    IkFileUpload: typeof import('./../main-app/components/BaseCom/IkFileUpload/index.vue')['default']
    IkFixedActionBar: typeof import('./../main-app/components/BaseCom/IkFixedActionBar/index.vue')['default']
    IkFlowIconPicker: typeof import('./../main-app/components/BaseCom/IkFlowIconPicker/index.vue')['default']
    IkFooter: typeof import('./../main-app/components/LayoutCom/IkFooter/index.vue')['default']
    IkFormItemTitle: typeof import('./../main-app/components/BaseCom/IkFormItemTitle.vue')['default']
    IkFormList: typeof import('./../main-app/components/BaseCom/IkForm/IkFormList.vue')['default']
    IkHlDialog: typeof import('./../main-app/layouts/ui-kit/IkHlDialog.vue')['default']
    IkI18nSelector: typeof import('./../main-app/components/BaseCom/IkI18nSelector/index.vue')['default']
    IkIconImage: typeof import('./../main-app/components/BaseCom/IkIconImage.vue')['default']
    IkIconPicker: typeof import('./../main-app/components/BaseCom/IkIconPicker/index.vue')['default']
    IkImagePreview: typeof import('./../main-app/components/BaseCom/IkImagePreview/index.vue')['default']
    IkImagesUpload: typeof import('./../main-app/components/BaseCom/IkImagesUpload/index.vue')['default']
    IkImageUpload: typeof import('./../main-app/components/BaseCom/IkImageUpload/index.vue')['default']
    IkInlineSwitch: typeof import('./../main-app/components/BaseCom/IkInlineSwitch.vue')['default']
    IkInput: typeof import('./../main-app/layouts/ui-kit/IkInput.vue')['default']
    IkItemCardList: typeof import('./../main-app/components/BaseCom/IkItemCardList/index.vue')['default']
    IkItemList: typeof import('./../main-app/components/BaseCom/IkItemList/index.vue')['default']
    IkKbd: typeof import('./../main-app/layouts/ui-kit/IkKbd.vue')['default']
    IkLinearIcon: typeof import('./../main-app/components/BaseCom/IkLinearIcon/index.vue')['default']
    IkMonacoEditor: typeof import('./../main-app/components/BaseCom/IkCodeEditor/IkMonacoEditor.vue')['default']
    IkNumberDuration: typeof import('./../main-app/components/BaseCom/IkNumberDuration.vue')['default']
    IkNumberUnit: typeof import('./../main-app/components/BaseCom/IkNumberUnit.vue')['default']
    IkOrgItems: typeof import('./../main-app/components/BaseCom/IkOrgItems/index.vue')['default']
    IkPage: typeof import('./../main-app/components/BaseCom/IkPage/index.vue')['default']
    IkPageFooter: typeof import('./../main-app/components/LayoutCom/IkPageFooter/index.vue')['default']
    IkPageFull: typeof import('./../main-app/components/LayoutCom/IkPageFull/index.vue')['default']
    IkPageHead: typeof import('./../main-app/components/LayoutCom/IkPageFull/IkPageHead.vue')['default']
    IkPageHeader: typeof import('./../main-app/components/LayoutCom/IkPageHeader/IkPageHeader.vue')['default']
    IkPageMain: typeof import('./../main-app/components/LayoutCom/IkPageMain/index.vue')['default']
    IkPageTable: typeof import('./../main-app/components/LayoutCom/IkPageFull/IkPageTable.vue')['default']
    IkPageTabs: typeof import('./../main-app/components/LayoutCom/IkPageFull/IkPageTabs.vue')['default']
    IkPanel: typeof import('./../main-app/components/LayoutCom/IkPanel/index.vue')['default']
    IkPcasCascader: typeof import('./../main-app/components/BusinessCom/IkPcasCascader/index.vue')['default']
    IkPersonModal: typeof import('./../main-app/components/BusinessCom/IkPerson/IkPersonModal.vue')['default']
    IkPersonSelect: typeof import('./../main-app/components/BusinessCom/IkPerson/IkPersonSelect.vue')['default']
    IkPreviewFile: typeof import('./../main-app/components/BaseCom/IkPreviewFile/IkPreviewFile.vue')['default']
    IkRadioGroup: typeof import('./../main-app/components/BaseCom/IkRadioGroup/index.vue')['default']
    IkSearch: typeof import('./../main-app/components/LayoutCom/IkSearch/index.vue')['default']
    IkSearchAuto: typeof import('./../main-app/components/LayoutCom/IkSearchAuto/index.tsx')['default']
    IkSelect: typeof import('./../main-app/layouts/ui-kit/IkSelect.vue')['default']
    IkSelectDep: typeof import('./../main-app/components/BusinessCom/IkBusinessSelect/IkSelectDep.vue')['default']
    IkSelectPost: typeof import('./../main-app/components/BusinessCom/IkBusinessSelect/IkSelectPost.vue')['default']
    IkSelectRank: typeof import('./../main-app/components/BusinessCom/IkBusinessSelect/IkSelectRank.vue')['default']
    IkSelectRole: typeof import('./../main-app/components/BusinessCom/IkBusinessSelect/IkSelectRole.vue')['default']
    IkSideText: typeof import('./../main-app/components/BaseCom/IkTableCellCom/IkSideText.vue')['default']
    IkSitePicker: typeof import('./../main-app/components/BaseCom/IkSitePicker/index.vue')['default']
    IkSlideover: typeof import('./../main-app/layouts/ui-kit/IkSlideover.vue')['default']
    IkSpinkitLoading: typeof import('./../main-app/components/BaseCom/IkSpinkitLoading/index.vue')['default']
    IkSplitPan: typeof import('./../main-app/components/LayoutCom/IkSplitPan/index.vue')['default']
    IkStatu: typeof import('./../main-app/components/BaseCom/IkStatu/index.vue')['default']
    IkStatuOver: typeof import('./../main-app/components/BaseCom/IkStatuOver/index.vue')['default']
    IkStorageBox: typeof import('./../main-app/components/BaseCom/IkStorageBox/index.vue')['default']
    IkSvgIcon: typeof import('./../main-app/components/BaseCom/IkSvgIcon/index.vue')['default']
    IkSvgIconProvider: typeof import('./../main-app/components/BaseCom/IkSvgIconProvider.vue')['default']
    IkSystemInfo: typeof import('./../main-app/components/BaseCom/IkSystemInfo/index.vue')['default']
    IkSysTooltip: typeof import('./../main-app/layouts/ui-kit/IkSysTooltip.vue')['default']
    IkTabList: typeof import('./../main-app/layouts/ui-kit/IkTabList.vue')['default']
    IkTextEllipsis: typeof import('./../main-app/components/BaseCom/IkTextEllipsis/index.vue')['default']
    IkTip: typeof import('./../main-app/components/BaseCom/IkTip/index.vue')['default']
    IkTitle: typeof import('./../main-app/components/BaseCom/IkTitle.vue')['default']
    IkToggle: typeof import('./../main-app/layouts/ui-kit/IkToggle.vue')['default']
    IkTooltip: typeof import('./../main-app/components/BaseCom/IkButton/IkTooltip.vue')['default']
    IkTree: typeof import('./../main-app/components/BaseCom/IkTree/IkTree.vue')['default']
    IkTrend: typeof import('./../main-app/components/BaseCom/IkTrend/index.vue')['default']
    IkUploadFileProvider: typeof import('./../main-app/components/BaseCom/IkUploadFileProvider.vue')['default']
    IkUploadPhoto: typeof import('./../main-app/components/BaseCom/IkUploadPhoto.vue')['default']
    'Index.vue.BASE': typeof import('./../main-app/components/BaseCom/IkTextEllipsis/index.vue.BASE.vue')['default']
    'Index.vue.LOCAL': typeof import('./../main-app/components/BaseCom/IkTextEllipsis/index.vue.LOCAL.vue')['default']
    'Index.vue.REMOTE': typeof import('./../main-app/components/BaseCom/IkTextEllipsis/index.vue.REMOTE.vue')['default']
    Jzsb: typeof import('./../main-app/components/svg/jzsb.vue')['default']
    Kong: typeof import('./../main-app/components/svg/kong.vue')['default']
    NotAllowed: typeof import('./../main-app/components/NotAllowed/index.vue')['default']
    Post_svg: typeof import('./../main-app/components/svg/post_svg.vue')['default']
    PrintView: typeof import('./../main-app/components/BusinessCom/ApproveDetail/PrintView.vue')['default']
    Role_svg: typeof import('./../main-app/components/svg/role_svg.vue')['default']
    RouterLink: typeof import('vue-router')['RouterLink']
    RouterView: typeof import('vue-router')['RouterView']
    Search_svg: typeof import('./../main-app/components/svg/search_svg.vue')['default']
    Sjjz: typeof import('./../main-app/components/svg/sjjz.vue')['default']
    Svg: typeof import('./../main-app/components/svg/svg.vue')['default']
    SystemInfo: typeof import('./../main-app/components/SystemInfo/index.vue')['default']
    Wlcw: typeof import('./../main-app/components/svg/wlcw.vue')['default']
    Zwjl: typeof import('./../main-app/components/svg/zwjl.vue')['default']
    Zwrc: typeof import('./../main-app/components/svg/zwrc.vue')['default']
    Zwsc: typeof import('./../main-app/components/svg/zwsc.vue')['default']
    Zwsj: typeof import('./../main-app/components/svg/zwsj.vue')['default']
    Zwxh: typeof import('./../main-app/components/svg/zwxh.vue')['default']
    Zwxx: typeof import('./../main-app/components/svg/zwxx.vue')['default']
  }
}
