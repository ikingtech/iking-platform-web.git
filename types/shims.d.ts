/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-03-14 11:00:23
 * @LastEditTime : 2024-04-28 12:04:31
 */
declare interface Window {
  webkitDevicePixelRatio: any
  mozDevicePixelRatio: any
}

declare const __SYSTEM_INFO__: {
  pkg: {
    version: Recordable<string>
    dependencies: Recordable<string>
    devDependencies: Recordable<string>
  }
  lastBuildTime: string
}

declare interface ApiModel<T = any> {
  code: number
  data: T
  msg: string
  pages: number
  success: boolean
  total: number
}

declare module 'iking-slider-valid'
declare module 'iking-web-ui-pro'
declare module 'iking-template-inset'
declare module 'iking-form-pro'
declare module 'splitpanes'
