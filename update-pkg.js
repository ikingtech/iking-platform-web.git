/*
 * @Author: wfl
 * @LastEditors: wfl
 * @description:
 * @updateInfo:
 * @Date: 2024-05-06 15:43:32
 * @LastEditTime: 2024-05-06 17:12:48
 */
// 自动获取最新版本iking-*包，并替换
import { exec } from 'node:child_process'
import fs from 'fs-extra'
import picocolors from 'picocolors'

const { green, blue, red } = picocolors
// 读取package.json
const packageJsonPath = './package.json'
let packageJsonContent
try {
  packageJsonContent = JSON.parse(fs.readFileSync(packageJsonPath, 'utf8'))
}
catch (error) {
  console.error(`无法读取${packageJsonPath}: ${error}`)
  // eslint-disable-next-line node/prefer-global/process
  process.exit(1)
}
// 获取iking-main-app当前版本
const currentVersion = packageJsonContent.devDependencies['iking-main-app']

// 查询npm获取最新版本
exec('pnpm view iking-main-app version', (error, stdout) => {
  if (error) {
    console.error(`查询iking-main-app最新版本失败: ${error}`)
    return
  }

  const latestVersion = stdout.trim()
  // 比较版本
  if (latestVersion.replace('^', '') > currentVersion.replace('^', '')) {
    console.info(red(`发现新版本：当前版本 ${currentVersion}，最新版本 ${green(latestVersion)}`))
    // 更新iking-main-app到最新版本
    console.info(green('开始更新iking-main-app...'))
    exec(`pnpm add iking-main-app@latest`, (error) => {
      if (error) {
        console.error('安装最新版iking-main-app失败:', error)
      }
      else {
        console.info(blue('iking-main-app已更新至最新版本'))
        console.info(blue('开始替换平台main-app'))
        replaceMainPlugin()
      }
    })
  }
  else {
    console.info(blue('iking-main-app已是最新版本'))
    console.info(blue('直接替换平台main-app'))
    replaceMainPlugin()
  }
})

async function replaceMainPlugin() {
  const srcPath = './node_modules/iking-main-app/pkg'
  const targetPath = './main-app'

  if (fs.existsSync(srcPath)) {
    console.info(green('开始更新main-app...'))

    // 如果目标目录存在，则清空目标目录
    if (fs.existsSync(targetPath) && !targetPath.endsWith('main-app')) {
      fs.emptyDirSync(targetPath)
    }
    // 复制文件
    await fs.copy(srcPath, targetPath)
    console.info(green('更新完成！'))
  }
  else {
    console.info(red('npm包[iking-main-app]不存在！请检查是否已install iking-main-app'))
  }
}
