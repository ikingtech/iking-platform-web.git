/*
 * @Author       : wfl
 * @LastEditors: zqf
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-06-27 16:40:54
 * @LastEditTime: 2024-06-12 10:21:22
 */
// const PLATFORM_TITLE = import.meta.env.VITE_APP_TITLE
// 报表导出
export const excludeUrl = [
  "/server/scaffold/project/download",
  "/server/scaffold/module/download",
];

interface ISystemConfig {
  // 登录页项目LOGO+标题
  project?: {
    // 标题
    title?: {
      // 标题内容
      value?: string;
      // 标题样式
      style?: string;
    };
    logo?: {
      // LOGO内容
      value?: string;
      darkValue: string;
      // LOGO内容
      style?: string;
    };
    // 位置 - 相对于左上角
    position?: {
      left?: number | string;
      top?: number | string;
    };
    // 版权
    copyright: {
      enable?: boolean;
      // 时间2022-2025
      date?: string;
      // 描述
      desc?: string;
    };
  };
  // 表格操作按钮类型
  tableIconType?: "icon" | "button";
  // IkPageFull 带斑马纹表格
  tableStripe?: boolean;
  // 表格操作更多显示文字
  tableMoreText?: string;
  remberRoute?: boolean;
  homePage: string; // 首页路由
  // 不需要的配置项设置为 '' 即可
  // 配置的静态文件全部放在public/system-config文件夹下
  loginComponentPath: string | null; // 登录页面vue文件地址
  loginOnlyBg: boolean; // 登录页是否只显示背景图不显示其它元素
  loginBg: string; // 登录页背景图
  loginBgDark: string; // 登录页背景图（暗色）
  loginTitle: string; // 登录页欢迎语 标题
  navBg?: string; // 顶部导航栏背景图
  navBgDark?: string; // 顶部导航栏背景图（暗色）
  navTitle?: string[]; // 顶部导航栏内容
  navTitleMargin?: number; // 顶部导航栏内容间隔

  sysTitle?: string; // 平台名称 - 菜单顶部显示的名称
  sysTitleEn?: string; // 平台名称 - 菜单顶部显示的英文名称
  sysIcon?: string; // 平台图标
  sysAlign?: "left" | "center" | "right"; // 平台名称 - 菜单顶部显示的位置
  twoLine?: boolean;

  // 平台顶部右侧操作图标是否显示
  // 未配置的默认显示
  tools: {
    org?: boolean; // 组织
    tenant: boolean; // 租户
    search: boolean; // 搜索
    notice: boolean; // 通知
    i18n: boolean; // 国际化
    fullScreen: boolean; // 全屏
    refresh: boolean; // 刷新
    theme: boolean; // 主题
    setting: boolean; // 设置
    userInfo: boolean; // 用户信息
  };
  // 登录页配置项
  loginTools: {
    forgetPassword: boolean; // 忘记密码
    register: boolean; // 注册
    remberMe: boolean; // 记住我
    // 数组形式传递，可设置多个登录方式；传[]则不显示登录方式
    loginType: Array<"password" | "phone" | "wx" | "dd">; // 登录方式 password: 密码登录 phone: 手机号登录 wx: 微信登录 dd: 钉钉登录
  };
  // 功能设计
  manageDesign?: {
    designMode: Array<"工程模式" | "功能模式" | "表单模式">;
  };
  // 用户管理新增 必填项配置
  // 未配置的默认 非必填
  role?: {
    fieldList?: Array<"name" | "dataScopeType" | "remark" | "dataScopeCodes">;
    extendTabs?: {
      name?: string;
      componentPath?: string;
    };
  };
  user?: {
    // 姓名
    name?: boolean;
    // 用户名
    username?: boolean;
    // 昵称
    nickname?: boolean;
    // 手机号
    phone?: boolean;
    // 邮箱
    email?: boolean;
    // 部门
    dept?: boolean;
    // 角色
    role?: boolean;
    // 岗位
    post?: boolean;
    extend: {
      componentPath: string;
      // 提交
      onSubmit: Function;
      // 校验
      onValidate: Function;
      // 重置
      onReset: Function;
    };
  };
  // 消息下拉面板自定义 - 组件位置如(默认是MicroApp目录下，所以直接写MicroApp下的地址)：/views/xxx/xxx.vue
  newsComponentPath?: string;
  // 点击 “进入消息列表” 时要跳转的地址
  newLinkPath?: string;
  // 通知显示消息//待办
  newsList?: Array<"message" | "todo">;
  // 自定义审批项
  arrpove?: {
    // 自定义审批form表单vue文件路径 如(默认是MicroApp目录下，所以直接写MicroApp下的地址)：/views/xxx/xxx.vue
    componentPath: string;
  };
  // 组织
  organiza?: {
    enable: boolean;
  };
  // 表格内操作按钮显示方式
  table?: {
    btnType?: "icon" | "all" | "title";
  };
  // 平台跳转路径配置
  path: {
    // 审批详情
    approvalDetailPath: "";
    // 消息详情
    newsPath: "";
  };
  // 右上角个人信息功能列
  systemSetting: Array<
    | "switchAccount"
    | "modifyPassword"
    | "shortcut"
    | "homePage"
    | "about"
    | "logOut"
  >;
  // 上传预览的onlyOffice配置信息
  onlyOfficeSystem: {
    isOnlyOfficePreview: boolean;
    onlyOfficeAccept: string;
  };
}

export const systemConfig: ISystemConfig = {
  // 登录页项目LOGO+标题
  project: {
    title: {
      value: "金合技术中台",
      style: "",
    },
    logo: {
      value: "vite/config/assets/logo.svg",
      darkValue: "vite/config/assets/logo.svg",
      style: "",
    },
    position: {
      left: 40,
      top: 20,
    },
    copyright: {
      enable: true,
      desc: "金合中台，高效交付，赋能企业智能化未来！",
    },
  },
  // 表格操作按钮类型
  tableIconType: "icon",
  // IkPageFull 带斑马纹表格
  tableStripe: true,
  tableMoreText: "",
  // 返回登录页时是否记录退出时的地址，登录后跳转至对应页面
  remberRoute: false,
  homePage: "/", // 比如要将用户管理设置为首页，这里就填写 '/system-manage/user-manage'
  loginOnlyBg: false, // 登录页是否只显示背景图不显示其它元素
  loginComponentPath: null,
  loginBg: "vite/config/assets/bg.mp4",
  loginBgDark: "vite/config/assets/bgDark.mp4",
  loginTitle: "您好，欢迎登录",
  navBg: "nav_bg.webp",
  navBgDark: "nav_bg_dark.webp",
  // 数组形式传递，可设置多个标题
  navTitle: ["一诺千金", "合作共赢"],
  // navTitle: ['', ''],
  navTitleMargin: 70,

  sysTitle: "金合技术中台",
  sysTitleEn: "", // 'IKING TECH PLATFORM',
  sysIcon: "logo.png",
  sysAlign: "center",
  twoLine: true,

  tools: {
    org: true,
    tenant: true,
    search: true,
    notice: true,
    i18n: true,
    fullScreen: true,
    refresh: true,
    theme: true,
    setting: true,
    userInfo: true,
  },

  loginTools: {
    forgetPassword: false,
    register: false,
    remberMe: true,
    // 数组形式传递，可设置多个登录方式；传[]则不显示登录方式
    loginType: ["password", "phone", "wx", "dd"],
  },
  role: {
    // 只配置新增角色时需要显示的项
    fieldList: ["name", "dataScopeType", "remark", "dataScopeCodes"],
    // 角色管理配置自定义tab页面
    extendTabs: {
      name: "",
      componentPath: "",
    },
  },
  // 只配置需要必填的项即可
  user: {
    name: true,
    username: true,
    phone: true,
    extend: {
      componentPath: "",
      // 提交
      onSubmit: () => {},
      // 校验
      onValidate: () => {},
      // 重置
      onReset: () => {},
    },
  },
  manageDesign: {
    designMode: ["工程模式", "功能模式", "表单模式"], //
  },

  // ========消息配置==在未来版本将被替换为组件名称==========
  newsComponentPath: "",
  newLinkPath: "",
  newsList: ["message", "todo"],
  arrpove: {
    componentPath: "views/text.vue",
  },
  // 组织
  organiza: {
    enable: true,
  },
  // 表格内操作按钮显示方式
  table: {
    btnType: "icon", // all title
  },
  path: {
    approvalDetailPath: "",
    newsPath: "",
  },
  systemSetting: [
    "switchAccount",
    "modifyPassword",
    "shortcut",
    "homePage",
    "about",
    "logOut",
  ],
  onlyOfficeSystem: {
    isOnlyOfficePreview: true,
    onlyOfficeAccept: ".pdf,.docx,.doc.ppt,.pptx,.xlsx,.xls,",
  },
};
