/*
 * @Author       : wfl
 * @LastEditors: zqf
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-06-13 18:56:35
 * @LastEditTime: 2024-03-14 16:54:12
 */
import { ikHttp } from '@main/api/request'

export const approveCenterApi = {
  demo: async () => ikHttp.post('/demo'),
}
