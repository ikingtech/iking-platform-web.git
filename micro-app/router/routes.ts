import type { RouteRecordRaw } from 'vue-router'

export const routes: RouteRecordRaw[] = [
  {
    path: '/micro-test',
    name: 'microTest',
    component: () => import('@micro/views/test.vue'),
    meta: {
      whiteList: true,
      title: '项目配置',
    },
  },
]
