/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-06-13 18:57:50
 * @LastEditTime : 2023-06-13 18:58:58
 */
const useDemoStore = defineStore(
  // 唯一ID
  'demo',
  () => {
    const count = ref(0)

    return {
      count,
    }
  },
)

export default useDemoStore
