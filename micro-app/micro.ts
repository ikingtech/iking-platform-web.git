/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-03-05 11:39:01
 * @LastEditTime : 2024-03-05 13:43:19
 */
import startApp from '@main/main'

const { mount } = startApp()
// const { app, mount } = startApp()
// 在挂载前可以做项目自定义的各种配置
// 例如：使用新的插件  app.use(xxx)

// 挂载
mount()
