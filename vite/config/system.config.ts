/*
 * @Author       : wfl
 * @LastEditors: qiye
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-06-27 16:40:54
 * @LastEditTime: 2024-02-05 11:50:17
 */
// 报表导出
export const excludeUrl = [
  '/server/scaffold/project/download',
  '/server/scaffold/module/download',
]

export const systemConfig: ISystemConfig = {
  // 登录页项目LOGO+标题
  project: {
    title: {
      value: '',
      style: '',
    },
    logo: {
      value: '',
      style: '',
    },
    position: {
      left: 50,
      top: 50,
    },
  },
  // 返回登录页时是否记录退出时的地址，登录后跳转至对应页面
  remberRoute: false,
  homePage: '/', // 比如要将用户管理设置为首页，这里就填写 '/system-manage/user-manage'
  loginOnlyBg: false, // 登录页是否只显示背景图不显示其它元素
  loginComponentPath: null,
  loginBg: 'login/bg@2x.webp',
  loginBgDark: 'login-dark/bg@2x.webp',
  loginTitle: '您好，欢迎登录',
  navBg: 'nav_bg.webp',
  navBgDark: 'nav_bg_dark.webp',
  // 数组形式传递，可设置多个标题
  navTitle: ['一诺千金', '合作共赢'],
  // navTitle: ['', ''],
  navTitleMargin: 70,

  sysTitle: '金合技术中台12313',
  sysTitleEn: '', // 'IKING TECH PLATFORM',
  sysIcon: 'logo.png',
  sysAlign: 'center',
  twoLine: true,

  tools: {
    org: true,
    tenant: true,
    search: true,
    notice: true,
    i18n: true,
    fullScreen: true,
    refresh: true,
    theme: true,
    setting: true,
    userInfo: true,
  },

  loginTools: {
    forgetPassword: false,
    register: false,
    remberMe: true,
    // 数组形式传递，可设置多个登录方式；传[]则不显示登录方式
    loginType: ['password', 'phone', 'wx', 'dd'],
  },
  // 只配置需要必填的项即可
  user: {
    name: true,
    username: true,
    phone: true,
  },

  manageDesign: {
    designMode: ['工程模式', '功能模式', '表单模式'], //
  },

  // ========消息配置==在未来版本将被替换为组件名称==========
  newsComponentPath: '',
  newLinkPath: '',
  arrpove: {
    componentPath: 'views/text.vue',
  },
  // 组织
  organiza: {
    enable: true,
  },
  // 登录页验证码配置
  loginImageValidate: {
    background: '#F2F5F7',
    backgroundDark: '#2B2B40',
    height: 48,
    codeColor: ['#F628E2'],
    type: 'number',
    useCase: false,
  },
  // 表格内操作按钮显示方式
  table: {
    btnType: 'icon', // all title
  },
}
