/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-03-14 11:00:23
 * @LastEditTime : 2024-03-15 14:45:18
 */
import path from 'node:path'
import process from 'node:process'
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons'

export default function createSvgIcon(isBuild) {
  return createSvgIconsPlugin({
    iconDirs: [path.resolve(process.cwd(), 'main-app/assets/icons/')],
    symbolId: 'ik-icon-[dir]-[name]',
    svgoOptions: isBuild,
  })
}
