/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-03-14 11:00:23
 * @LastEditTime : 2024-04-28 11:56:27
 */
import type { PluginOption } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import vueLegacy from '@vitejs/plugin-legacy'
import { createHtmlPlugin } from 'vite-plugin-html'
import vueDevTools from 'vite-plugin-vue-devtools'
import appInfo from './app-info'
import createAutoImport from './auto-import'
import createComponents from './components'
import createUnocss from './unocss'
import createSvgIcon from './svg-icon'
import createI18n from './i18n'
import createLayouts from './layouts'
import createPages from './pages'
import createCompression from './compression'
import createArchiver from './archiver'
import copyConfigPlugin from './copy-config'

import createBanner from './banner'
import createProgress from './progress'

export default function createVitePlugins(viteEnv, isBuild = false, mode: string) {
  const vitePlugins: (PluginOption | PluginOption[])[] = [
    appInfo(),
    vue({
      template: {
        compilerOptions: {
          isCustomElement: (tag: string) => {
            return tag.startsWith('ik-')
          },
        },
      },
    }),
    vueJsx(),
    vueLegacy({
      renderLegacyChunks: false,
      modernPolyfills: [
        'es.array.at',
        'es.array.find-last',
      ],
    }),
    createHtmlPlugin({
      minify: true,
      entry: isBuild ? '../micro-app/micro.ts' : '../../micro-app/micro.ts',
      template: 'main-app/index.html',
      inject: {
        data: {
          title: viteEnv.VITE_APP_TITLE,
          icon: viteEnv.VITE_APP_ICON,
          downloadUrl: viteEnv.VITE_APP_DOWNLOAD_URL,
        },
      },
    }),
    copyConfigPlugin(mode),
    vueDevTools({
      launchEditor: 'webstorm',
    }),
  ]
  vitePlugins.push(createAutoImport())
  vitePlugins.push(createComponents())
  vitePlugins.push(createUnocss())
  vitePlugins.push(createSvgIcon(isBuild))
  vitePlugins.push(createI18n())
  vitePlugins.push(createLayouts())
  vitePlugins.push(createPages())
  vitePlugins.push(...createCompression(viteEnv, isBuild))
  vitePlugins.push(createArchiver(viteEnv))
  vitePlugins.push(createBanner())
  vitePlugins.push(createProgress())
  return vitePlugins
}
