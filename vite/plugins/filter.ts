// vite-filter-warn-plugin.js
import type { Plugin } from 'vite'

export default function viteFilterWarnPlugin(): Plugin {
  return {
    name: 'vite-filter-warn-plugin',

    async configureServer(server) {
      // 在开发服务器启动时，也可以过滤掉服务器端的 WARN 信息
      server.watcher.on('change', (file) => {
        if (file.endsWith('.js')) {
          // TODO: 实现类似构建时的过滤逻辑，这里仅作占位
        }
      })
    },

    async configResolved(config) {
      // 如果您需要基于 Vite 配置动态调整过滤规则，可以在这里实现
    },

    async buildStart() {
      // 构建开始时的准备工作，例如初始化过滤规则等
    },

    async generateBundle(options, bundle) {
      // 这个钩子在所有模块处理完毕，生成最终的输出包时被调用
      // 可以在这里遍历 bundle 对象，对每个 chunk 进行处理
      Object.values(bundle).forEach((chunk) => {
        if (chunk.type === 'chunk' && chunk.code) {
          // 使用正则表达式匹配并移除 WARN 信息
          chunk.code = chunk.code.replace(/\/\*!\s*WARN[\s\S]*?\*\//g, '')
        }
      })
    },

    async transform(code, id) {
      // 在模块转换阶段，也可以尝试过滤 WARN 信息，但这可能会影响源码映射（source map）
      // code = code.replace(/\/\*!\s*WARN[\s\S]*?\*\//g, '');

      return {
        code,
        map: null, // 如果您确实需要在此阶段过滤，可能需要手动处理 source map
      }
    },

    async renderChunk(code, chunk, options) {
      // 在代码块生成后，对其进行处理，移除 WARN 信息
      return {
        code: code.replace(/\/\*!\s*WARN[\s\S]*?\*\//g, ''),
        map: null, // 如果您确实需要在此阶段过滤，可能需要手动处理 source map
      }
    },

    async writeBundle(options, bundle) {
      // 在所有代码块写入磁盘前，进行最后的处理
      // 这里可以用来验证过滤后的代码，或者进行其他清理工作
    },
  }
}
