/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-02-29 14:41:18
 * @LastEditTime : 2024-03-29 14:02:49
 */
import boxen from 'boxen'
import picocolors from 'picocolors'
import type { Plugin } from 'vite'

export default function appInfo(): Plugin {
  return {
    name: 'appInfo',
    apply: 'serve',
    async buildStart() {
      const { bold, green, magenta, bgGreen, underline, red } = picocolors
      console.info(boxen(
          `${bold(green(`[ 由 ${bgGreen('IKING-ADMIN')} 驱动 ]`))}\n\n[ ${underline('https://gitee.com/ikingtech')} 当前使用：${magenta('内部项目专属版')} ]\n\n${red('[ 未经授权,请勿移除或篡改本信息,平台版权归金合信息科技股份有限公司所有 ]')}`,
          {
            padding: 1,
            margin: 1,
            borderStyle: 'double',
            textAlignment: 'center',
          },
      ),
      )
    },
  }
}
