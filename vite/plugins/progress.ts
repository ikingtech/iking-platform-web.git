import progress from 'vite-plugin-progress'
import colors from 'picocolors'

export default function createProgress() {
  return progress({
    srcDir: 'micro-app',
    format: `${colors.green(colors.bold('[Iking Admin] 构建中...'))}: ${colors.blue('[:bar] :percent 用时:elapsed秒')}`,
    total: 100,
    width: 60,
    // complete: '=',
    incomplete: '',
    callback: () => {
      console.info(colors.green(colors.bold('[Iking Admin] 构建完成!')))
    },
  })
}
