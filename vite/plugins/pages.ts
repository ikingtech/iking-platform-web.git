import Pages from 'vite-plugin-pages'

export default function createPages() {
  return Pages({
    dirs: 'main-app/views',
    exclude: [
      '**/components/**/*.vue',
    ],
  })
}
