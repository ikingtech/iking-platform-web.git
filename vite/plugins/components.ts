/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-02-29 14:41:18
 * @LastEditTime : 2024-03-07 15:30:19
 */
import components from 'unplugin-vue-components/vite'

export default function createComponents() {
  return components({
    dirs: [
      'main-app/components',
      'main-app/layouts/ui-kit',
    ],
    extensions: ['vue', 'tsx'],
    include: [/\.vue$/, /\.vue\?vue/, /\.tsx$/],
    dts: './types/components.d.ts',
  })
}
