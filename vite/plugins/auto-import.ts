/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-02-29 14:41:18
 * @LastEditTime : 2024-04-16 10:17:08
 */
import autoImport from 'unplugin-auto-import/vite'

export default function createAutoImport() {
  return autoImport({
    imports: [
      'vue',
      'vue-router',
      'pinia',
      {
        'iking-utils-pro': ['Cryptojs', 'dayjs', 'lodash', 'ikDate', 'ikColor', 'ikObject', 'ikTree', 'ikVue', 'ikDom', 'paramType', 'buildShortUUID', 'buildUUID', 'ikMitt', 'ikResize', 'ikFile', 'AesEncryption', 'EncryptionParams', 'passwordEncry', 'passwordValid', 'ikStore', 'getIdCardInfo', 'ikArray', 'ikString', 'useSocket', 'ikValidate', 'ikMath', 'safe', '_', 'ikNotification'],
      },
    ],
    dts: './types/auto-imports.d.ts',
    dirs: [
      './main-app/hooks/**',
      './vite/config/**',
      './main-app/enums/**',
      './main-app/utils/global/**',
      './main-app/store/modules/**',
      './micro-app/config/**',
    ],
  })
}
