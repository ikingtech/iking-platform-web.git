/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-03-20 16:10:36
 * @LastEditTime : 2024-03-20 16:36:28
 */
import path from 'node:path'
import fs from 'fs-extra'
import picocolors from 'picocolors'

export default async function replaceMainPlugin(viteEnv) {
  const { red, green } = picocolors
  if (viteEnv.VITE_AUTO_UPDATE !== 'true') {
    console.info(green('已关闭更新main-app...'))
    return
  };
  const srcPath = path.join(__dirname, '../../node_modules/iking-main-app/pkg')
  const targetPath = path.join(__dirname, '../../main-app')
  if (fs.existsSync(srcPath)) {
    console.info(green('开始更新main-app...'))

    // 如果目标目录存在，则清空目标目录
    if (fs.existsSync(targetPath) && !targetPath.endsWith('main-app')) {
      fs.emptyDirSync(targetPath)
    }

    // 复制文件
    await fs.copy(srcPath, targetPath)

    console.info(green('更新完成！'))
  }
  else {
    console.info(red('npm包[iking-main-app]不存在！请检查是否已install iking-main-app'))
  }
}
