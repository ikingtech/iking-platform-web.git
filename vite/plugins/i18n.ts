/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-02-29 14:41:18
 * @LastEditTime : 2024-03-05 10:30:34
 */
import path from 'node:path'
import vueI18n from '@intlify/unplugin-vue-i18n/vite'

export default function createI18n() {
  return vueI18n({
    include: path.resolve(__dirname, '../../main-app/assets/locales/lang/**'),
  })
}
