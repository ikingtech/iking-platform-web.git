/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-03-06 14:04:31
 * @LastEditTime : 2024-04-02 14:35:04
 */
import fs from 'node:fs'
import path from 'node:path'
import picocolors from 'picocolors'

const ICON_MAIN_COLOR = '#A1A5B7'
const ICON_COLOR = '#E3E4E9'
const ICON_MAIN_COLOR1 = '#71778D'
const ICON_COLOR1 = '#B9BAC5'

// 将双色图标中的色值修改为 颜色变量 后写入文件
export function replaceIconfont() {
  const { blue, red } = picocolors
  try {
    const iconPath = path.resolve(__dirname, '../../main-app/iconfont/iconfont.js')
    const iconFile = fs.readFileSync(iconPath)
    let iconStr = iconFile.toString()
    console.info(blue('[Iking Admin]: 替换双色图标色值为变量'))
    iconStr = iconStr.replaceAll(ICON_MAIN_COLOR, 'var(--ik-color-icon-dark)')
      .replaceAll(ICON_MAIN_COLOR1, 'var(--ik-color-icon-dark)')
      .replaceAll(ICON_COLOR, 'var(--ik-color-icon-light)')
      .replaceAll(ICON_COLOR1, 'var(--ik-color-icon-light)')
    fs.writeFileSync(iconPath, iconStr)
    console.info(blue('[Iking Admin]: 替换完成'))
    console.info(blue('[Iking Admin]: 更新双色图标可将下载后的iconfont.js文件替换main-app/iconfont/iconfont.js文件'))
  }
  catch (error) {
    console.info(red('[Iking Admin]: 替换双色图标色值错误'))
  }
}
