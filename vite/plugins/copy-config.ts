// 导入所需模块
import type { Plugin } from 'vite'
import fse from 'fs-extra'

// 定义复制目录的插件
export default function copyConfigPlugin(mode): Plugin {
  return {
    name: 'vite-plugin-copy-config',
    enforce: 'post',
    // 确保在所有其他插件之后执行
    async closeBundle() {
      if (mode === 'development') {
        return
      }
      try {
        const sourceDir = 'vite/config'
        const targetDir = `dist${mode === 'production' ? '' : `-${mode}`}/vite/config`
        // 确保目标目录存在
        await fse.ensureDir(targetDir)
        // 复制目录
        try {
          await fse.copy(sourceDir, targetDir, { recursive: true })
        }
        catch (e) {
          console.warn(e)
        }
      }
      catch (err) {
        console.error('Error copying directory:', err)
      }
    },
  }
}
