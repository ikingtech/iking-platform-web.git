import { hex2rgba } from '@unocss/preset-mini/utils'

export default {
  light: {
    // 颜色主题
    'color-scheme': 'light',
    // 内置 UI
    '--ui-primary': hex2rgba('#029ef7')!.join(' '),
    '--ui-text': hex2rgba('#fcfcfc')!.join(' '),
    // 主体
    '--ik-bg': '#F5F8FA',
    '--ik-container-bg': '#fff',
    '--ik-border-color': '#f2f2f2',
    // 头部
    '--ik-header-bg': '#fff',
    '--ik-header-color': '#0f0f0f',
    '--ik-header-menu-color': '#0f0f0f',
    '--ik-header-menu-hover-bg': '#EAEEF2',
    '--ik-header-menu-hover-color': 'var(--ik-color-primary)',
    '--ik-header-menu-active-bg': '#EAEEF2',
    '--ik-header-menu-active-color': 'var(--ik-color-primary)',
    // 主导航
    '--ik-main-sidebar-bg': '#fff',
    '--ik-main-sidebar-menu-color': 'var(--ik-color-font-normal)',
    '--ik-main-sidebar-menu-hover-bg': '#EAEEF2',
    '--ik-main-sidebar-menu-hover-color': 'var(--ik-color-primary)',
    '--ik-main-sidebar-menu-active-bg': 'var(--ik-color-bg-page-tag)',
    '--ik-main-sidebar-menu-active-color': 'var(--ik-color-primary)',
    // 次导航
    '--ik-sub-sidebar-bg': '#fff',
    '--ik-sub-sidebar-logo-bg': '#fff',
    '--ik-sub-sidebar-logo-color': 'var(--ik-color-font-title)',
    '--ik-sub-sidebar-menu-color': 'var(--ik-color-font-normal)',
    '--ik-sub-sidebar-menu-hover-bg': '#EAEEF2',
    '--ik-sub-sidebar-menu-hover-color': 'var(--ik-color-primary)',
    '--ik-sub-sidebar-menu-active-bg': 'var(--ik-color-bg-page-tag)',
    '--ik-sub-sidebar-menu-active-color': 'var(--ik-color-primary)',
    // 标签栏
    '--ik-tabbar-dividers-bg': '#a3a3a3',
    '--ik-tabbar-tab-color': '#a3a3a3',
    '--ik-tabbar-tab-hover-bg': '#e5e5e5',
    '--ik-tabbar-tab-hover-color': '#0f0f0f',
    '--ik-tabbar-tab-active-color': '#0f0f0f',
    // scroll
    '--ik-scrollbar-color': 'rgb(187 186 186 / 45.1%)',
    '--ik-scrollbar-hover-color': 'rgb(187 186 186 / 55.1%)',
  },
  classic: {
    // 颜色主题
    'color-scheme': 'light',
    // 内置 UI
    '--ui-primary': hex2rgba('#409eff')!.join(' '),
    '--ui-text': hex2rgba('#fcfcfc')!.join(' '),
    // 主体
    '--ik-bg': '#F5F8FA',
    '--ik-container-bg': '#fff',
    '--ik-border-color': '#f2f2f2',
    // 头部
    '--ik-header-bg': '#222b45',
    '--ik-header-color': '#fff',
    '--ik-header-menu-color': '#fff',
    '--ik-header-menu-hover-bg': '#EAEEF2',
    '--ik-header-menu-hover-color': 'var(--ik-color-primary)',
    '--ik-header-menu-active-bg': '#28283C',
    '--ik-header-menu-active-color': 'var(--ik-color-primary)',
    // 主导航
    '--ik-main-sidebar-bg': '#222b45',
    '--ik-main-sidebar-menu-color': '#fff',
    '--ik-main-sidebar-menu-hover-bg': '#EAEEF2',
    '--ik-main-sidebar-menu-hover-color': 'var(--ik-color-primary)',
    '--ik-main-sidebar-menu-active-bg': '#28283C',
    '--ik-main-sidebar-menu-active-color': 'var(--ik-color-primary)',
    // 次导航
    '--ik-sub-sidebar-bg': '#fff',
    '--ik-sub-sidebar-logo-bg': '#fff',
    '--ik-sub-sidebar-logo-color': 'var(--ik-color-font-title)',
    '--ik-sub-sidebar-menu-color': '#0f0f0f',
    '--ik-sub-sidebar-menu-hover-bg': '#EAEEF2',
    '--ik-sub-sidebar-menu-hover-color': 'var(--ik-color-primary)',
    '--ik-sub-sidebar-menu-active-bg': '#28283C',
    '--ik-sub-sidebar-menu-active-color': 'var(--ik-color-primary)',
    // 标签栏
    '--ik-tabbar-dividers-bg': '#a3a3a3',
    '--ik-tabbar-tab-color': '#a3a3a3',
    '--ik-tabbar-tab-hover-bg': '#e5e5e5',
    '--ik-tabbar-tab-hover-color': '#0f0f0f',
    '--ik-tabbar-tab-active-color': '#0f0f0f',
  },
  naive: {
    // 颜色主题
    'color-scheme': 'light',
    // 内置 UI
    '--ui-primary': hex2rgba('#18a058')!.join(' '),
    '--ui-text': hex2rgba('#fcfcfc')!.join(' '),
    // 主体
    '--ik-bg': '#F5F8FA',
    '--ik-container-bg': '#fff',
    '--ik-border-color': '#f2f2f2',
    // 头部
    '--ik-header-bg': '#1d2935',
    '--ik-header-color': '#fff',
    '--ik-header-menu-color': '#fff',
    '--ik-header-menu-hover-bg': '#EAEEF2',
    '--ik-header-menu-hover-color': '#fff',
    '--ik-header-menu-active-bg': '#EAEEF2',
    '--ik-header-menu-active-color': 'var(--ik-color-primary)',
    // 主导航
    '--ik-main-sidebar-bg': '#1d2935',
    '--ik-main-sidebar-menu-color': '#fff',
    '--ik-main-sidebar-menu-hover-bg': '#EAEEF2',
    '--ik-main-sidebar-menu-hover-color': '#fff',
    '--ik-main-sidebar-menu-active-bg': '#18a058',
    '--ik-main-sidebar-menu-active-color': '#fff',
    // 次导航
    '--ik-sub-sidebar-bg': '#fff',
    '--ik-sub-sidebar-logo-bg': '#fff',
    '--ik-sub-sidebar-logo-color': 'var(--ik-color-font-title)',
    '--ik-sub-sidebar-menu-color': '#0f0f0f',
    '--ik-sub-sidebar-menu-hover-bg': '#EAEEF2',
    '--ik-sub-sidebar-menu-hover-color': 'var(--ik-color-primary)',
    '--ik-sub-sidebar-menu-active-bg': '#18a058',
    '--ik-sub-sidebar-menu-active-color': '#fff',
    // 标签栏
    '--ik-tabbar-dividers-bg': '#a3a3a3',
    '--ik-tabbar-tab-color': '#a3a3a3',
    '--ik-tabbar-tab-hover-bg': '#e5e5e5',
    '--ik-tabbar-tab-hover-color': '#0f0f0f',
    '--ik-tabbar-tab-active-color': '#0f0f0f',
  },
  barbie: {
    // 颜色主题
    'color-scheme': 'light',
    // 内置 UI
    '--ui-primary': hex2rgba('#ff43bc')!.join(' '),
    '--ui-text': hex2rgba('#fcfcfc')!.join(' '),
    // 主体
    '--ik-bg': '#F5F8FA',
    '--ik-container-bg': '#fff',
    '--ik-border-color': '#f2f2f2',
    // 头部
    '--ik-header-bg': '#ff43bc',
    '--ik-header-color': '#fff',
    '--ik-header-menu-color': '#fff',
    '--ik-header-menu-hover-bg': '#dd39a3',
    '--ik-header-menu-hover-color': '#fff',
    '--ik-header-menu-active-bg': '#dd39a3',
    '--ik-header-menu-active-color': 'var(--ik-color-primary)',
    // 主导航
    '--ik-main-sidebar-bg': '#ff43bc',
    '--ik-main-sidebar-menu-color': '#fff',
    '--ik-main-sidebar-menu-hover-bg': '#dd39a3',
    '--ik-main-sidebar-menu-hover-color': '#fff',
    '--ik-main-sidebar-menu-active-bg': '#dd39a3',
    '--ik-main-sidebar-menu-active-color': '#fff',
    // 次导航
    '--ik-sub-sidebar-bg': '#fff',
    '--ik-sub-sidebar-logo-bg': '#fff',
    '--ik-sub-sidebar-logo-color': 'var(--ik-color-font-title)',
    '--ik-sub-sidebar-menu-color': '#0f0f0f',
    '--ik-sub-sidebar-menu-hover-bg': '#dd39a3',
    '--ik-sub-sidebar-menu-hover-color': 'var(--ik-color-primary)',
    '--ik-sub-sidebar-menu-active-bg': '#dd39a3',
    '--ik-sub-sidebar-menu-active-color': '#fff',
    // 标签栏
    '--ik-tabbar-dividers-bg': '#a3a3a3',
    '--ik-tabbar-tab-color': '#a3a3a3',
    '--ik-tabbar-tab-hover-bg': '#e5e5e5',
    '--ik-tabbar-tab-hover-color': '#0f0f0f',
    '--ik-tabbar-tab-active-color': '#0f0f0f',
  },
  winter: {
    // 颜色主题
    'color-scheme': 'light',
    // 内置 UI
    '--ui-primary': hex2rgba('#a6e4f8')!.join(' '),
    '--ui-text': hex2rgba('#3e4e68')!.join(' '),
    // 主体
    '--ik-bg': '#f1f5fe',
    '--ik-container-bg': '#fff',
    '--ik-border-color': '#f2f2f2',
    // 头部
    '--ik-header-bg': '#fff',
    '--ik-header-color': '#3e4e68',
    '--ik-header-menu-color': '#3e4e68',
    '--ik-header-menu-hover-bg': '#e3e8f2',
    '--ik-header-menu-hover-color': '#3e4e68',
    '--ik-header-menu-active-bg': '#a6e4f8',
    '--ik-header-menu-active-color': '#3e4e68',
    // 主导航
    '--ik-main-sidebar-bg': '#f1f5fe',
    '--ik-main-sidebar-menu-color': '#3e4e68',
    '--ik-main-sidebar-menu-hover-bg': '#e3e8f2',
    '--ik-main-sidebar-menu-hover-color': '#3e4e68',
    '--ik-main-sidebar-menu-active-bg': '#a6e4f8',
    '--ik-main-sidebar-menu-active-color': '#3e4e68',
    // 次导航
    '--ik-sub-sidebar-bg': '#fff',
    '--ik-sub-sidebar-logo-bg': '#e3e8f2',
    '--ik-sub-sidebar-logo-color': '#3e4e68',
    '--ik-sub-sidebar-menu-color': '#0f0f0f',
    '--ik-sub-sidebar-menu-hover-bg': '#e3e8f2',
    '--ik-sub-sidebar-menu-hover-color': '#3e4e68',
    '--ik-sub-sidebar-menu-active-bg': '#a6e4f8',
    '--ik-sub-sidebar-menu-active-color': '#3e4e68',
    // 标签栏
    '--ik-tabbar-dividers-bg': '#a3a3a3',
    '--ik-tabbar-tab-color': '#a3a3a3',
    '--ik-tabbar-tab-hover-bg': '#e5e5e5',
    '--ik-tabbar-tab-hover-color': '#0f0f0f',
    '--ik-tabbar-tab-active-color': '#0f0f0f',
  },
  cyberpunk: {
    // 颜色主题
    'color-scheme': 'light',
    // 内置 UI
    '--ui-primary': hex2rgba('#dbcb00')!.join(' '),
    '--ui-text': hex2rgba('#0f0f0f')!.join(' '),
    // 主体
    '--ik-bg': '#F5F8FA',
    '--ik-container-bg': '#fff',
    '--ik-border-color': '#f2f2f2',
    // 头部
    '--ik-header-bg': '#fff',
    '--ik-header-color': '#302d12',
    '--ik-header-menu-color': '#302d12',
    '--ik-header-menu-hover-bg': '#dbcb00',
    '--ik-header-menu-hover-color': '#302d12',
    '--ik-header-menu-active-bg': '#dbcb00',
    '--ik-header-menu-active-color': '#302d12',
    // 主导航
    '--ik-main-sidebar-bg': '#f2f2f2',
    '--ik-main-sidebar-menu-color': '#302d12',
    '--ik-main-sidebar-menu-hover-bg': '#dbcb00',
    '--ik-main-sidebar-menu-hover-color': '#302d12',
    '--ik-main-sidebar-menu-active-bg': '#dbcb00',
    '--ik-main-sidebar-menu-active-color': '#302d12',
    // 次导航
    '--ik-sub-sidebar-bg': '#fff',
    '--ik-sub-sidebar-logo-bg': '#dbcb00',
    '--ik-sub-sidebar-logo-color': '#302d12',
    '--ik-sub-sidebar-menu-color': '#0f0f0f',
    '--ik-sub-sidebar-menu-hover-bg': '#dbcb00',
    '--ik-sub-sidebar-menu-hover-color': '#302d12',
    '--ik-sub-sidebar-menu-active-bg': '#dbcb00',
    '--ik-sub-sidebar-menu-active-color': '#302d12',
    // 标签栏
    '--ik-tabbar-dividers-bg': '#a3a3a3',
    '--ik-tabbar-tab-color': '#a3a3a3',
    '--ik-tabbar-tab-hover-bg': '#e5e5e5',
    '--ik-tabbar-tab-hover-color': '#0f0f0f',
    '--ik-tabbar-tab-active-color': '#0f0f0f',
  },
  dark: {
    // 颜色主题
    'color-scheme': 'dark',
    // 内置 UI
    '--ui-primary': hex2rgba('#e5e5e5')!.join(' '),
    '--ui-text': hex2rgba('#0f0f0f')!.join(' '),
    // 主体
    '--ik-bg': '#151521',
    '--ik-container-bg': '#1E1E2D',
    '--ik-border-color': '#15191e',
    // 头部
    '--ik-header-bg': '#141414',
    '--ik-header-color': '#e5e5e5',
    '--ik-header-menu-color': '#a8a29e',
    '--ik-header-menu-hover-bg': '#141414',
    '--ik-header-menu-hover-color': 'var(--ik-color-primary)',
    '--ik-header-menu-active-bg': '#28283C',
    '--ik-header-menu-active-color': '#009EF7',
    // 主导航
    '--ik-main-sidebar-bg': '#1E1E2D',
    '--ik-main-sidebar-menu-color': '#a8a29e',
    '--ik-main-sidebar-menu-hover-bg': '#141414',
    '--ik-main-sidebar-menu-hover-color': 'var(--ik-color-primary)',
    '--ik-main-sidebar-menu-active-bg': '#28283C',
    '--ik-main-sidebar-menu-active-color': 'var(--ik-color-primary)',
    // 次导航
    '--ik-sub-sidebar-bg': '#1E1E2D',
    '--ik-sub-sidebar-logo-bg': '#161622',
    '--ik-sub-sidebar-logo-color': '#e5e5e5',
    '--ik-sub-sidebar-menu-color': '#a8a29e',
    '--ik-sub-sidebar-menu-hover-bg': '#0a0a0a',
    '--ik-sub-sidebar-menu-hover-color': 'var(--ik-color-primary)',
    '--ik-sub-sidebar-menu-active-bg': '#28283C',
    '--ik-sub-sidebar-menu-active-color': 'var(--ik-color-primary)',
    // 标签栏
    '--ik-tabbar-dividers-bg': '#a8a29e',
    '--ik-tabbar-tab-color': '#a8a29e',
    '--ik-tabbar-tab-hover-bg': '#1b1b1b',
    '--ik-tabbar-tab-hover-color': '#e5e5e5',
    '--ik-tabbar-tab-active-color': '#e5e5e5',
    // scroll
    '--ik-scrollbar-color': '#28283C',
    '--ik-scrollbar-hover-color': 'rgb(187 186 186 / 55.1%)',
  },
  dracula: {
    // 颜色主题
    'color-scheme': 'dark',
    // 内置 UI
    '--ui-primary': hex2rgba('#a6adbb')!.join(' '),
    '--ui-text': hex2rgba('#242b33')!.join(' '),
    // 主体
    '--ik-bg': '#272935',
    '--ik-container-bg': '#1d232a',
    '--ik-border-color': '#191E24',
    // 头部
    '--ik-header-bg': '#191E24',
    '--ik-header-color': '#f8f8f2',
    '--ik-header-menu-color': '#a6adbb',
    '--ik-header-menu-hover-color': '#f8f8f2',
    '--ik-header-menu-hover-bg': '#181920',
    '--ik-header-menu-active-color': '#f8f8f2',
    '--ik-header-menu-active-bg': '#414558',
    // 主导航
    '--ik-main-sidebar-bg': '#191E24',
    '--ik-main-sidebar-menu-color': '#a6adbb',
    '--ik-main-sidebar-menu-hover-color': '#f8f8f2',
    '--ik-main-sidebar-menu-hover-bg': '#181920',
    '--ik-main-sidebar-menu-active-color': '#f8f8f2',
    '--ik-main-sidebar-menu-active-bg': '#414558',
    // 次导航
    '--ik-sub-sidebar-bg': '#1d232a',
    '--ik-sub-sidebar-logo-color': '#1d232a',
    '--ik-sub-sidebar-logo-bg': '#a6adbb',
    '--ik-sub-sidebar-menu-color': '#a6adbb',
    '--ik-sub-sidebar-menu-hover-color': '#f8f8f2',
    '--ik-sub-sidebar-menu-hover-bg': '#181920',
    '--ik-sub-sidebar-menu-active-color': '#f8f8f2',
    '--ik-sub-sidebar-menu-active-bg': '#414558',
    // 标签栏
    '--ik-tabbar-dividers-bg': '#a6adbb',
    '--ik-tabbar-tab-color': '#a6adbb',
    '--ik-tabbar-tab-hover-color': '#f8f8f2',
    '--ik-tabbar-tab-hover-bg': '#242b33',
    '--ik-tabbar-tab-active-color': '#f8f8f2',
  },
  night: {
    // 颜色主题
    'color-scheme': 'dark',
    // 内置 UI
    '--ui-primary': hex2rgba('#0ca6e9')!.join(' '),
    '--ui-text': hex2rgba('#242b33')!.join(' '),
    // 主体
    '--ik-bg': '#0f1729',
    '--ik-container-bg': '#1d283a',
    '--ik-border-color': '#191E24',
    // 头部
    '--ik-header-bg': '#0f1729',
    '--ik-header-color': '#c8cad0',
    '--ik-header-menu-color': '#a6adbb',
    '--ik-header-menu-hover-color': '#c8cad0',
    '--ik-header-menu-hover-bg': '#1d283a',
    '--ik-header-menu-active-color': '#c8cad0',
    '--ik-header-menu-active-bg': '#1d283a',
    // 主导航
    '--ik-main-sidebar-bg': '#0f1729',
    '--ik-main-sidebar-menu-color': '#a6adbb',
    '--ik-main-sidebar-menu-hover-color': '#c8cad0',
    '--ik-main-sidebar-menu-hover-bg': '#1d283a',
    '--ik-main-sidebar-menu-active-color': '#c8cad0',
    '--ik-main-sidebar-menu-active-bg': '#1d283a',
    // 次导航
    '--ik-sub-sidebar-bg': '#1d283a',
    '--ik-sub-sidebar-logo-color': '#1d232a',
    '--ik-sub-sidebar-logo-bg': '#a6adbb',
    '--ik-sub-sidebar-menu-color': '#a6adbb',
    '--ik-sub-sidebar-menu-hover-color': '#c8cad0',
    '--ik-sub-sidebar-menu-hover-bg': '#273449',
    '--ik-sub-sidebar-menu-active-color': '#c8cad0',
    '--ik-sub-sidebar-menu-active-bg': '#0f1729',
    // 标签栏
    '--ik-tabbar-dividers-bg': '#a6adbb',
    '--ik-tabbar-tab-color': '#a6adbb',
    '--ik-tabbar-tab-hover-color': '#c8cad0',
    '--ik-tabbar-tab-hover-bg': '#242b33',
    '--ik-tabbar-tab-active-color': '#c8cad0',
  },
  luxury: {
    // 颜色主题
    'color-scheme': 'dark',
    // 内置 UI
    '--ui-primary': hex2rgba('#dca54c')!.join(' '),
    '--ui-text': hex2rgba('#242b33')!.join(' '),
    // 主体
    '--ik-bg': '#09090b',
    '--ik-container-bg': '#171618',
    '--ik-border-color': '#191E24',
    // 头部
    '--ik-header-bg': '#09090b',
    '--ik-header-color': '#dca54c',
    '--ik-header-menu-color': '#dca54c',
    '--ik-header-menu-hover-color': '#dca54c',
    '--ik-header-menu-hover-bg': '#331800',
    '--ik-header-menu-active-color': '#ffe7a3',
    '--ik-header-menu-active-bg': '#331800',
    // 主导航
    '--ik-main-sidebar-bg': '#09090b',
    '--ik-main-sidebar-menu-color': '#dca54c',
    '--ik-main-sidebar-menu-hover-color': '#dca54c',
    '--ik-main-sidebar-menu-hover-bg': '#331800',
    '--ik-main-sidebar-menu-active-color': '#ffe7a3',
    '--ik-main-sidebar-menu-active-bg': '#331800',
    // 次导航
    '--ik-sub-sidebar-bg': '#171618',
    '--ik-sub-sidebar-logo-color': '#e3d664',
    '--ik-sub-sidebar-logo-bg': '#09090b',
    '--ik-sub-sidebar-menu-color': '#dca54c',
    '--ik-sub-sidebar-menu-hover-color': '#dca54c',
    '--ik-sub-sidebar-menu-hover-bg': '#331800',
    '--ik-sub-sidebar-menu-active-color': '#ffe7a3',
    '--ik-sub-sidebar-menu-active-bg': '#331800',
    // 标签栏
    '--ik-tabbar-dividers-bg': '#a6adbb',
    '--ik-tabbar-tab-color': '#a6adbb',
    '--ik-tabbar-tab-hover-color': '#c8cad0',
    '--ik-tabbar-tab-hover-bg': '#242b33',
    '--ik-tabbar-tab-active-color': '#c8cad0',
  },
  synthwave: {
    // 颜色主题
    'color-scheme': 'dark',
    // 内置 UI
    '--ui-primary': hex2rgba('#58c7f3')!.join(' '),
    '--ui-text': hex2rgba('#1a272e')!.join(' '),
    // 主体
    '--ik-bg': '#1a103c',
    '--ik-container-bg': '#221551',
    '--ik-border-color': '#191E24',
    // 头部
    '--ik-header-bg': '#1a103c',
    '--ik-header-color': '#f9f7fd',
    '--ik-header-menu-color': '#f9f7fd',
    '--ik-header-menu-hover-color': '#f9f7fd',
    '--ik-header-menu-hover-bg': '#221551',
    '--ik-header-menu-active-color': '#f9f7fd',
    '--ik-header-menu-active-bg': '#221551',
    // 主导航
    '--ik-main-sidebar-bg': '#1a103c',
    '--ik-main-sidebar-menu-color': '#f9f7fd',
    '--ik-main-sidebar-menu-hover-color': '#f9f7fd',
    '--ik-main-sidebar-menu-hover-bg': '#221551',
    '--ik-main-sidebar-menu-active-color': '#f9f7fd',
    '--ik-main-sidebar-menu-active-bg': '#221551',
    // 次导航
    '--ik-sub-sidebar-bg': '#221551',
    '--ik-sub-sidebar-logo-color': '#f9f7fd',
    '--ik-sub-sidebar-logo-bg': '#1a103c',
    '--ik-sub-sidebar-menu-color': '#f9f7fd',
    '--ik-sub-sidebar-menu-hover-color': '#f9f7fd',
    '--ik-sub-sidebar-menu-hover-bg': '#160e35',
    '--ik-sub-sidebar-menu-active-color': '#f9f7fd',
    '--ik-sub-sidebar-menu-active-bg': '#160e35',
    // 标签栏
    '--ik-tabbar-dividers-bg': '#a6adbb',
    '--ik-tabbar-tab-color': '#a6adbb',
    '--ik-tabbar-tab-hover-color': '#c8cad0',
    '--ik-tabbar-tab-hover-bg': '#242b33',
    '--ik-tabbar-tab-active-color': '#c8cad0',
  },
  stone: {
    // 颜色主题
    'color-scheme': 'dark',
    // 内置 UI
    '--ui-primary': hex2rgba('#68d1bf')!.join(' '),
    '--ui-text': hex2rgba('#1b1a18')!.join(' '),
    // 主体
    '--ik-bg': '#1b1917',
    '--ik-container-bg': '#282524',
    '--ik-border-color': '#43403c',
    // 头部
    '--ik-header-bg': '#1b1917',
    '--ik-header-color': '#e7e5e4',
    '--ik-header-menu-color': '#e7e5e4',
    '--ik-header-menu-hover-color': '#e7e5e4',
    '--ik-header-menu-hover-bg': '#282524',
    '--ik-header-menu-active-color': '#e7e5e4',
    '--ik-header-menu-active-bg': '#282524',
    // 主导航
    '--ik-main-sidebar-bg': '#1b1917',
    '--ik-main-sidebar-menu-color': '#e7e5e4',
    '--ik-main-sidebar-menu-hover-color': '#e7e5e4',
    '--ik-main-sidebar-menu-hover-bg': '#282524',
    '--ik-main-sidebar-menu-active-color': '#e7e5e4',
    '--ik-main-sidebar-menu-active-bg': '#282524',
    // 次导航
    '--ik-sub-sidebar-bg': '#282524',
    '--ik-sub-sidebar-logo-color': '#e7e5e4',
    '--ik-sub-sidebar-logo-bg': '#1a103c',
    '--ik-sub-sidebar-menu-color': '#e7e5e4',
    '--ik-sub-sidebar-menu-hover-color': '#e7e5e4',
    '--ik-sub-sidebar-menu-hover-bg': '#1b1917',
    '--ik-sub-sidebar-menu-active-color': '#e7e5e4',
    '--ik-sub-sidebar-menu-active-bg': '#1b1917',
    // 标签栏
    '--ik-tabbar-dividers-bg': '#a6adbb',
    '--ik-tabbar-tab-color': '#a6adbb',
    '--ik-tabbar-tab-hover-color': '#c8cad0',
    '--ik-tabbar-tab-hover-bg': '#282524',
    '--ik-tabbar-tab-active-color': '#c8cad0',
  },
}
