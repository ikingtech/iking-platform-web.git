/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-02-29 14:41:18
 * @LastEditTime : 2024-04-17 12:21:55
 */
import antfu from '@antfu/eslint-config'

export default antfu(
  {
    typescript: true,
    vue: true,
    stylistic: true,
    unocss: false,
    ignores: [
      'public',
      'dist*',
      '**/*.js',
    ],
    jsonc: false,
    yaml: false,
    formatters: {
      css: false,
      html: true,
    },
  },
  {
    rules: {
      'eslint-comments/no-unlimited-disable': 'off',
      'antfu/top-level-function': 'off',
      'curly': ['error', 'all'],
      'antfu/consistent-list-newline': 'off',
      'no-console': ['error', { allow: ['info', 'error', 'warn'] }],
      // "sort-imports": ["error", {
      //   "ignoreCase": false,
      //   "ignoreDeclarationSort": false,
      //   "ignoreMemberSort": false,
      //   "memberSyntaxSortOrder": ["none", "all", "multiple", "single"],
      //   "allowSeparatedGroups": false
      // }]
      'jsdoc/require-returns-description': 'off',
      'ts/no-this-alias': 'off',
      'ts/no-unused-vars': ['off', {
        vars: 'local',
        args: 'none',
        caughtErrors: 'all',
      }],
      'ts/no-use-before-define': ['off', {
        functions: false,
        classes: true,
        variables: true,
        allowNamedExports: false,
      }],
      'no-unused-vars': ['off', {
        functions: false,
        classes: true,
        variables: false,
        allowNamedExports: false,
      }],
    },
  },
  {
    files: [
      'main-app/**/*.vue',
      'main-app/**/*.ts',
      'micro-app/**/*.vue',
      'micro-app/**/*.ts',
      'vite/**/*.ts',
    ],
    rules: {
      'vue/block-order': ['error', {
        order: ['route', 'i18n', 'script', 'template', 'style'],
      }],
      'object-curly-spacing': ['error', 'always'],
      'arrow-parens': ['off', 'as-needed'],
      'quote-props': ['error', 'as-needed'],
      'style/quote-props': 'off',
      'prefer-template': 'error',
      '@typescript-eslint/ban-types': 0,
      '@typescript-eslint/no-empty-function': 0,
      '@typescript-eslint/no-non-null-assertion': 0,
      '@typescript-eslint/explicit-module-boundary-types': 'off',
      '@typescript-eslint/no-explicit-any': 'off',
      '@typescript-eslint/ban-ts-comment': 'off',
      '@typescript-eslint/no-duplicate-enum-values': 0,
      '@typescript-eslint/no-this-alias': ['off'],
      '@typescript-eslint/no-unused-vars': 'off',
      'vue/first-attribute-linebreak': ['error', {
        singleline: 'ignore',
        multiline: 'below',
      }],
      '@typescript-eslint/no-use-before-define': 'off',
      // '@typescript-eslint/indent': ['warn', 2, { SwitchCase: 1 }],
      'vue/no-v-html': 'off',
      'vue/no-setup-props-destructure': 'off',
      'vue/singleline-html-element-content-newline': 'off',
      'vue/html-self-closing': [
        'error',
        {
          html: {
            void: 'never',
            normal: 'never',
            component: 'always',
          },
        },
      ],
      'vue/max-attributes-per-line': [
        'error',
        {
          singleline: 3,
          multiline: 1,
        },
      ],
      'max-statements-per-line': [
        'error',
        {
          max: 3,
        },
      ],
      'vue/custom-event-name-casing': [
        'error',
        'camelCase',
        {
          // Vue3推荐自定义事件 驼峰命名 为适配某些组件取消验证即 'kebab-case' 'camelCase'都可以
          ignores: ['/^[a-z]+(-[a-z]+)*$/u'],
        },
      ],
      'vue/require-default-prop': 'off',
      'vue/html-closing-bracket-spacing': 'error',
      'vue/no-mutating-props': 'off',
      'vue/multi-word-component-names': 'off',
      'vue/v-on-event-hyphenation': 'off',
      'prettier/prettier': 'off',
      // 'vue/no-unused-vars': ['off', {
      //   vars: 'local',
      //   args: 'none',
      //   caughtErrors: 'all',
      // }],
      'ts/no-use-before-define': ['off', {
        functions: false,
        classes: true,
        variables: true,
        allowNamedExports: false,
      }],
      // 'vue/no-unknown-component-props': ['error', {
      //   ignores: [
      //     /^El[A-Z]/, // 忽略以 'El' 开头的未知组件警告
      //   ],
      // }],
    },
  },
)
