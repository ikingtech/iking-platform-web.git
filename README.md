<!-- Security123$%^ Improved compatibility of back to top link: See: https://github.com/othneildrew/Best-README-Template/pull/73 -->
<a name="readme-top"></a>

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://platform.ikingtech.com">
    <img src="https://platformdoc.ikingtech.com/logo.svg" alt="Logo" width="120" height="120">
  </a>

  <h3>金合技术中台</h3>

  <strong>现代化的下一代企业级技术中台，简洁、高效、稳定、开放</strong>

  <a href="http://www.ikingtech.com/">官网</a>
  |
  <a href="https://platformdoc.ikingtech.com">文档</a>
  |
  <a href="https://platform.ikingtech.com">在线演示</a>
  |
  <a href="https://gitee.com/ikingtech/iking-platform/issues">提交Bug</a>
</div>

### 演示环境账号

* 平台管理员：admin Security123$%^<br />
* 租户管理员：iking_admin Security123$%^<br />

<br />

[![Stargazers][stars-shield]][stars-url]
[![Forks][forks-shield]][forks-url]

[![Vuejs][Vuejs]][Vuejs-url]
[![Vite][Vite]][Vite-url]
[![TypeScript][TypeScript]][TypeScript-url]
[![VSCODE][VSCODE]][VSCODE-url]

[![OpenJDK][OpenJDK]][OpenJDK-url]
[![SpringBoot][SpringBoot]][SpringBoot-url]
[![Maven][Maven]][Maven-url]
[![IDEA][IDEA]][IDEA-url]

<!-- TABLE OF CONTENTS -->
<details>
  <summary>目录</summary>
  <ol>
    <li>
      <a href="#平台概述">平台概述</a>
      <ul>
        <li><a href="#界面预览">界面预览</a></li>
        <li><a href="#特性">特性</a></li>
        <li><a href="#技术架构">技术架构</a></li>
        <li><a href="#系统架构">系统架构</a></li>
      </ul>
    </li>
    <li>
      <a href="#快速开始">快速开始</a>
      <ul>
        <li><a href="#准备工作">准备工作</a></li>
        <li><a href="#安装">安装</a></li>
      </ul>
    </li>
    <li><a href="#使用说明">使用说明</a></li>
    <li><a href="#技术路线">技术路线</a></li>
    <li><a href="#关于我们">关于我们</a></li>
    <li><a href="#贡献">贡献</a></li>
    <li><a href="#License">License</a></li>
    <li><a href="#联系我们">联系我们</a></li>
  </ol>
</details>

<!-- 平台概述 -->
## 平台概述

### 界面预览

[![ASSETS-LOGIN][ASSETS-LOGIN]][ASSETS-LOGIN-url]
[![ASSETS-WORKBENCH][ASSETS-WORKBENCH]][ASSETS-WORKBENCH-url]
[![ASSETS-APPROVE-SUBMIT][ASSETS-APPROVE-SUBMIT]][ASSETS-APPROVE-SUBMIT-url]
[![ASSETS-APPROVE-LIST][ASSETS-APPROVE-LIST]][ASSETS-APPROVE-LIST-url]
[![ASSETS-APPROVE-EXECUTE][ASSETS-APPROVE-EXECUTE]][ASSETS-APPROVE-EXECUTE-url]
[![ASSETS-APPLICATION][ASSETS-APPLICATION]][ASSETS-APPLICATION-url]
[![ASSETS-APPLICATION-PAGE][ASSETS-APPLICATION-PAGE]][ASSETS-APPLICATION-PAGE-url]

### 特性

* **企业级产品/UI设计，满足各类业务场景需求。**
* **经历我司内部项目生产环境的反复锤炼，确保高效稳定运行。**
* **双技术架构，单体/微服务无缝切换。**
* **双业务架构，小型管理系统可使用后台管理模式，大型业务系统可使用Saas平台模式。**
* **全面的通用能力支撑，系统管理、文件服务、日志审计、任务调度等应有尽有。**
* **完整实现“中国式审批”的各类需求，开箱即用。**
* **高灵活度的消息中心，随心配置模板、多渠道（站内/微信/短信/钉钉）触达。**
* **深度集成第三方组件/平台，包括积木报表、OnlyOffice、KKFile。**
* **支持多种登录方式，包括短信验证码登录、微信扫码登录、微信小程序登录等。**
* **支持多种数据库，包括MySQL、Postgresql、Oracle。**
* **支持多种对象存储服务，包括Minio、腾讯COS、阿里OSS等。**
* ...

### 技术架构

* 单体/微服务无缝切换
[![TechArch][ASSETS-TECH-ARCH]][ASSETS-TECH-ARCH-url]

* 全模块化设计
[![ModuleArch][ASSETS-MODULE-ARCH]][ASSETS-MODULE-ARCH-url]

### 系统架构

[![SystemArch][ASSETS-SYSTEM-ARCH]][ASSETS-SYSTEM-ARCH-url]

<!-- GETTING STARTED -->
## 快速开始

### 准备工作

### 安装

<!-- 使用说明 -->
## 使用说明

<!-- 技术路线 -->
## 技术路线

### 前端

* [x] vuejs 3.4.3
* [x] vite 4.5.0
* [x] element plus 2.4.4
* [x] pinia 2.1.7
* [x] vue-router 4.2.5

### 后端

- [x] JDK 17
* [x] Maven 4.0.0
* [x] Spring Boot 3.1.7
* [x] Spring Cloud 2022.0.4
* [x] Spring Cloud Alibaba 2022.0.0.0
* [x] Mybatis-Plus 3.5.5
* [x] Redis 7.2
* [x] MySQL 8.0+/Postgresql 16+/Oracle 11g+12c
* [x] Minio/Tencent COS/Ali OSS
* [ ] RabbitMQ/RocketMQ
* [ ] Prometheus+Grafana

<!-- 关于我们 -->
## 关于我们

金合技术开发平台是陕西金合信息科技股份有限公司在内部孵化的技术开发平台，致力于减少项目交付在基础环境上消耗的工时，换而言之金合的技术开发平台是为项目交付而生，让项目不需要在底层的功能或环境上再消耗时间。<br />
目前金合技术中台已经支持交付了内外部40+项目，当前版本为首个开源版本，我们迫切的想听到您的改进意见和功能建议，希望更多的开发者可以参与改进平台，助力更多开发者和企业。

<!-- 贡献 -->
## 贡献

社区力量是开源项目持续发展的唯一动力来源，我们真诚的希望与您一起共建这片学习、交流、成长并且获益的“小宇宙”，您的每一个建议、每一次PR我们都**倍感珍惜**。

如果您对本项目有任何的想法，请您务必留下您的宝贵意见和建议，当然 `Talk is cheap, show me the code`，我们更欢迎您按照如下方式扩展本项目的功能！

1. Fork本仓库。
2. 创建您的功能分支 (`git checkout -b feature/awsome-feature`)。
3. 提交您的修改并推送到分支上 (`git commit -m 'Add some Awsome Feature'` && `git push origin feature/awsome-feature`)。
4. 提交PR。

<!-- LICENSE -->
## License

本项目基于 Apache License Version 2.0 开源协议，请遵守以下规范。

* 不得将本项目应用于危害国家安全、荣誉和利益的行为，不能以任何形式用于非法行为。
* 使用源代码时请保留源文件中的版权声明和作者信息。
* 任何基于本项目二次开发而产生的一切法律纠纷和责任，均与作者无关。

更多信息请参考项目根目录下的`LICENSE`文件。

<!-- 联系我们 -->
## 联系我们

<h3>技术交流群</h3>
<img src="https://platformdoc.ikingtech.com/image/doc/lll-qiyeweixin.jpg" width="200">
<br />
<br />
帖严 - tieybrain@gmail.com / ty19880929@hotmail.com

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[forks-shield]: https://gitee.com/ikingtech/iking-platform/badge/fork.svg?theme=dark
[forks-url]: https://gitee.com/ikingtech/iking-platform/members
[stars-shield]: https://gitee.com/ikingtech/iking-platform/badge/star.svg?theme=dark
[stars-url]: https://gitee.com/ikingtech/iking-platform/stargazers
[SpringBoot]: https://img.shields.io/badge/Spring_Boot-6DB33F?style=for-the-badge&logo=spring-boot&logoColor=white
[SpringBoot-url]: https://spring.io/projects/spring-boot
[Maven]: https://img.shields.io/badge/apache_maven-C71A36?style=for-the-badge&logo=apachemaven&logoColor=white
[Maven-url]: https://spring.io/projects/spring-boot
[Vuejs]: https://img.shields.io/badge/Vue%20js-35495E?style=for-the-badge&logo=vuedotjs&logoColor=4FC08D
[Vuejs-url]: https://cn.vuejs.org
[Vite]: https://img.shields.io/badge/Vite-B73BFE?style=for-the-badge&logo=vite&logoColor=FFD62E
[Vite-url]: https://vitejs.dev/
[TypeScript]: https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge&logo=typescript&logoColor=white
[TypeScript-url]: https://www.typescriptlang.org/
[OpenJDK]: https://img.shields.io/badge/OpenJDK-ED8B00?style=for-the-badge&logo=openjdk&logoColor=white
[OpenJDK-url]: https://openjdk.org/
[IDEA]: https://img.shields.io/badge/IntelliJ_IDEA-000000.svg?style=for-the-badge&logo=intellij-idea&logoColor=white
[IDEA-url]: https://www.jetbrains.com/zh-cn/idea/
[VSCODE]: https://img.shields.io/badge/VSCode-0078D4?style=for-the-badge&logo=visual%20studio%20code&logoColor=white
[VSCODE-url]: https://code.visualstudio.com/
[ASSETS-LOGIN]: assets/技术中台-登录.png
[ASSETS-LOGIN-url]: https://platform.ikingtech.com
[ASSETS-WORKBENCH]: assets/技术中台-工作台.png
[ASSETS-WORKBENCH-url]: https://platform.ikingtech.com
[ASSETS-APPROVE-SUBMIT]: assets/技术中台-审批中心-发起审批.png
[ASSETS-APPROVE-SUBMIT-url]: https://platform.ikingtech.com
[ASSETS-APPROVE-LIST]: assets/技术中台-审批中心-审批表单列表.png
[ASSETS-APPROVE-LIST-url]: https://platform.ikingtech.com
[ASSETS-APPROVE-EXECUTE]: assets/技术中台-审批中心-我审批的.png
[ASSETS-APPROVE-EXECUTE-url]: https://platform.ikingtech.com
[ASSETS-APPLICATION]: assets/技术中台-应用开发.png
[ASSETS-APPLICATION-url]: https://platform.ikingtech.com
[ASSETS-APPLICATION-PAGE]: assets/技术中台-应用开发-页面设计.png
[ASSETS-APPLICATION-PAGE-url]: https://platform.ikingtech.com
[ASSETS-MODULE-ARCH]: assets/技术中台-功能模块架构.png
[ASSETS-MODULE-ARCH-url]: https://platform.ikingtech.com
[ASSETS-TECH-ARCH]: assets/技术中台-技术架构.png
[ASSETS-TECH-ARCH-url]: https://platform.ikingtech.com
[ASSETS-SYSTEM-ARCH]: assets/技术中台-业务架构.png
[ASSETS-SYSTEM-ARCH-url]: https://platform.ikingtech.com
