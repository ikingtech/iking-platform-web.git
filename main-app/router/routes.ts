import { setupLayouts } from 'virtual:meta-layouts'
import generatedRoutes from 'virtual:generated-pages'
import type { RouteRecordRaw } from 'vue-router'
import useSettingsStore from '@main/store/modules/settings'
import { routes as MicroRoutes } from '@micro/router/routes'
import type { Route } from '#/global'

// 固定路由（默认路由）
const constantRoutes: RouteRecordRaw[] = [
  {
    path: '/login',
    name: 'login',
    component: () => import('@main/views/login/login.vue'),
    meta: {
      whiteList: true,
      title: '登录',
      i18n: 'route.login',
    },
  },
  {
    path: '/:all(.*)*',
    name: 'notFound',
    component: () => import('@main/views/[...all].vue'),
    meta: {
      title: '找不到页面',
    },
  },
  {
    path: '/config-page',
    name: 'ConfigPage',
    component: () => import('@main/views/config-page/index.vue'),
    meta: {
      whiteList: true,
      title: '项目配置',
    },
  },
  {
    path: '/icon-page',
    name: 'IconPage',
    component: () => import('@main/views/icon-page/index.vue'),
    meta: {
      whiteList: true,
      title: '图标库',
    },
  },
  ...(MicroRoutes || []),
]

// 系统路由
const systemRoutes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('@main/layouts/index.vue'),
    meta: {
      title: () => useSettingsStore().settings.home.title,
      breadcrumb: false,
    },
    children: [
      {
        path: '',
        component: () => import('@main/views/index.vue'),
        name: 'home',
        meta: {
          title: () => useSettingsStore().settings.home.title,
          i18n: 'route.home',
          icon: 'i-ant-design:home-twotone',
          breadcrumb: false,
        },
      },
      {
        path: 'reload',
        name: 'reload',
        component: () => import('@main/views/reload.vue'),
        meta: {
          title: '重新加载',
          breadcrumb: false,
        },
      },
      {
        path: 'approve-center/approve-list/waitapply',
        name: 'WaitApply',
        component: () => import('@main/views/approve-workflow/ApproveList/WaitApply.vue'),
        meta: {
          title: '审批申请',
          breadcrumb: true,
          breadList: ['approve-center/approve-list'],
          activeMenu: '/approve-center/approve-list',
        },
      },
      {
        path: 'approve-center/approve-configs/edit-form/:id?',
        name: 'EditForm',
        component: () => import('@main/views/approve-workflow/ApproveConfig/Tabs/TabForm.vue'),
        meta: {
          title: '审批流程配置',
          breadList: ['approve-center/approve-configs'],
          breadcrumb: true,
          activeMenu: '/approve-center/approve-configs',
        },
      },
      {
        path: 'news-center/news-template/add-msg-template',
        name: 'AddMsgTemplate',
        component: () => import('@main/views/news-center/news-template/NewsConfig.vue'),
        meta: {
          title: '消息模板配置',
          breadcrumb: true,
          activeMenu: '/news-center/news-template',
        },
      },
      {
        path: 'news-center/news-template/msg-channel-config',
        name: 'ConfigChannelConfig',
        component: () => import('@main/views/news-center/news-template/NewsChannelConfig.vue'),
        meta: {
          title: '模块配置',
          breadcrumb: true,
          activeMenu: '/news-center/news-template',
        },
      },
      {
        path: 'ui-testing',
        name: 'UiTesting',
        component: () => import('@main/views/ui-testing.vue'),
        meta: {
          title: 'UI组件效果测试',
          breadcrumb: true,
        },
      },
    ],
  },
]

// 动态路由（异步路由、导航栏路由）
const asyncRoutes: Route.recordMainRaw[] = []

const constantRoutesByFilesystem = generatedRoutes.filter((item) => {
  return item.meta?.enabled !== false && item.meta?.constant === true
})

const asyncRoutesByFilesystem = setupLayouts(generatedRoutes.filter((item) => {
  return item.meta?.enabled !== false && item.meta?.constant !== true && item.meta?.layout !== false
}))

export {
  constantRoutes,
  systemRoutes,
  asyncRoutes,
  constantRoutesByFilesystem,
  asyncRoutesByFilesystem,
}
