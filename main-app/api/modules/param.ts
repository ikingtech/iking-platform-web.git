/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-03-06 12:00:43
 * @LastEditTime : 2023-03-21 18:41:53
 */
import { ikHttp, textHeader } from '@main/api/request'

// 参数
export const paramApi = {
  // 获取所有参数
  getParams: async () => await ikHttp.post('/server/system/variable/list/all'),
  // 根据参数键名查询参数值
  queryParamByKey: async (variableKey: string) => await ikHttp.post('/server/system/variable/value/key', variableKey, textHeader),
  // 分页获取参数列表
  getParamList: async (params: any) => await ikHttp.post('/server/system/variable/list/page', params),
  // 新增
  addParams: async (params: any) => await ikHttp.post('/server/system/variable/add', params),
  // 修改
  updateParams: async (params: any) => await ikHttp.post('/server/system/variable/update', params),
  // 删除
  deleteParams: async (variableId: string) => await ikHttp.post('/server/system/variable/delete', variableId, textHeader),
}
