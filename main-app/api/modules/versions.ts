/*
 * @Author: zqf
 * @Date: 2024-04-16 12:14:19
 * @email: 13289847921@163.com
 * @LastEditors: zqf
 * @LastEditTime: 2024-04-17 14:20:43
 * @Description:
 */
import { ikHttp, textHeader } from '@main/api/request'

export const versionApi = {
  // 当前版本信息
  getVersion: async () => ikHttp.post('/charts/state'),
  // 激活
  activation: async (dictId: string) =>
    await ikHttp.post('/charts/activation', dictId, textHeader),
  // 复制机器码
  downMachineCode: async () => ikHttp.post('/charts/machine/code'),
  // 卸载license
  unloadEvent: async () => ikHttp.post('/charts/uninstall'),
}
