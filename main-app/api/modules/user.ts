/*
 * @Author       : wfl
 * @LastEditors: ww
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-02-28 11:45:50
 * @LastEditTime: 2023-05-24 16:04:35
 */
import { ikHttp, textHeader } from '@main/api/request'

export const userApi = {
  // 用户授权
  setAuthGrant: async () => await ikHttp.post('/server/authorization/grant/login-user'),
  // 更新密码
  updatePwd: async (data: any) => ikHttp.post('/server/system/user/password/modify', data),
  // 获取
  getUserList: async (params: any) => await ikHttp.post('/server/system/user/list/page', params),
  // 获取所有用户
  getAllUser: async () => await ikHttp.post('/server/system/user/info/list/all'),
  // 新增
  addUser: async (params: any) => await ikHttp.post('/server/system/user/add', params),
  // 更新
  updateUser: async (params: any) => await ikHttp.post('/server/system/user/update', params),
  // 删除
  deleteUser: async (userId: string) => await ikHttp.post('/server/system/user/delete', userId, textHeader),
  // 重置密码
  resetPassword: async (userId: string) => await ikHttp.post('/server/system/user/password/reset', userId, textHeader),
  // 锁定、解锁用户unlock
  lockedUser: async (bool: boolean, name: string) => {
    return await ikHttp.post(`/server/system/user/${bool ? 'lock' : 'unlock'}`, name, {
      headers: {
        'Content-Type': 'application/text;charset=UTF-8',
      },
    },

    )
  },
  getSetting: async () => await ikHttp.post('/server/system/user/config/list/login-user', null),
  setSetting: async (params: {
    id?: string
    userId: string
    configInfoList: Array<{ type: 'THEME' | 'LANGUAGE', value: string }>
    global: boolean
  }) => await ikHttp.post('/server/system/user/config/update', params),
  // 查询用户所属于的组织
  organizationList: async () => await ikHttp.post('/server/system/organization/list/organization/user-id'),
  // 查询部门
  treeDept: async (params: any) => await ikHttp.post('/server/system/dept/list/all', params),
}
