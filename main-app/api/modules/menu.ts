/*
 * @Author       : wfl
 * @LastEditors: ww
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-03-03 14:01:21
 * @LastEditTime: 2023-06-02 10:04:50
 */
import { ikHttp, textHeader } from '@main/api/request'

enum Api {
  GetMenuList = '/server/system/menu/list/login-user',
  UpdateMenu = '/server/system/menu/update',
  AddMenu = '/server/system/menu/add',
  DeleteMenu = '/server/system/menu/delete',
  GetAllMenu = '/server/system/menu/list/all',
  SortMenu = '/server/system/menu/drag',
  GetAllDiction = '/server/dict/item/map/all',
  Detail = '/server/system/menu/detail/',
}
export const menuApi = {
  // 获取当前用户的菜单列表
  getUserMenuList: async (showAll: any) => {
    const res = await ikHttp.post(Api.GetMenuList, showAll, {
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
      },
    })
    // 在此对页面所有菜单数据排序
    if (res.data) {
      res.data = res.data.sort((a: { sortOrder: number }, b: { sortOrder: number }) => a.sortOrder - b.sortOrder)
    }
    return res
  },
  getMenuTree: async () => await ikHttp.post(Api.GetMenuList),
  // 更新
  updateMenu: async (params: any) => await ikHttp.post(Api.UpdateMenu, params),
  // 新增
  addNewMenu: async (params: any) => await ikHttp.post(Api.AddMenu, params),
  // 删除
  deleteMenu: async (menuId: string) => await ikHttp.post(`${Api.DeleteMenu}`, menuId, textHeader),
  // 全量查询
  getAllMenu: async () => await ikHttp.post(Api.GetAllMenu),
  // 排序
  toSortMenu: async (params: any) => await ikHttp.post(Api.SortMenu, params),
  addTranslateConfig: async (params: any) => await ikHttp.post('', params),
  getTranslateConfig: async (params: any) => await ikHttp.post('', params),
  // 查询详情
  getMenuDetail: async (id: string) => await ikHttp.post(`${Api.Detail}/${id}`),

  getAllDiction: async () => await ikHttp.post('/server/system/dict/item/list/all'),
}

export default menuApi
