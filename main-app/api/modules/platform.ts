/*
 * @Author: zqf
 * @Date: 2024-04-18 16:13:45
 * @email: 13289847921@163.com
 * @LastEditors: zqf
 * @LastEditTime: 2024-04-29 10:50:46
 * @Description:
 */

import { ikHttp, textHeader } from '@main/api/request'

export const platformApi = {
  // 批量新增
  addBatch: async (params: any) =>
    await ikHttp.post('/platform-config/add/batch', params),
  // 查询
  query: async (type: string) =>
    await ikHttp.post('/platform-config/list/type', type, textHeader),
}
