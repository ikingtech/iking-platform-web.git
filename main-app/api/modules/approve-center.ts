/*
 * @Author       : wfl
 * @LastEditors: zqf
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-03-24 09:56:49
 * @LastEditTime: 2024-04-09 10:24:50
 */
import { ikHttp, textHeader } from '@main/api/request'

const TYPE_PLAIN = { headers: { 'Content-Type': 'text/plain' } }

export const formApi = {
  // 删除草稿
  deleteDraft: async (id: string) =>
    ikHttp.post('/server/approve/form/instance/draft/delete', id, TYPE_PLAIN),
  // 查询草稿
  queryDraft: async (param: any) =>
    ikHttp.post('/server/approve/form/instance/draft/page', param),
  // 保存草稿
  addDraft: async (param: any) =>
    ikHttp.post('/server/approve/form/instance/draft/add', param),
  getFormInfo: async (param?: any) =>
    ikHttp.post('/server/approve/form/list/form-group', param, {
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
      },
    }),
  getGroupList: async () => ikHttp.post('/server/approve/form/group/list/all'),
  // 获取已有表单模式下的表单列表
  getFormList: async () => ikHttp.post('/server/approve/form/register/list'),
  // 查询表单组件信息
  getFormWidgets: async (id: string) =>
    ikHttp.post('/server/approve/form/widget/list/form-id', id, TYPE_PLAIN),
  // 新增表单分组
  createGroup: async (data: { name: string }) =>
    ikHttp.post('/server/approve/form/group/add', data),
  // 重命名表单分组
  renameGroup: async (data: { id: string, name: string }) =>
    ikHttp.post('/server/approve/form/group/update', data),
  // 表单分组名称重复性检查
  checkGroupName: async (param: { id: string, name: string }) =>
    ikHttp.post('/server/approve/form/group/name-check', param),
  // 删除表单分组
  deleteGroup: async (id: string) =>
    ikHttp.post('/server/approve/form/group/delete', id, TYPE_PLAIN),
  // 排序
  sortGroup: async (data: any) =>
    ikHttp.post('/server/approve/form/group/drag', data),
  // 新增表单
  addForm: async (data: any) => ikHttp.post('/server/approve/form/add', data),
  // 修改表单
  editForm: async (data: any) =>
    ikHttp.post('/server/approve/form/update', data),
  // 拖拽表单排序
  sortForm: async (data: any) => ikHttp.post('/server/approve/form/drag', data),
  // 删除表单
  deleteForm: async (id: string) =>
    ikHttp.post('/server/approve/form/delete', id, TYPE_PLAIN),
  // 修改表单实例
  editInstance: async (data: any) =>
    ikHttp.post('/server/approve/form/instance/update', data),
  // 停用
  blockUp: async (id: string) =>
    ikHttp.post('/server/approve/form/disable', id, TYPE_PLAIN),
  // 启用
  enableForm: async (data: any) =>
    ikHttp.post('/server/approve/form/enable', data),
  // 移动
  moveForm: async (data: any) => ikHttp.post('/server/approve/form/move', data),
  // 更新发起人
  editInitiator: async (param: any) =>
    ikHttp.post('/server/approve/form/initiator/update', param),
  // 撤销修改
  revokeEditForm: async (formInstanceId: string) =>
    ikHttp.post('/server/approve/form/instance/update/cancel', {
      formInstanceId,
    }),
  // 撤销
  revokeForm: async (formInstanceId: string) =>
    ikHttp.post('/server/approve/form/instance/revoke', { formInstanceId }),
  // 中止撤销
  abortRevokeForm: async (formInstanceId: string) =>
    ikHttp.post('/server/approve/form/instance/revoke/cancel', {
      formInstanceId,
    }),
  // 催办
  urgeApprove: async (formInstanceId: string) =>
    ikHttp.post('/server/approve/form/instance/remind', { formInstanceId }),
  // {
  //  "formInstanceId": "",
  //  "comment": "",
  //  "attachment": ""
  // }
  // 评论
  commentApprove: async (param: any) =>
    ikHttp.post('/server/approve/form/instance/comment', param),
  // {
  //   "formInstanceId": "",
  //   "comment": "",
  //   "attachment": "",
  //   "images": []
  // }
  // 同意
  agreeApprove: async (param: any) =>
    ikHttp.post('/server/approve/form/instance/pass', param),
  // 拒绝 - 参数同 同意
  refuseApprove: async (param: any) =>
    ikHttp.post('/server/approve/form/instance/reject', param),
  // {
  //   "formInstanceId": "",
  //   "backToNodeId": "",
  //   "comment": "",
  //   "attachment": "",
  //   "images": []
  // }
  // 退回
  backApprove: async (param: any) =>
    ikHttp.post('/server/approve/form/instance/back', param),
  // {
  //   "formInstanceId": "",
  //   "appendType": "",
  //   "appendExecutorUserIds": "",
  //   "comment": "",
  //   "attachment": "",
  //   "images": []
  // }
  // 加签
  addApprove: async (param: any) =>
    ikHttp.post('/server/approve/form/instance/back', param),

  // 表单详情
  getFormDetail: async (id: string) =>
    ikHttp.post('/server/approve/form/detail/id', id, TYPE_PLAIN),
  // 表单预览详情
  getFormPreview: async (param: any) =>
    ikHttp.post('/server/approve/form/instance/preview', param),

  // 操作类
  // 下载表单提交数据
  downloadFormData: async (id: string) =>
    ikHttp.post('/server/approve/form/instance/download', id, TYPE_PLAIN),

  // 审批
  // 提交审批
  submitApprove: async (param: any) =>
    ikHttp.post('/server/approve/form/instance/submit', param),
  reSubmitApprove: async (param: any) =>
    ikHttp.post('/server/approve/form/instance/re-submit', param),
  // 保存草稿
  saveDraft: async (param: any) => ikHttp.post('', param),
  //
}

// 高级设置相关接口
export const advanceApi = {
  // 迁移报表模板数据
  migrateJmreport: async (id: string) => {
    // 849564219620032512
    // 849564219620032512
    // 849564219620032500
    return ikHttp.post('/server/jmreport/management/migrate', id, textHeader)
  },
  // 绑定表单报表模板
  bindReportTemplate: async (params: any) => ikHttp.post('/server/approve/form/report-template/bind', params),

}
