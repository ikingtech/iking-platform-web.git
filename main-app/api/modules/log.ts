/*
 * @Author       : wfl
 * @LastEditors: ww
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-02-28 11:45:50
 * @LastEditTime: 2023-05-24 10:18:14
 */
import { ikHttp } from '@main/api/request'

// 日志
export const logApi = {
  // 查询登录日志
  getPageLogs: async (params: any) =>
    await ikHttp.post('/server/log/auth/list/page', params),
  // 查询操作日志
  getOperateLogs: async (params: any) =>
    await ikHttp.post('/server/log/operation/list/page', params),
  // 查询操作日志
  getSystemLogs: async (params: any) =>
    await ikHttp.post('/server/log/system/list/page', params),
  getLogDetail: async (id: any) =>
    await ikHttp.post('/server/log/system/detail/id', id, {
      headers: {
        'Content-Type': 'application/text;charset=UTF-8',
      },
    }),
}
