/*
 * @Author       : wfl
 * @LastEditors: qiye
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-03-06 12:00:43
 * @LastEditTime: 2024-02-04 14:35:03
 */
import { ikHttp, textHeader } from '@main/api/request'

// 数据源
export const datasourceApi = {
  // 获取所有数据源
  getDatasource: async () => await ikHttp.post('/server/datasource/list/all'),
  // 分页获取数据源列表
  getDatasourceList: async (params: any) => await ikHttp.post('/server/datasource/list/page', params),
  // 新增
  addDatasources: async (params: any) => await ikHttp.post('/server/datasource/add', params),
  // 修改
  updateDatasource: async (params: any) => await ikHttp.post('/server/datasource/update', params),
  // 删除
  deleteDatasource: async (variableId: string) => await ikHttp.post('/server/datasource/delete', variableId, textHeader),
  // 测试链接
  testLink: async (params: any) => await ikHttp.post('/server/datasource/connection/test', params),
  // 获取数据库类型
  getDatabaseType: async () => await ikHttp.get('/server/datasource/list/database/type'),
  // 测试链接
  getALLCurrent: async (params: any) => await ikHttp.post('/server/datasource/schema/list', params),
}
