/*
 * @Author       : wfl
 * @LastEditors: qiye
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-03-06 12:02:04
 * @LastEditTime: 2024-02-02 13:48:32
 */
import { ikHttp, textHeader } from '@main/api/request'

// 租户
export const tenantApi = {
  // 获取所有租户
  getTenant: async () => await ikHttp.post('/server/tenant/list/all'),
  // 根据参数键名查询租户值
  queryTenantById: async (tenantId: string) => await ikHttp.post('/server/tenant/detail/code', tenantId, textHeader),
  // 分页获取租户列表
  getTenantList: async (params: any) => await ikHttp.post('/server/tenant/list/page', params),
  // 新增
  addTenant: async (params: any) => await ikHttp.post('/server/tenant/add', params),
  // 修改
  updateTenant: async (params: any) => await ikHttp.post('/server/tenant/update', params),
  // 删除
  deleteTenant: async (tenantCode: string) => await ikHttp.post('/server/tenant/delete', tenantCode, textHeader),
  // 重置密码
  resetPasswordTenant: async (tenantCode: string) => await ikHttp.post('/server/tenant/admin/password', tenantCode, {
    headers: {
      'Content-Type': 'application/text;charset=UTF-8',
    },
  }),
  // 重置密码
  retryTenant: async (tenantId: string) => await ikHttp.get(`/server/tenant/retry?tenantId=${tenantId}`),
  // old queryTenantList: async () => await ikHttp.get('/server/tenant/list/current-user'),
  queryTenantList: async () => await ikHttp.post('/server/tenant/list/login-user'),
  // 新增用户租户登录信息
  addUserTenant: async (params: any) => await ikHttp.post('/server/system/user/recent/tenant', params, {
    headers: {
      'Content-Type': 'application/text;charset=UTF-8',
    },
  }),
  getLoginUser: async () => await ikHttp.post('/server/authorization/grant/login-user'),
}
