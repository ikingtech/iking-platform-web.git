/*
 * @Author       : wfl
 * @LastEditors: zqf
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-02-28 11:45:50
 * @LastEditTime: 2024-04-11 15:51:22
 */
import { ikHttp, textHeader } from '@main/api/request'

export const depApi = {
  getAllDeps: async (deptId?: any, excludeId?: string) => {
    return await ikHttp.post(
      `/server/system/dept/info/list/all${
        excludeId ? `?excludeId=${excludeId}` : ''
      }`,
      deptId,
      textHeader,
    )
  },
  // 获取修改时父级部门列表
  getUpdateDepts: async (deptId: string) =>
    await ikHttp.post(
      `/server/system/dept/info/list/all?excludeId=${deptId}`,
      deptId,
    ),
  // 获取部门树型列表
  getDepTree: async () =>
    ikTree.listToTree((await depApi.getAllDeps())?.data || []),
  // 新增部门
  addDep: async (params: any) =>
    await ikHttp.post('/server/system/dept/add', params),
  // 查询部门详情
  DepDetails: async (deptId: string) =>
    await ikHttp.post('/system/dept/detail/id', deptId, textHeader),
  // 修改
  updateDep: async (params: any) =>
    await ikHttp.post('/server/system/dept/update', params),
  // 删除
  deleteDep: async (deptId: string) =>
    await ikHttp.post('/server/system/dept/delete', deptId, textHeader),
  // 排序
  sortTree: async (params: any) =>
    await ikHttp.post('/server/system/dept/drag', params),
  // 获取子单位
  getChildDep: async (parentId: string) =>
    await ikHttp.post('/server/system/dept/sub/all/list', {
      parentDeptId: parentId,
    }),
}
