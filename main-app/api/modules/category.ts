/*
 * @Author: zqf
 * @Date: 2024-03-12 17:20:22
 * @email: 13289847921@163.com
 * @LastEditors: zqf
 * @LastEditTime: 2024-03-14 17:16:24
 * @Description:
 */
import { ikHttp } from '@main/api/request'

export const categoryApi = {
  // 全量身份列表
  getAllCategory: async () =>
    await ikHttp.post('/system/user/category/list/login-user'),
}
