/*
 * @Author       : tg
 * @LastEditors  : tg
 * @description  : 组织管理接口
 * @updateInfo   :
 * @Date         : 2023-10-20 11:45:50
 * @LastEditTime : 2023-10-20 11:25:18
 */
import { ikHttp, textHeader } from '@main/api/request'

export const organizaApi = {
  getAllDeps: async (params?: any) => {
    return await ikHttp.post('/server/system/organization/list/all', params, {
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
      },
    })
  },
  //   // 获取部门树型列表
  getDepTree: async () => ikTree.listToTree((await organizaApi.getAllDeps())?.data || []),
  // 新增
  addOrganiza: async (params: any) => await ikHttp.post('/server/system/organization/add', params),
  // 修改
  updateOrganiza: async (params: any) => await ikHttp.post('/server/system/organization/update', params),
  // 停用
  deactiveId: async (id: string) => await ikHttp.post('/server/system/organization/deactive/id', id, textHeader),
  // 启用
  activeId: async (id: string) => await ikHttp.post('/server/system/organization/active/id', id, textHeader),
  // 删除
  deleteId: async (id: string) => await ikHttp.post('/server/system/organization/delete', id, textHeader),
  // 查询详情
  detailId: async (id: string) => await ikHttp.post('/server/system/organization/detail/id', id, textHeader),
  // 全量查询-全量查询(仅省市)
  cityList: async () => await ikHttp.post('/server/system/division/list/province-city'),
  // 全量查询-查询行政区划(父编号)
  parentId: async (id: string) => await ikHttp.post('/server/system/division/list/parent-id', id, textHeader),
  // 排序
  sortTree: async (params: any) => await ikHttp.post('/server/system/organization/drag', params),
  // 获取子单位
  getChildDep: async (params: any) => await ikHttp.post('/server/system/organization/list/page', params),
  // 员工花名册
  organizationId: async (params: any) => await ikHttp.post('/server/system/organization/list/user/organization/page', params),
  // 标签管理
  // -列表
  listPage: async (params: any) => await ikHttp.post('/server/system/label/list/page', params),
  // -更新
  labelUpdate: async (params: any) => await ikHttp.post('/server/system/label/update', params),
  // -新增
  labelAdd: async (params: any) => await ikHttp.post('/server/system/label/add', params),
  // -删除
  labelDelete: async (id: any) => await ikHttp.post('/server/system/label/delete', id, textHeader),
  // -批量删除
  batchDelete: async (params: any) => await ikHttp.post('/server/system/label/batch/delete', params),
}
