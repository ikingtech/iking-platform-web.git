/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-04-13 14:08:59
 * @LastEditTime : 2023-05-19 08:59:01
 */
import { ikHttp } from '@main/api/request'
export const commonApi = {
  // 混合选择组件
  getAllComData: async (param?: any) => await ikHttp.post('/server/component/pick/mix', param),
  // 部门选择组件
  getDepData: async (param?: any) => await ikHttp.post('/server/component/pick/department', param),
  // 角色选择组件
  getRoleData: async (param?: any) => await ikHttp.post('/server/component/pick/role', param),
  // 岗位选择组件
  getPostData: async (param?: any) => await ikHttp.post('/server/component/pick/post', param),

  // 获取顶部标语
  getSloganData: async (param?: any) => await ikHttp.post('', param),
}
