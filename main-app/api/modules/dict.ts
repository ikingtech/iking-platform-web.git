/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-02-28 11:45:50
 * @LastEditTime : 2023-03-21 18:39:03
 */
import { ikHttp, textHeader } from '@main/api/request'

export const dictApi = {
  // 获取字典
  getDictionary: async () => await ikHttp.post('/server/system/dict/list/all'),
  // 根据ID查询字典
  getDictById: async (dictId: string) =>
    await ikHttp.post(`/server/system/dict/item/detail/${dictId}`),
  // 添加字典
  addDictItem: async (params: any) => await ikHttp.post('/server/system/dict/add', params),
  // 更新
  updateDictItem: async (params: any) => await ikHttp.post('/server/system/dict/update', params),
  // 删除
  deleteDictItem: async (dictId: string) => await ikHttp.post('/server/system/dict/delete', dictId, textHeader),
  // 添加字典项
  addDictContent: async (params: any) => await ikHttp.post('/server/system/dict/item/add', params),
  // 更新字典项
  updateDictContent: async (params: any) => await ikHttp.post('/server/system/dict/item/update', params),
  // 删除字典项
  deleteDictContent: async (dictConId: string) => await ikHttp.post('/server/system/dict/item/delete', dictConId, textHeader),
  // 通过字典获取字典项
  getDictContentByItem: async (dictId: string) => await ikHttp.post('/server/system/dict/item/list/dict-id', dictId, textHeader),
}
