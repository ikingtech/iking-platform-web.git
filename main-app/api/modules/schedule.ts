/*
 * @Author       : qiye
 * @LastEditors: qiye
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-02-28 11:45:50
 * @LastEditTime: 2023-12-06 17:24:57
 */
import { ikHttp, textHeader } from '@main/api/request'

export const api = {
  // 添加
  add: async (params: any) => await ikHttp.post('/server/schedule/add', params),
  // 删除
  del: async (id: any) => await ikHttp.post('/server/schedule/delete', id, textHeader),
  // 改
  update: async (param: any) => await ikHttp.post('/server/schedule/update', param),
  // 查
  select: async () => await ikHttp.post('/server/schedule/list/all'),
  // 按日期查询日程数量
  countDate: async (params: any) => await ikHttp.post('/schedule/count/date', params),

}
