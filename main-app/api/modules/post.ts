/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-02-28 11:45:50
 * @LastEditTime : 2023-03-21 18:33:51
 */
import { ikHttp, textHeader } from '@main/api/request'

export const postApi = {
  // 全量获取岗位列表
  getAllPosts: async () => await ikHttp.post('/server/system/post/list/all'),
  // 分页获取岗位列表
  getPagePosts: async (params: any) =>
    await ikHttp.post('/server/system/post/list/page', params),
  // 添加
  addPost: async (params: any) =>
    await ikHttp.post('/server/system/post/add', params),
  // 更新
  updatePost: async (params: any) =>
    await ikHttp.post('/server/system/post/update', params),
  // 删除
  deletePost: async (postId: string) => await ikHttp.post('/server/system/post/delete', postId, textHeader),
  // 排序
  sortPost: async (params: any) =>
    await ikHttp.post('/server/system/post/drag', params),
}
