/*
 * @Author       : qiye
 * @LastEditors: zqf
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-02-28 11:45:50
 * @LastEditTime: 2024-04-19 10:37:29
 */
import { ikHttp, textHeader } from '@main/api/request'
import { api as schedule } from './schedule'

/**
 * 应用中心
 */
export const applicationApi = {
  // // 添加
  // add: async (params: any) => await ikHttp.post('/server/schedule/add', params),
  // // 删除
  // del: async (id: any) => await ikHttp.post('/schedule/delete', id, textHeader),
  // // 改
  // update: async (param: any) => await ikHttp.post('/schedule/update', param),
  // 查
  select: async (params: any) =>
    await ikHttp.post('/server/application/list/page', params),
  // 新增快捷入口
  addQuick: async (id: any) =>
    await ikHttp.post('/application/quick-entry/add', id, textHeader),
  // 查询快捷入口应用
  selectQuick: async () =>
    await ikHttp.post('/application/quick-entry/list/login-user'),
  // 移除快捷入口应用
  deleteQuick: async (id: any) =>
    await ikHttp.post('/application/quick-entry/delete', id, textHeader),
}

/**
 * 我的待办
 */

export const taskApi = {
  select: async (params: any) => await ikHttp.post('/server/workbench/todo-task/list/page', params),
}

/**
 * 我的日程
 */
export const scheduleApi = {
  select: async (params: any) => await ikHttp.post('server/schedule/list/page', params),
  add: schedule.add,
  update: schedule.update,
  del: schedule.del,
  countDate: schedule.countDate,
}
