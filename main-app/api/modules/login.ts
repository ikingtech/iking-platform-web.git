/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-02-28 11:45:50
 * @LastEditTime : 2023-12-05 14:38:12
 */

import { ikHttp, textHeader } from '@main/api/request'

export const loginApi = {
  login: async (data: any, config: any | undefined) => ikHttp.post('/auth/sign', data, config),
  loginOut: async () => ikHttp.post('/auth/sign-out'),
  getUserInfo: async (id: string) => await ikHttp.post('/server/system/user/detail/id', id, textHeader),
  getPageCssFilter: async () => await ikHttp.post('/server/system/user/config/front-gray-scale/value'),
  getUserBaseInfoByToken: async () => await ikHttp.post('/server/system/user/info/login-user'),
}
