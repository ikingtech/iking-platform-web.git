/*
 * @Author       : wfl
 * @LastEditors: zqf
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-02-28 11:45:50
 * @LastEditTime: 2024-03-14 16:29:58
 */
import { ikHttp, textHeader } from '@main/api/request'

export const roleApi = {
  // 获取所有权限
  getRoleList: async () => await ikHttp.post('/server/system/role/list/all'),
  // 添加
  addRole: async (params: any) => await ikHttp.post('/server/system/role/add', params),
  // 更新
  updateRole: async (params: any) => await ikHttp.post('/server/system/role/update', params),
  // 删除
  deleteRole: async (roleId: any) => await ikHttp.post('/server/system/role/delete', roleId, textHeader),
  // 获取角色下菜单、按钮
  getRoleMenus: async (roleId: any) => await ikHttp.post('/server/system/menu/list/role-id', roleId, textHeader),
  // 更新角色下菜单、按钮
  updateRoleMenus: async (params: any) => await ikHttp.post('/server/system/role/menu/update', params),
}
/** 角色用户 api */
export const roleUserApi = {
  // 分配角色用户
  add: async (params: any) => await ikHttp.post('/server/system/role/user/add', params),
  // 移除角色用户
  remove: async (params: any) => await ikHttp.post('/server/system/role/user/remove', params),
  // 查询
  select: async (params: any) => await ikHttp.post('/server/system/user/list/page', params),
}
