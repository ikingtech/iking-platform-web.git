/*
 * @Author       : wfl
 * @LastEditors: zqf
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-06-30 14:33:47
 * @LastEditTime: 2024-04-29 12:05:15
 */
import { ikHttp, textHeader } from '@main/api/request'
export const msgApi = {
  /**
   * @description: 消息相关
   * @return {*}
   */
  // 获取业务类型
  getBusiness: async (params?: any) =>
    await ikHttp.post('/server/message/template/business-type/list', params),
  // 更新消息模板
  updateMsgTemplate: async (params: any) =>
    await ikHttp.post('/server/message/template/update', params),
  // 分页查询消息模板列表
  getPageTemplate: async (params: any) =>
    await ikHttp.post('/server/message/template/list/page', params),
  // 根据业务类型获取消息模板标识Map
  getMsgTemplateByBusiness: async (businessName: string) =>
    await ikHttp.post(
      '/server/message/template/info/list/business-key',
      businessName,
      textHeader,
    ),
  // 获取消息渠道及配置信息
  getMsgChannelByNo: async (no: string) =>
    await ikHttp.post(
      '/server/message/template/channel/definition/preview/template-id',
      no,
      textHeader,
    ),
  // 新增消息体定义
  insertMsgTemplate: async (params: any) =>
    await ikHttp.post(
      '/server/message/template/channel/definition/add',
      params,
    ),
  // 更新消息体定义
  updateMsgChannelTemplate: async (params: any) =>
    await ikHttp.post(
      '/server/message/template/channel/definition/update',
      params,
    ),
  // 预览消息渠道列表
  getMsgChannelPreview: async (no: string) =>
    await ikHttp.post(
      '/server/message/template/channel/definition/preview/template-id',
      no,
      textHeader,
    ),
  // 查询消息模板详情
  getMsgChannelDetail: async (no: string) =>
    await ikHttp.post('/server/message/template/detail/id', no, textHeader),
  // 更新消息模板状态
  changeMsgTemplateStatus: async (params: any, enable: boolean) =>
    await ikHttp.post(
      enable
        ? '/server/message/template/channel/definition/enable'
        : '/message/template/channel/definition/disable',
      params,
      textHeader,
    ),
  // 删除消息模板
  deleteMsgTemplate: async (no: string) =>
    await ikHttp.post('/server/message/template/delete', no, textHeader),
  /**
   * @description: 微信小程序相关
   * @return {*}
   */
  // 全量查询微信小程序
  getWxListApi: async () =>
    await ikHttp.post(
      '/platform-config/list/type',
      'WECHAT_MINI_CONFIG',
      textHeader,
    ),
  // 获取订阅消息模板
  getWxTemplateApi: async (no: string) =>
    await ikHttp.post(
      '/wechat/mini/subscribe-message-template/list',
      no,
      textHeader,
    ),

  // 分页查询消息列表
  getPageMsg: async (params: any) =>
    await ikHttp.post('/server/message/page', params),
  // 消息已读(单条)
  tagMsg: async (id: string) =>
    await ikHttp.get(`/server/message/read?id=${id}`),
  // 消全部已读
  tagMsgs: async () => await ikHttp.get('/server/message/read/all'),
  // 发送消息
  sendMsg: async (params: any) =>
    await ikHttp.post('/server/message/send', params),
  // 过消息模板编号获取消息模板信息
  infoByTemplateId: async (id: string) =>
    await ikHttp.get(`/server/message/template?messageTemplateId=${id}`),
  // 全量获取消息模板列表
  getTemplates: async () => await ikHttp.get('/server/message/template/all'),
  // 根据消息标识获取消息模板预定义信息
  getTempinfoByMsg: async (messageKey: string) =>
    await ikHttp.get(`/server/message/template/key?messageKey=${messageKey}`),
  // 获取微信小程序列表
  getWxPlatform: async () => await ikHttp.get('/server/message/wechat-mini'),
  // 获取微信小程序消息名称列表
  getWxTemplate: async (miniAppId: string) =>
    await ikHttp.get(
      `/server/message/wechat-mini/message-templates?miniAppId=${miniAppId}`,
    ),

  // 分页查询消息管理列表
  getManagementList: async (params: any) =>
    await ikHttp.post('/server/message/management/list/page', params),
  // 更新消息状态为已读/message/management/read
  markRead: async (params: any) =>
    await ikHttp.post('/server/message/management/read/id', params, textHeader),
  // 分页查询消息管理列表
  markReadAll: async () =>
    await ikHttp.post('/server/message/management/read/all'),

  /**
   * @description:  短信
   * @return {*}
   */
  // 全量查询(有条件)
  getSmsTemplateList: async (params: any) =>
    await ikHttp.post('/server/sms/list/all/condition', params),
  // 获取模板
  getSmsTemplateApi: async (no: string) =>
    await ikHttp.post('/server/sms/template/list', no, textHeader),

  /**
   * @description: 微信
   * @return {*}
   */
  // 分页查询微信小程序消息
  getWechatMsgList: async (params: any) =>
    await ikHttp.post('/server/wechat/mini/list/page', params),
  // 新增微信小程序消息
  addWechatMsg: async (params: any) =>
    await ikHttp.post('/server/wechat/mini/add', params),
  // 修改微信小程序消息
  updateWechatMsg: async (params: any) =>
    await ikHttp.post('/server/wechat/mini/update', params),
  // 删除微信小程序
  deleteWechatMsg: async (no: string) =>
    await ikHttp.post('/server/wechat/mini/delete', no, textHeader),
}
