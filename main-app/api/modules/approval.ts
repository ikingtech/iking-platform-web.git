/*
 * @Author       : wfl
 * @LastEditors: zqf
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-06-30 16:35:54
 * @LastEditTime: 2024-03-27 16:29:48
 */
import { ikHttp } from '@main/api/request'
export const approvalApi = {
  // 列表数据
  getListData: async (param?: any) =>
    await ikHttp.post('/server/approve/form/instance/list/page', param),
  getDetailData: async (param?: any) =>
    await ikHttp.post('/server/approve/form/instance/detail/id', param, {
      headers: {
        'Content-Type': 'application/text;charset=UTF-8',
      },
    }),
  passApproval: async (param?: any) =>
    await ikHttp.post('/server/approve/form/instance/pass', param),
  cancelApproval: async (param?: any) =>
    await ikHttp.post('/server/approve/form/instance/update/cancel', param),
  revokeApproval: async (param?: any) =>
    await ikHttp.post('/server/approve/form/instance/revoke', param),
  remindApproval: async (param?: any) =>
    await ikHttp.post('/server/approve/form/instance/remind', param),
  commentApproval: async (param?: any) =>
    await ikHttp.post('/server/approve/form/instance/comment', param),
  rejectApproval: async (param?: any) =>
    await ikHttp.post('/server/approve/form/instance/reject', param),
  backApproval: async (param?: any) =>
    await ikHttp.post('/server/approve/form/instance/back', param),
  appendApproval: async (param?: any) =>
    await ikHttp.post('/server/approve/form/instance/append', param),
  executedNode: async (param?: any) =>
    await ikHttp.post('/server/approve/form/instance/executed-node/list/id', param, {
      headers: {
        'Content-Type': 'application/text;charset=UTF-8',
      },
    }),
  resubmitApproval: async (param?: any) =>
    await ikHttp.post('/server/approve/form/instance/re-submit', param),
  // 标记为已读
  handReaded: async (param?: any) => await ikHttp.post('/server/approve/form/instance/carbon-copy/read', param),
}
