/*
 * @Author       : wfl
 * @LastEditors: wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-02-29 14:41:18
 * @LastEditTime: 2024-05-06 18:10:01
 */
import axios from 'axios'
import router from '@main/router'

import useSettingsStore from '@main/store/modules/settings'
import useUserStore from '@main/store/modules/user'

const { remberRoute } = systemConfig

const { msgWarning } = useMessage()

function toLogin(statu = true) {
  const query
    = statu && remberRoute
      ? {
          redirect:
            router.currentRoute.value.path !== '/login'
              ? router.currentRoute.value.fullPath
              : undefined,
        }
      : {}
  router.push({
    path: '/login',
    query,
  })
}

const baseURL
  = import.meta.env.DEV && import.meta.env.VITE_OPEN_PROXY === 'true'
    ? '/api/'
    : import.meta.env.VITE_APP_API_BASEURL

const api = axios.create({
  baseURL,
  timeout: 1000 * 20,
  responseType: 'json',
})

api.interceptors.request.use((request) => {
  // 全局拦截请求发送前提交的参数
  const settingsStore = useSettingsStore()
  const userStore = useUserStore()
  if (request.url?.startsWith('http')) {
    request.baseURL = ''
  }
  else {
    request.baseURL = baseURL
  }
  // 设置公共请求头
  if (request.headers) {
    request.headers['Accept-Language'] = settingsStore.settings.app.defaultLang
    request.headers.Authorization
      = userStore.token || ikStore.local.getItem(ELocal.TOKEN)
    // 设置域标识
    setDomainHeader(request.headers)
    // if (userStore.isLogin) {
    //
    // }
  }
  // 是否将 POST 请求参数进行字符串化处理
  if (request.method === 'post') {
    // request.data = qs.stringify(request.data, {
    //   arrayFormat: 'brackets',
    // })
  }
  return request
})

api.interceptors.response.use(
  (response: any) => {
    /**
     * 尝试根据响应判断是否为下载类型，并返回相应的Promise对象。
     * @param response 原始响应对象，预期包含config和data属性。
     * @returns 如果响应数据的特定类型标记为"DOWNLOAD"，则返回一个解析为该响应的Promise；否则不返回任何内容。
     */
    try {
      // 尝试解析响应数据，检查是否标记为下载类型
      if (JSON.parse(response?.config?.data || '{}')?.__TYPE__ === 'DOWNLOAD') {
        return Promise.resolve(response)
      }
    }
    catch (error) {} // 捕获并忽略在解析过程中发生的任何错误
    /**
     * 全局拦截请求发送后返回的数据，如果数据有报错则在这做全局的错误提示
     */
    if (
      response.status === 200
      || response.data.status === 1
      || response.data.code === 0
    ) {
      if (
        !excludeUrl.includes(response?.config?.url)
        && !response.data.success
      ) {
        // 非返回文件流时提示错误信息
        response?.data?.msg && msgWarning(response.data.msg)
        return Promise.resolve(response.data)
      }
    }
    else {
      // TODO：返回登录页的条件
      useUserStore().logout()
    }
    try {
      if (!response.data.success) {
        msgWarning(response?.data?.msg || '登录失效, 即将返回登录页面')
      }
    }
    catch (error) {

    }
    return Promise.resolve(response.data)
  },
  (error) => {
    let message = error.message
    if (message === 'Network Error') {
      msgWarning('网络错误，请稍后重试')
    }
    else if (message.includes('timeout')) {
      msgWarning('请求超时，请稍后重试')
      toLogin()
      return Promise.reject(error)
    }
    // 登录过期，返回登录页
    else if (message.includes('401')) {
      message = '登录失效, 即将返回登录页面'
      useUserStore().logout()
    }
    else if (message.includes('Request failed with status code')) {
      message = `请求[${error?.config?.url}]${message.substr(
        message.length - 3,
      )}异常`
      msgWarning(message)
      if (location.hash === '#/') {
        // TODO
        // useUserStore().logout()
        // ikStore.local.clear()
      }
    }
    else {
      msgWarning(message)
    }
    return Promise.reject(error)
  },
)

export default api
