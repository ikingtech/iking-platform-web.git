/*
 * @Author       : wfl
 * @LastEditors: zqf
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-02-29 14:41:18
 * @LastEditTime: 2024-03-25 15:23:31
 */
import axios from 'axios'
import router from '@main/router'

import useSettingsStore from '@main/store/modules/settings'
import useUserStore from '@main/store/modules/user'

const { remberRoute } = systemConfig

const { msgWarning } = useMessage()

function toLogin(statu = true) {
  // useUserStore().clearCache()
  const query
    = statu && remberRoute
      ? {
          redirect:
            router.currentRoute.value.path !== '/login'
              ? router.currentRoute.value.fullPath
              : undefined,
        }
      : {}
  router.push({
    path: '/login',
    query,
  })
  // location.href = `${location.href}`
}

const api = axios.create({
  baseURL:
    import.meta.env.DEV && import.meta.env.VITE_OPEN_PROXY === 'true'
      ? '/api/'
      : import.meta.env.VITE_APP_API_BASEURL,
  timeout: 1000 * 20,
  responseType: 'json',
})

api.interceptors.request.use((request) => {
  // 全局拦截请求发送前提交的参数
  const settingsStore = useSettingsStore()
  const userStore = useUserStore()
  // 设置公共请求头
  if (request.headers) {
    request.headers['Accept-Language'] = settingsStore.settings.app.defaultLang
    request.headers.Authorization
      = userStore.token || ikStore.local.getItem(ELocal.TOKEN)
    if (userStore.isLogin) {
      // const menuId = ikStore.local.getItem('menuid')
      // request.headers['menu-id'] = menuId
      // request.headers['X-TENANT']
      // = userStore?.tenant?.code || ikStore.local.getItem(ELocal.TENANT)?.code
      // request.headers['X-ORG']
      // = userStore?.organiza?.select || ikStore.local.getItem(ELocal.ORGANIZA)?.select
    }

    // 设置域标识
    setDomainHeader(request.headers)
  }
  // 是否将 POST 请求参数进行字符串化处理
  if (request.method === 'post') {
    // request.data = qs.stringify(request.data, {
    //   arrayFormat: 'brackets',
    // })
  }
  return request
})

api.interceptors.response.use(
  (response: any) => {
    /**
     * 全局拦截请求发送后返回的数据，如果数据有报错则在这做全局的错误提示
     */
    if (
      response.status === 200
      || response.data.status === 1
      || response.data.code === 0
    ) {
      if (
        !excludeUrl.includes(response?.config?.url)
        && !response.data.success
      ) {
        msgWarning(response.data.msg)
        return Promise.resolve(response.data)
      }
    }
    else {
      // TODO：返回登录页的条件
      useUserStore().logout()
    }
    return Promise.resolve(response.data)
  },
  (error) => {
    let message = error.message
    if (message === 'Network Error') {
      msgWarning('网络错误，请稍后重试')
    }
    else if (message.includes('timeout')) {
      msgWarning('请求超时，请稍后重试')
      toLogin()
      return Promise.reject(error)
    }
    // 登录过期，返回登录页
    else if (message.includes('401')) {
      message = '登录失效, 即将返回登录页面'
      useUserStore().logout()
    }
    else if (message.includes('Request failed with status code')) {
      message = `请求[${error?.config?.url}]${message.substr(
        message.length - 3,
      )}异常`
      msgWarning(message)
    }
    else {
      msgWarning(message)
    }
    return Promise.reject(error)
  },
)

export default api
