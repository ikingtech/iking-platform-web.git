/*
 * @Author: fj
 * @LastEditors: fj
 * @description:
 * @updateInfo: 本次更新内容：
 * @Date: 2023-07-31 11:33:28
 * @LastEditTime: 2023-07-31 17:03:50
 */

import type { Ref } from 'vue'
import { msgApi } from '@main/api/modules/msg'

export interface Tlist {
  label: string
  value: any
  [key: string]: any
}

export const useNews = () => {
  const businessType: Ref<Array<Tlist>> = ref([])
  const businessTypeNotAll: Ref<Array<Tlist>> = ref([])
  const loading = ref(false)
  const pageData: any = reactive({})
  const tableData = ref([])
  const total = ref(0)
  const { msgError, msgSuccess, msgBoxWarnAsync } = useMessage()

  const getTableData = async (param: any) => {
    loading.value = true
    const params = {
      ...param,
      ...pageData.value,
    }
    const { msg, success, data, total: pageTotal } = await msgApi.getManagementList(params)
    tableData.value = (success && data) ? data : []
    total.value = pageTotal
    !success && msgError(msg)
    loading.value = false
  }

  return {
    businessTypeNotAll,
    businessType,
    tableData,
    total,
    pageData,
    loading,
    getTableData,
    msgError,
    msgSuccess,
    msgBoxWarnAsync,
  }
}
