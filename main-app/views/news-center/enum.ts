/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-07-28 16:40:53
 * @LastEditTime : 2023-07-28 16:42:37
 */
export enum ENewsChannel {
  启用 = 'ENABLED',
  禁用 = 'DISABLED',
}
