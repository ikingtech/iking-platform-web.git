/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-11-25 09:44:17
 * @LastEditTime : 2023-10-13 17:48:15
 */
import { _ } from 'iking-utils-pro'
import type { Ref } from 'vue'
import useNewsStore from '@main/store/modules/news'
import { msgApi } from '@main/api/modules/msg'

export enum ESendType {
  系统消息 = 'SYSTEM',
  微信小程序 = 'WECHAT_MINI_SUBSCRIBE',
  钉钉群机器人 = 'DING_TALK_ROBOT',
  短信 = 'SMS',
}
export const sendTypeOption = [
  {
    label: '系统消息',
    value: ESendType.系统消息,
  },
  {
    label: '微信小程序',
    value: ESendType['微信小程序'],
  },
]
export interface Tlist {
  businessKey: string
  businessName: string
  [key: string]: any
}
export const useNews = () => {
  const newsStore = useNewsStore()
  const tloading = ref(false)
  const msgNoList: Ref<any[]> = ref([])
  const tempForm = ref(_.cloneDeep(newsStore.templateForm))
  const businessType: Ref<Array<Tlist>> = ref([])
  const { msgError, msgSuccess, msgBoxWarnAsync } = useMessage()

  // 根据模块获取编号
  const getNoByTemplate = async (statu = true) => {
    tloading.value = true
    msgNoList.value = []
    const { msg, success, data } = await msgApi.getMsgTemplateByBusiness(tempForm.value.businessKey)
    tloading.value = false
    if (!success) {
      msgError(msg)
      statu && (tempForm.value.messageTemplateKey = '')
    }
    else {
      msgNoList.value = data
      if (statu) {
        tempForm.value = _.cloneDeep(msgNoList.value?.[0] || {})
        newsStore.setTemplateForm(tempForm.value)
      }

      msgNoList.value.forEach((item: any) => {
        item.label = item.messageTemplateKey
        item.value = item.messageTemplateKey
      })
    }
  }
  /**
   * @description: 获取业务类型(模块列表)
   * @param {*} bool: 是否排除所有模板已配置的业务模块
   * @return {*}
   */
  const getBusiness = async () => {
    const { msg, success, data } = await msgApi.getBusiness({
      excludeNonTemplateConfigured: true,
    })
    businessType.value = (success && data) ? data : []
    newsStore.setBusinessType(_.cloneDeep(businessType.value))
    businessType.value.unshift({
      businessName: '全部',
      businessKey: '',
    })
    !success && msgError(msg)

    {
      const { msg, success, data } = await msgApi.getBusiness({
        excludeAllTemplateConfigured: true,
      })
      newsStore.setBusinessType(_.cloneDeep(data))
      !success && msgError(msg)
    }
  }

  /**
   * @description: 根据模板编号获取消息渠道及配置信息
   * @param {string} no
   * @return {*}
   */
  const getMsgChannelByNo = async () => {
    const no = (tempForm.value as any).id
    const { msg, success, data } = await msgApi.getMsgChannelByNo(no)
    if (!success) {
      msgError(msg)
      return null
    }
    else {
      return data
    }
  }

  return {
    ESendType,
    tloading,
    newsStore,
    tempForm,
    businessType,
    msgNoList,
    getBusiness,
    msgError,
    msgSuccess,
    msgBoxWarnAsync,
    getNoByTemplate,
    getMsgChannelByNo,
  }
}
