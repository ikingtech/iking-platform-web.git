/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-11-23 11:08:47
 * @LastEditTime : 2023-01-05 16:17:08
 */

// "SYSTEM", "WECHAT_MINI_SUBSCRIBE"
export enum ESendType {
  '系统消息' = 'SYSTEM',
  '微信小程序-订阅消息' = 'WECHAT_MINI_SUBSCRIBE',
}
export const sendTypeOption = [
  {
    label: '系统消息',
    value: ESendType.系统消息,
  },
  {
    label: '微信小程序-订阅消息',
    value: ESendType['微信小程序-订阅消息'],
  },
]

export const formKey = {
  businessType: '',
  messageKey: '',
  messageTitle: '',
  receiverType: '',
  // ---
  receiver: '',
  sendTypes: [],
  systemMessageDefinition: {
    content: '',
    copyToMail: false,
    copyToShortMessage: false,
    redirectTo: '',
    voice: false,
    templateId: '',
  },
  wechatMiniMessageDefinition: {
    miniId: '',
    miniMessageTemplateContent: '',
    miniMessageTemplateId: '',
    miniName: '',
    paramDefinitionList: [],
    templateId: '',
  },
}

export const placeholder = {
  businessKey: '请选择模块',
  messageTemplateKey: '请选择编号',
  messageTemplateTitle: '请输入标题',
  receiverType: '请选择消息接收人',
  templateId: '请选择消息接收人',
  sendTypes: '请选择推送渠道',
}

export const rules = {
  businessKey: [{ required: true, message: placeholder.businessKey, trigger: 'change' }],
  messageTemplateKey: [{ required: true, message: placeholder.messageTemplateKey, trigger: 'change' }],
  messageTemplateTitle: [{ required: true, message: placeholder.messageTemplateTitle, trigger: 'blur' }],
  receiverType: [{ required: true, message: placeholder.receiverType, trigger: 'change' }],
  sendTypes: [{ required: true, message: placeholder.sendTypes, trigger: 'change' }],
}

export const receiverTypeOption = [
  {
    label: '全部',
    value: 'ALL',
  },
  {
    label: '按模块参数指定',
    value: 'BUSINESS_SPECIFIED',
  },
  {
    label: '部门',
    value: 'DEPARTMENT',
  },
  {
    label: '菜单',
    value: 'MENU',
  },
  {
    label: '岗位',
    value: 'POST',
  },
  {
    label: '角色',
    value: 'ROLE',
  },
  {
    label: '用户',
    value: 'USER',
  },
]
