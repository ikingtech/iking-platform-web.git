/*
 * @Author       : wfl
 * @LastEditors: zqf
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-07-26 11:28:16
 * @LastEditTime: 2024-03-28 15:25:12
 */
import type { ISearchForm } from '@main/enums/search'

const useFields = () => {
  const defKeys = useTableKey()
  const fieldList: Ref<ISearchForm[]> = ref([
    {
      key: 'businessName',
      label: '模块',
      show: true,
      width: 120,
      search: true,
      formProp: {
        clearable: true,
      },
    },
    {
      key: 'messageTemplateKey',
      label: '编号',
      show: true,
      minWidth: 100,
      search: true,
    },
    {
      key: 'messageTemplateTitle',
      label: '标题',
      show: true,
      minWidth: 140,
      search: true,
    },
    {
      key: 'channel',
      label: '推送渠道',
      show: true,
      minWidth: 160,
      tableSlot: 'channelNames',
      search: true,
      type: EFormType.select,
      options: [
        {
          value: 'SYSTEM',
          label: '系统消息',
        },
        {
          value: 'WECHAT_MINI_SUBSCRIBE',
          label: '微信小程序-订阅消息',
        },
        {
          value: 'SMS',
          label: '短信',
        },
        {
          value: 'DING_TALK_ROBOT',
          label: '钉钉',
        },
      ],
    },
    ...defKeys,
    {
      key: '',
      label: '操作',
      show: true,
      width: 106,
      tableSlot: 'operate',
      fixed: 'right',
    },
  ])

  return {
    fieldList,
  }
}

export default useFields
