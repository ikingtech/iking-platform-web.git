/*
 * @Author: fj
 * @LastEditors: fj
 * @description:
 * @updateInfo: 本次更新内容：
 * @Date: 2023-08-17 14:36:24
 * @LastEditTime: 2023-08-29 10:32:39
 */
import type { ISearchForm } from '@main/enums/search'

const useFields = () => {
  const defKeys = useTableKey()
  const formKey = {
    // 小程序名称
    name: '',
    // 排序
    sort: '',
    appId: '',
  }
  const fieldList: Ref<ISearchForm[]> = ref([
    {
      key: 'name',
      label: '小程序名称',
      show: true,
      minWidth: 120,
      search: true,
      isEdit: true,
      type: 'el-input',
      formProp: {
        clearable: true,
      },
      rules: [{
        required: true,
        message: '请输入小程序名称',
        trigger: 'blur',
      }],
    },
    {
      key: 'appId',
      label: 'AppId',
      show: true,
      minWidth: 100,
      search: true,
      isEdit: true,
      type: 'el-input',
      formProp: {
        clearable: true,
      },
      rules: [{
        required: true,
        message: '请输入AppId',
        trigger: 'blur',
      }],
    },
    ...defKeys,
    {
      key: '',
      label: '操作',
      show: true,
      width: 106,
      tableSlot: 'operate',
      fixed: 'right',
    },
  ])

  return {
    fieldList,
    formKey,
  }
}

export default useFields
