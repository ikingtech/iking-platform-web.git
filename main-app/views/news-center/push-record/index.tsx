/*
 * @Author       : wfl
 * @LastEditors: fj
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-11-07 16:08:42
 * @LastEditTime: 2023-07-31 15:37:15
 */
import { defineComponent, ref } from 'vue'
import type { Ref } from 'vue'
import IkPageFull from '@main/components/LayoutCom/IkPageFull/index.vue'
import IkSplitPan from '@main/components/LayoutCom/IkSplitPan/index.vue'
import IkItemList from '@main/components/BaseCom/IkItemList/index.vue'
import { IkStatu, IkSvgIcon } from 'iking-web-ui-pro'
import type { ISearchForm } from '@main/enums/search'
import { useNews } from '../news-template/useNews'
import { useNews as centerUserNews } from '../useNews'

export interface TList {
  businessKey: string
  businessName: string
  [key: string]: any
}
const PushRecord = defineComponent({
  setup() {
    /**
     * @description: 查询左侧列表
     * @return {*}
     */
    const {
      businessType,
      getBusiness,
    } = useNews()

    // 右侧表格查询
    const {
      getTableData,
      tableData,
      total,
      pageData,
      loading,
    } = centerUserNews()

    const typeList = computed(() => businessType.value)
    const businessKey = ref('')
    const pageRef: Ref<any> = ref()
    const tableFields: Ref<ISearchForm[]> = ref([{
      key: 'businessName',
      value: '',
      label: '模块',
      show: true,
      minWidth: 100,
    }, {
      key: 'messageTemplateKey',
      value: '',
      label: '编号',
      show: true,
      minWidth: 100,
    }, {
      key: 'messageTitle',
      value: '',
      label: '标题',
      show: true,
      minWidth: 100,
    }, {
      key: 'sendChannelName',
      value: '',
      label: '推送渠道',
      show: true,
      minWidth: 100,
    }, {
      key: 'receiverName',
      value: '',
      label: '消息接收人',
      show: true,
      minWidth: 100,
    }, {
      key: 'receiverId',
      value: '',
      label: '用户ID',
      show: true,
      minWidth: 100,
    }, {
      key: 'deliverTime',
      value: '',
      label: '推送时间',
      show: true,
      minWidth: 110,
    }, {
      key: 'deliverStatus',
      value: '',
      label: '状态',
      show: true,
      tableSlot: 'deliverStatus',
      minWidth: 150,
    }])

    // 切换选中消息类型
    const handSelectType = (row: TList) => {
      pageData.value.page = 1
      businessKey.value = row.businessKey
      getTableData({ businessKey: row.businessKey })
    }

    onMounted(() => {
      getBusiness()
    })
    return () => (
      <>
        <IkSplitPan size={0} left-size={[0, 100]}>
          {{
            left: (
              <>
                <IkPageFull
                  header-title="推送记录"
                  refreshBtn={false}
                  settingBtn={false}
                >
                  {{
                    headerRight: (
                      <>
                        <IkSvgIcon
                          show-bg
                          title="刷新"
                          name="icon-shuaxin1"
                          v-role="ROLE-USER"
                          onClick={() => getBusiness()}
                        />
                      </>
                    ),
                    table: (
                      <>
                        <IkItemList
                          list={typeList.value as any}
                          proxy={{ label: 'businessName', value: 'businessKey' }}
                          onSelect={(role: any) => handSelectType(role)}
                        >
                        </IkItemList>
                      </>
                    ),
                  }}
                </IkPageFull>
              </>
            ),
            right: (
              <IkPageFull
                ref={pageRef as any}
                v-model={pageData.value}
                fields={tableFields.value}
                table-data={tableData.value}
                total={total.value}
                loading={loading.value}
                search={() => getTableData({ businessKey: businessKey.value })}
              >
                {{
                  deliverStatus: ({ data }: { data: any }) => data.row?.deliverStatus === 'DELIVER_FAILED'
                    ? (
                      <span>
                        <IkStatu type={data.row?.deliverStatus === 'DELIVER_FAILED' ? 'danger' : 'success'} title={data.row?.deliverStatusName} />
                        <span>{`（${data.row?.cause}）` || ''}</span>
                      </span>
                      )
                    : (
                      <IkStatu type={data.row?.deliverStatus === 'DELIVER_FAILED' ? 'danger' : 'success'} title={data.row?.deliverStatusName} />
                      ),
                }}
              </IkPageFull>
            ),
          }}
        </IkSplitPan>
      </>
    )
  },
})

export default PushRecord
