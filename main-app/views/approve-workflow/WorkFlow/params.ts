/*
 * @Author       : wfl
 * @LastEditors: ww
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-11-23 16:30:54
 * @LastEditTime: 2023-06-26 19:14:39
 */
export const ValueType = {
  string: 'string',
  object: 'object',
  array: 'array',
  number: 'number',
  date: 'date',
  time: 'time',
  user: 'User',
  dept: 'Dept',
  dateRange: 'DateRange',
  input: 'input',
  select: 'select',
}

export const WidgetTypeToValueType = (widgetType: string) => {
  switch (widgetType) {
    case 'INPUT':
    case 'SELECT':
    case 'TEXT_AREA':
    case 'RADIO':
      return 'string'
    case 'DATE_PICKER':
    case 'DATE':
      return 'date'
    case 'TIME_PICKER':
    case 'TIME':
      return 'time'
    case 'NUMBER':
    case 'IKCURRENCYINPUT':
      return 'number'
    case 'CHECKBOX':
      return 'array'
    case 'USER_PICKER':
      return 'User'
    case 'DEPT_PICKER':
    case 'ORGANIZATION_PICKER':
      return 'Dept'
    default:
      return 'string'
  }
}

export const groupNames = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']
