/*
 * @Author       : wfl
 * @LastEditors: zqf
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-05-08 14:49:02
 * @LastEditTime: 2024-05-31 14:41:55
 */
import { useFlowStore } from '@main/store/modules/flow'
import {
  EApprovalEmpty,
  EApprovalReject,
  EApproveCategory,
  EApproveType,
  EMultipleSignature,
  ESelectRange,
} from '@main/views/approve-workflow/enum'

export const useApproveObject = () => {
  enum EConfigType {
    对象 = 'object',
    为空时 = 'empty',
    部门 = 'dep',
  }

  const flowStore = useFlowStore()
  const pickerValue = ref<any>('')
  const ApprovalReject = EApprovalReject
  const MultipleSignature = EMultipleSignature
  const ApproveType = EApproveType
  const SelectRange = ESelectRange
  const ApproveCategory = EApproveCategory
  const ApprovalEmpty = EApprovalEmpty

  // picker组件显示or隐藏
  const orgPickerShow = ref(false)

  const pickerTitle = ref('选择人员')

  const pickState: {
    type: string
    tabs: string[]
    chooseType: string[]
    multiple: boolean
  } = reactive({
    // type: object - 审批对象   empty - 为空时人员
    type: EConfigType.对象,
    tabs: [],
    chooseType: [],
    multiple: true,
  })

  // TODO  选人组件数据回显
  const selectUser = () => {
    pickerTitle.value = '选择人员'
    pickState.type = EConfigType.对象
    pickState.tabs = ['group']
    pickState.chooseType = ['user']
    pickState.multiple = true
    pickerValue.value = flowStore.selectedNode.props.assignedUser
    orgPickerShow.value = true
  }
  const selectNoSetUser = () => {
    pickState.tabs = ['group']
    pickerTitle.value = '选择人员'
    pickState.type = EConfigType.为空时
    pickState.chooseType = ['user']
    pickState.multiple = true
    pickerValue.value
      = flowStore.selectedNode.props?.reserveApprovals
      || flowStore.selectedNode.props.nobody?.assignedUser
      || []
    orgPickerShow.value = true
  }
  const selectRole = () => {
    pickState.tabs = ['role']
    pickerTitle.value = '选择角色'
    pickState.type = EConfigType.对象
    pickState.multiple = false
    pickState.chooseType = ['role']
    pickerValue.value = flowStore.selectedNode.props.assignedUser
    orgPickerShow.value = true
  }

  const selectDep = () => {
    pickState.tabs = ['group']
    pickerTitle.value = '选择部门'
    pickState.type = EConfigType.部门
    pickState.multiple = true
    pickState.chooseType = ['dep']
    pickerValue.value = flowStore.selectedNode.props.executorDeptIds
    orgPickerShow.value = true
  }

  // 范围选择 按角色、按成员
  const handSelectRoleUser = (val: { type: string }) => {
    pickState.type = EConfigType.对象
    if (val.type === ESelectRange.按角色指定) {
      selectRole()
    }
    else if (val.type === ESelectRange.按成员指定) {
      selectUser()
    }
  }
  // 多角色选择
  const assignedUserState: any = ref({
    index: 0,
    list: flowStore?.selectedNode?.props?.assignedUser?.length
      ? flowStore?.selectedNode?.props?.assignedUser
      : [{ executorParentDeptLevel: 1 }],
  })
  const selectAssignedUser = (index: number) => {
    assignedUserState.value.index = index
    pickState.tabs = ['role']
    pickerTitle.value = '选择角色'
    pickState.type = EConfigType.对象
    pickState.multiple = false
    pickState.chooseType = ['role']
    pickerValue.value = assignedUserState.value.list?.length
      ? [assignedUserState.value.list[assignedUserState.value.index]]
      : []
    orgPickerShow.value = true
  }
  const selectAssignedUserDep = (index: number) => {
    assignedUserState.value.index = index
    pickState.tabs = ['group']
    pickerTitle.value = '选择部门'
    pickState.type = EConfigType.部门
    pickState.multiple = true
    pickState.chooseType = ['dep']
    pickerValue.value = assignedUserState.value.list?.length
      ? assignedUserState.value.list[assignedUserState.value.index]
        .executorDeptIds
      : []
    orgPickerShow.value = true
  }
  const handleAddAssignedUser = () => {
    assignedUserState.value.list.push({ executorParentDeptLevel: 1 })
  }
  return {
    orgPickerShow,
    EConfigType,
    ApprovalReject,
    MultipleSignature,
    ApproveType,
    SelectRange,
    ApprovalEmpty,
    ApproveCategory,
    handSelectRoleUser,
    selectNoSetUser,
    selectUser,
    selectRole,
    selectDep,
    pickerValue,
    pickerTitle,
    pickState,
    assignedUserState,
    selectAssignedUser,
    selectAssignedUserDep,
    handleAddAssignedUser,
  }
}
