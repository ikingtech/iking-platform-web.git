export const usePickerRole = () => {
  const { baseUrl } = useHttpUrl()
  const token = useToken()

  const comPickerConfig = ref({
    rootDepartmentOnly: false,
    dataScopeOnly: true,
    api: {
      methods: 'post',
      url: `${baseUrl}/server/component/pick/mix`,
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        // eslint-disable-next-line
        "Authorization": token,
        'X-TENANT': ikStore.local.getItem(ELocal.TENANT)?.code,
        'menu-id': ikStore.local.getItem(ELocal.MENU_ID),
      },
    },
    typeOption: {
      dep: 'DEPT',
      user: 'USER',
      role: 'ROLE',
      post: 'POST',
    },
    propOption: {
      name: 'elementName',
      id: 'elementId',
      type: 'elementType',
    },
  })

  onMounted(() => {
    const { dataScopeOnly, rootDepartmentOnly } = useRoute().query
    comPickerConfig.value.dataScopeOnly ??= dataScopeOnly as unknown as boolean
    comPickerConfig.value.rootDepartmentOnly ??= rootDepartmentOnly as unknown as boolean
  })

  return {
    comPickerConfig,
  }
}
