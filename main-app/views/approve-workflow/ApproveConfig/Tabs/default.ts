export const DEFAULT_BASE = {
  formId: '',
  groupId: '',
  remark: '',
  type: '',
  name: '',
  group: '',
  icon: 'ikflow-yonghu',
  iconBackground: '#1e90ff',
  // 描述
  desc: '',
  // 发起人
  initiators: [],
  initiatorsName: '',
  // 管理员
  managers: [],
  managersName: '',
  deptId: '',
  code: '',
}
