/*
 * @Author       : wfl
 * @LastEditors: wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-11-23 15:23:25
 * @LastEditTime: 2023-04-13 17:23:45
 */
// 节点类型
export enum EInsertNode {
  // 发起人节点
  ROOT = 'INITIATOR',
  // 审批人节点
  APPROVAL = 'APPROVE',
  // 抄送人节点
  CC = 'CARBON_COPY',
  // 处理人节点
  DISPOSE = 'DISPOSE',
  // 条件分支节点
  CONDITIONS = 'CONDITION',
  // 并行分支节点
  CONCURRENTS = 'BRANCH',
  // 结束节点
  END = 'END',

  // -------------以下暂留=未使用---------------
  // 延迟等待
  DELAY = 'DELAY',
  // 触发器
  TRIGGER = 'TRIGGER',
  // eslint-disable-next-line ts/no-duplicate-enum-values
  BRANCH = 'BRANCH',
}

export type TInsertNode = 'INITIATOR' | 'APPROVE' | 'CARBON_COPY' | 'DISPOSE' | 'CONDITION' | 'BRANCH' | 'END' | 'DELAY' | 'TRIGGER'

export enum ECompare {
  'IN' = 'IN',
  'B' = 'B',
  'AB' = 'AB',
  'BA' = 'BA',
  'ABA' = 'ABA',
  '<=' = '<=',
  '>=' = '>=',
}

export enum ENode {
  // 发起人，根节点
  ROOT = 'INITIATOR',
  // 审批
  APPROVAL = 'APPROVE',
  // 抄送
  CC = 'CARBON_COPY',
  // 办理人节点
  DISPOSE = 'DISPOSE',
  // 条件组
  CONDITIONS = 'CONDITION',
  // 并行节点组
  CONCURRENTS = 'BRANCH',
  // 条件分支
  // eslint-disable-next-line ts/no-duplicate-enum-values
  CONDITION = 'CONDITION',
  // 并行分支
  // eslint-disable-next-line ts/no-duplicate-enum-values
  CONCURRENT = 'BRANCH',
  // 触发器
  TRIGGER = 'TRIGGER',
  // 延时节点
  DELAY = 'DELAY',
  // 空节点，占位
  EMPTY = 'EMPTY',
  NODE = 'NODE',
  // eslint-disable-next-line ts/no-duplicate-enum-values
  BRANCH = 'BRANCH',
}

export enum ETrigger {
  发送请求 = 'WEBHOOK',
  发送邮件 = 'EMAIL',
}

export enum EChooseType {
  dep = 'DEPT',
  user = 'USER',
  role = 'ROLE',
  post = 'POST',
}

export enum ETab {
  group = 'group',
  role = 'role',
  post = 'post',
}

export interface TList {
  id: string | number
  name: string
  type: keyof typeof EChooseType // 部门 用户 角色
  [key: string]: any
}

// 审批方式
export enum EApproveType {
  人工审批 = 'MANUALLY',
  自动通过 = 'AUTO_PASS',
  自动拒绝 = 'AUTO_REJECT',
}

// 选择范围
export enum ESelectRange {
  全公司 = 'ALL',
  按角色指定 = 'BY_ROLE',
  按成员指定 = 'BY_USER',
}

// 审批对象类别
// approvals.executorType
// 执行者类型（approvals.executorType）这个字段用于标识所选的审批对象的类型，比如按角色指定，那这个字段就填ROLE，然后把选择的角色编号写到executorId中
export enum EApproveCategory {
  指定成员 = 'SPECIFIED_USER',
  发起人自选 = 'INITIATOR_SPECIFIED',
  指定角色 = 'ROLE',
  发起人部门主管 = 'INITIATOR_DEPT_MANAGER',
  发起人自己 = 'INITIATOR_SELF',
  部门控件关联角色 = 'DEPT_SELECT_COMPONENT',
  联系人控件关联成员 = 'USER_SELECT_COMPONENT',
  组织控件关联角色 = 'ORGANIZATION_SELECT_COMPONENT',
}

export enum EApproveRoleType {
  发起人所在部门 = 'INITIATOR_DEPT',
  发起人上级部门 = 'INITIATOR_PARENT_DEPT',
  指定部门 = 'SPECIFIED_DEPT',
}

export enum EApproveCategoryEdit {
  指定成员 = 'SPECIFIED_USER',
  发起人自选 = 'INITIATOR_SPECIFIED',
  指定角色 = 'ROLE',
  发起人部门主管 = 'INITIATOR_DEPT_MANAGER',
  发起人自己 = 'INITIATOR_SELF',
}

// 多人审批方式
export enum EMultipleSignature {
  依次审批 = 'SEQUENCE',
  会签 = 'ALL',
  或签 = 'ANY',
}

// 审批人为空时
export enum EApprovalEmpty {
  自动通过 = 'AUTO_PASS',
  自动转交管理员 = 'TRANSMIT_TO_MANAGER',
  指定人员审批 = 'SPECIFIED_USER',
}

// 审批人拒绝时
export enum EApprovalReject {
  直接结束 = 'END',
  退回至指定节点 = 'TO_SPECIFIED',
  手动选择节点 = 'TO_CHOOSE',
}

// 表单分类
export enum EFlowFormType {
  已有表单 = 'PROCESS_ONLY',
  流程表单 = 'PROCESS_FORM',
  已有模板 = 'DATA_FORM',
}

// 工作流分支节点条件比较式运算符
export enum EComparatorType {
  小于 = 'LESS',
  小于等于 = 'LESS_EQUAL',
  大于 = 'GREATER',
  大于等于 = 'GREATER_EQUAL',
  等于 = 'EQUAL',
  介于 = 'BETWEEN',
  // 多选、单选项
  完全包含 = 'IN',
  所有 = 'ALL',
  任意 = 'ANY',
}

export const ComparatorMap = {
  LESS: '<',
  LESS_EQUAL: '<=',
  GREATER: '>',
  GREATER_EQUA: '>=',
  EQUAL: '=',
  BETWEEN: '介于', // for multiple-choice task, only for now.  seems to be the only compar
}

// 显示类型
export enum EDateType {
  年 = 'year',
  月 = 'month',
  周 = 'week',
  日 = 'date',
  多选日 = 'dates',
  日期时间 = 'datetime',
  日期时间区间 = 'datetimerange',
  日期区间 = 'daterange',
  月区间 = 'monthrange',
}

// 格式化类型
export enum EFormatDate {
  'YYYY-MM-DD HH:mm:ss' = 'YYYY-MM-DD HH:mm:ss',
  'YYYY-MM-DD HH:m' = 'YYYY-MM-DD HH:mm',
  'YYYY-MM-DD' = 'YYYY-MM-DD',
  'YYYY' = 'YYYY',
  'YYYY-MM' = 'YYYY-MM',
  'MM-DD' = 'MM-DD',
  'HH:mm:ss' = 'HH:mm:ss',
  'HH:mm' = 'HH:mm',
}

// 表单权限类型
export enum EPermissionType {
  可编辑 = 'EDITABLE',
  只读 = 'READ_ONLY',
  隐藏 = 'INVISIBLE',
}

// 审批人去重类型
export enum EAdvanceApproveType {
  保留第一个 = 'KEEP_FIRST',
  自动去重 = 'CONTINUOUS_APPEARS_ONLY',
}
