export interface choseUser {
  id?: string
  formId?: string
  processId?: string
  formInstanceId?: string
  processInstanceId?: string
  userId?: string
  userName?: string
  userAvatar?: string
  accomplish?: boolean
  sortOrder?: number
}
export interface recordNodes {
  id?: string
  recordId?: string
  formInstanceId?: string
  processInstanceId?: string
  name?: string
  type?: string
  status?: string
  statusName?: string
  multiExecutorType?: string
  multiExecutorTypeName?: string
  executorUser?: Array<choseUser>
  appendExecutorUsers?: Array<choseUser>
  backToRecordNodeId?: string
  comment?: string
  attachment?: Array<any>
  sortOrder?: number
  createTime?: string
}
export interface initiatorSpecifiedScope {
  id?: string
  formId?: string
  nodeId?: string
  executorType?: string
  executorId?: string
  executorName?: string
  executorAvatar?: string
  executorCategory?: string
}
export interface instanceNodes {
  id?: string
  formId?: string
  processId?: string
  formInstanceId?: string
  processInstanceId?: string
  nodeId?: string
  name?: string
  type?: string
  typeName?: string
  approveType?: string
  approvalCategory?: string
  deptWidgetName?: string
  userWidgetName?: string
  initiatorSpecifiedScopeType?: string
  initiatorSpecify: true
  initiatorSpecifiedScope?: Array<initiatorSpecifiedScope>
  singleApproval: true
  multiExecutorType?: string
  multiExecutorTypeName?: string
  executorEmptyStrategy?: string
  executorEmpty?: boolean
  initiatorSpecifyCarbonCopy?: boolean
  autoExecuteComment?: string
  sortOrder?: number
  executorUsers?: Array<choseUser>
  reserveExecutorUsers?: Array<choseUser>
}
export interface processRecord {
  id?: string
  formInstanceId?: string
  processInstanceId?: string
  name?: string
  formId?: string
  processId?: string
  status?: string
  statusName?: string
  recordNodes?: Array<recordNodes>
  instanceNodes: Array<instanceNodes>
}

export interface sendData {
  id?: string
  formId?: string
  formName?: string
  businessType?: string
  businessDataId?: string
  title?: string
  initiatorId?: string
  initiatorName?: string
  initiatorDeptId?: string
  initiatorDeptName?: string
  initiatorTime?: string
  accomplishTime?: string
  waitTime?: string
  viewProperty?: string
  formData?: string
  formDataSummary?: string
  processStatus?: string
  processRecord?: processRecord
  processInstance?: processRecord
}
export interface detail {
  icon?: string
  iconBackground?: string
  name?: string
  [key: string]: any
}
export interface attachments {
  id?: string
  formId?: string
  processId?: string
  formInstanceId?: string
  processInstanceId?: string
  recordId?: string
  recordNodeId?: string
  attachmentId?: string
  attachmentName?: string
  attachmentSuffix?: string
  attachmentSize?: string
  [key: string]: any
}
export interface approvalEndorseForm {
  formInstanceId?: string
  formData?: string
  appendType?: string
  appendExecutorUserIds?: Array<any>
  backToInstanceNodeId?: string
  formInstanceComment?: string
  approveComment?: string
  attachments?: Array<attachments>
  images?: Array<any>
  [key: string]: any
}
export interface endorseFormState {
  title?: string
  loading?: boolean
  form?: approvalEndorseForm
}
export interface recordsState {
  data?: Array<any>
  returnNode?: Array<any>
  downloadUrl?: Array<any>
}
