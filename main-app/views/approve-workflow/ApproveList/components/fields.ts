/*
 * @Author       : wfl
 * @LastEditors: zqf
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-12-15 09:52:50
 * @LastEditTime: 2024-03-27 18:31:52
 */
import type { Ref } from 'vue'
import type { ISearchForm } from './search'

export const useFields = () => {
  const processStatus = {
    label: '流程状态',
    type: 'el-select',
    options: [
      {
        label: '待发起',
        value: 'WAIT_FOR_INITIATE',
      },
      {
        label: '审批中',
        value: 'APPROVING',
      },
      {
        label: '已通过',
        value: 'PASS',
      },
      {
        label: '已拒绝',
        value: 'REJECT',
      },
      {
        label: '已撤销',
        value: 'REVOKE',
      },
    ],
    key: 'processStatus',
    show: true,
    search: true,
    formProp: {
      multiple: true,
      collapseTags: true,
    },
    tableSlot: 'processStatusName',
  }

  const tableKeys: Ref<ISearchForm[]> = ref([])
  const fieldList: Ref<ISearchForm[]> = ref([
    {
      label: '标题',
      key: 'title',
      show: true,
      search: true,
    },
    {
      label: '摘要',
      key: 'formDataSummary',
      tableSlot: 'formDataSummary',
      show: true,
      search: false,
    },
    {
      label: '发起人',
      key: 'initiatorName',
      show: true,
      search: true,
    },
    {
      label: '所在部门',
      key: 'initiatorDeptName',
      show: true,
      search: true,
    },
    {
      label: '发起时间',
      key: 'initiatorTime',
      type: 'el-date-picker',
      formProp: {
        valueFormat: 'YYYY-MM-DD',
      },
      width: 155,
      show: true,
      search: true,
    },
    {
      label: '等待时长',
      key: 'waitTime',
      show: true,
      search: false,
    },
    {
      label: '审批编号',
      key: 'serialNo',
      show: true,
      search: false,
    },
    {
      ...processStatus,
    },
    {
      key: '',
      label: '操作',
      show: true,
      width: useOperateWidth(64, 75),
      showOverflowTooltip: false,
      tableSlot: 'operate',
      fixed: 'right',
      align: 'center',
    },
  ])
  const flowFieldList: Ref<ISearchForm[]> = ref([
    {
      label: '标题',
      key: 'title',
      show: true,
      search: true,
    },
    {
      label: '摘要',
      key: 'formDataSummary',
      tableSlot: 'formDataSummary',
      show: true,
      search: false,
    },
    {
      label: '发起人',
      key: 'initiatorName',
      show: true,
      search: true,
    },
    {
      label: '所在部门',
      key: 'initiatorDeptName',
      show: true,
      search: true,
    },
    {
      label: '发起时间',
      key: 'initiatorTime',
      type: 'el-date-picker',
      width: 155,
      formProp: {
        valueFormat: 'YYYY-MM-DD',
      },
      show: true,
      search: true,
    },
    {
      label: '审批时间',
      key: 'approveTime',
      type: 'el-date-picker',
      width: 155,
      show: true,
    },
    {
      label: '审批时长',
      key: 'waitTime',
      show: true,
      search: false,
    },
    {
      label: '审批编号',
      key: 'serialNo',
      show: true,
      search: false,
    },
    {
      ...processStatus,
    },
    {
      key: '',
      label: '操作',
      show: true,
      width: useOperateWidth(64, 75),
      showOverflowTooltip: false,
      tableSlot: 'operate',
      fixed: 'right',
      align: 'center',
    },
  ])
  const ISubmitFieldList: Ref<ISearchForm[]> = ref([
    {
      label: '标题',
      key: 'title',
      show: true,
      search: true,
    },
    {
      label: '摘要',
      key: 'formDataSummary',
      tableSlot: 'formDataSummary',
      show: true,
      search: false,
    },
    // {
    //   label: '发起人',
    //   key: 'initiatorName',
    //   show: true,
    //   search: true
    // },
    {
      label: '发起时间',
      key: 'initiatorTime',
      type: 'el-date-picker',
      width: 155,
      show: true,
      formProp: {
        valueFormat: 'YYYY-MM-DD',
      },
      search: true,
    },
    {
      label: '流程耗时',
      key: 'consumeTime',
      show: true,
      search: false,
    },
    {
      label: '审批编号',
      key: 'serialNo',
      show: true,
      search: false,
    },
    {
      ...processStatus,
    },
    {
      key: '',
      label: '操作',
      show: true,
      width: useOperateWidth(64, 75),
      showOverflowTooltip: false,
      tableSlot: 'operate',
      fixed: 'right',
      align: 'center',
    },
  ])
  const CopyForMeFieldList: Ref<ISearchForm[]> = ref([
    {
      label: '标题',
      key: 'title',
      show: true,
      search: true,
    },
    {
      label: '摘要',
      key: 'formDataSummary',
      tableSlot: 'formDataSummary',
      show: true,
      search: false,
    },
    {
      label: '已读状态',
      key: 'carbonCopyReadStatusName',
      tableSlot: 'carbonCopyReadStatusName',
      show: true,
      width: 100,
      align: 'center',
    },
    {
      label: '发起人',
      key: 'initiatorName',
      show: true,
      search: true,
    },
    {
      label: '所在部门',
      key: 'initiatorDeptName',
      show: true,
      search: true,
    },
    {
      label: '发起时间',
      key: 'initiatorTime',
      type: 'el-date-picker',
      show: true,
      formProp: {
        valueFormat: 'YYYY-MM-DD',
      },
      search: true,
    },
    {
      label: '审批编号',
      key: 'serialNo',
      show: true,
      search: false,
    },
    {
      ...processStatus,
    },
    {
      key: '',
      label: '操作',
      show: true,
      width: useOperateWidth(64, 75),
      showOverflowTooltip: false,
      tableSlot: 'operate',
      fixed: 'right',
      align: 'center',
    },
  ])
  return {
    fieldList,
    tableKeys,
    flowFieldList,
    ISubmitFieldList,
    CopyForMeFieldList,
  }
}
