/*
 * @Author       : wfl
 * @LastEditors: zqf
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-12-15 09:52:50
 * @LastEditTime: 2024-04-09 11:42:59
 */
import type { Ref } from 'vue'
import type { ISearchForm } from '@main/enums/search'
import { EFormType } from '@main/enums/search'

export const useFields = (datasourceDict: any) => {
  const tableKeys: Ref<ISearchForm[]> = ref([])
  const defKeys = useTableKey()
  const fieldList: Ref<ISearchForm[]> = ref([
    {
      key: 'name',
      value: '',
      label: '连接名称',
      show: true,
      search: true,
    },
    {
      key: 'type',
      label: '数据库类型',
      width: 100,
      type: EFormType.select,
      options: datasourceDict,
      show: true,
      search: true,
    },
    {
      key: 'ip',
      value: '',
      label: 'ip',
      width: 140,
      show: true,
      search: true,
    },
    {
      key: 'port',
      value: '',
      label: '端口',
      show: true,
      width: 100,
      search: true,
    },
    {
      key: 'schemaName',
      value: '',
      label: '数据库名称',
      show: true,
      search: true,
    },
    {
      key: 'remark',
      value: '',
      label: '描述',
      show: true,
      width: 150,
    },
    ...defKeys,
    {
      key: '',
      value: '',
      label: '操作',
      show: true,
      width: 106,
      fixed: 'right',
      tableSlot: 'operate',
    },
  ])

  return {
    fieldList,
    tableKeys,
  }
}
