/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-11-15 15:00:03
 * @LastEditTime : 2022-11-15 15:16:40
 */
export const datasourceDict = [
  {
    label: 'MYSQL',
    value: 'MYSQL',
  },
  {
    label: 'POSTGRES',
    value: 'POSTGRES',
  },
  {
    label: 'ORACLE',
    value: 'ORACLE',
  },
  {
    label: 'MSSQL_SERVER',
    value: 'MSSQL_SERVER',
  },
]
