/*
 * @Author       : wfl
 * @LastEditors: zqf
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-05-26 15:22:26
 * @LastEditTime: 2024-03-27 16:09:38
 */
import { ElCheckbox, ElSkeleton, ElTable, ElTooltip } from 'element-plus'
import { ikTree } from 'iking-utils-pro'
import { defineExpose } from 'vue'
import type { PropType } from 'vue'
import { roleApi } from '@main/api/modules/role'
import { menuApi } from '@main/api/modules/menu'
import { MenuBtnEnum } from '@main/enums/menu'
import { IkEmpty } from 'iking-web-ui-pro'
import type { MenuFormState, RoleFormState } from '@main/enums/role'
import './index.scss'

interface RoleMenuTableProps extends MenuFormState {
  checked?: boolean
  indeterminate?: boolean
}

enum CheckEnum {
  CHECKED = 'CHECKED',
  UNCHECKED = 'UNCHECKED',
  PARTIAL_CHECKED = 'PARTIAL_CHECKED',
}

const RoleListComfig = defineComponent({
  props: {
    role: {
      type: Object as PropType<RoleFormState>,
    },
    helpState: {
      type: Boolean,
      default: false,
    },
    height: { type: Number, default: 0 },
    check: {
      type: Boolean,
      default: false,
    },
  },
  emits: ['update', 'checkChange'],
  setup(props, { emit }) {
    const { msgSuccess, msgError } = useMessage()
    const loading = ref(false)
    const menuTreeJson = ref('')

    const state: { [key: string]: Array<RoleMenuTableProps & { [key: string]: any }> } = reactive({
      menuTree: [],
      roleList: [],
      btnList: [],
      __menuList: [],
    })

    defineExpose({ menuTreeJson, tree: state.menuTree })

    const { handChangeCheck, deepSetList, handChanging } = useRoles(toRef(state, 'menuTree'), emit)

    // 按钮绑定在__children属性上
    const deepSetBtnChild = (data: any[]) => {
      data.forEach((item: { children?: any[], __children: any }) => {
        if (item?.children?.length) {
          if (
            [MenuBtnEnum.PAGE_TAB, MenuBtnEnum.PAGE_BUTTON, MenuBtnEnum.GLOBAL_BUTTON].includes(
              item.children[0].menuType,
            )
          ) {
            item.__children = item.children
            delete item.children
          }
          else {
            deepSetBtnChild(item.children)
          }
        }
      })
    }

    // 获取所有菜单数据
    const getMenuListData = async (roleMenuList: any[]) => {
      menuApi
        .getAllMenu()
        .then(async ({ success, data, msg }) => {
          if (success) {
            state.__menuList = data
            state.btnList = data.filter((item: { menuType: MenuBtnEnum }) =>
              [MenuBtnEnum.PAGE_BUTTON, MenuBtnEnum.GLOBAL_BUTTON].includes(
                item.menuType,
              ),
            )
            state.menuTree = ikTree.listToTree(
              data.map(
                (item: {
                  id: any
                  checked: boolean
                  indeterminate: boolean
                }) => {
                  const checkData = roleMenuList.find(
                    (it: { id: any }) => it.id === item.id,
                  )
                  if (checkData) {
                    item.checked = checkData.roleAssigned === CheckEnum.CHECKED
                    item.indeterminate
                      = checkData.roleAssigned === CheckEnum.PARTIAL_CHECKED
                  }
                  else {
                    item.checked = false
                    item.indeterminate = false
                  }
                  return item
                },
              ),
            )
            await deepSetBtnChild(state.menuTree)
            menuTreeJson.value = JSON.stringify(state.menuTree)
          }
          else {
            msgError(msg)
          }
          loading.value = false
        })
        .catch(() => {
          loading.value = false
        })
    }

    // 获取权限菜单数据
    const getMenuTreeData = async () => {
      loading.value = true
      state.btnList = []
      try {
        const roleData = await roleApi.getRoleMenus(props.role?.id)
        const roleMenuList = roleData?.data?.menus || []
        getMenuListData(roleMenuList)
      }
      catch (error) {
        loading.value = false
        getMenuListData([])
      }
    }

    watch(
      () => props.role,
      (val) => {
        if (val?.id) {
          getMenuTreeData()
        }
        else {
          state.menuTree = []
          state.roleList = []
          state.btnList = []
          state.__menuList = []
        }
      },
    )

    watch(() => props.check, (val) => {
      handChanging.value = val
    })

    const handSaveRoles = async () => {
      loading.value = true
      const deepTree = deepSetList(state.menuTree)
      const { msg, success } = await roleApi.updateRoleMenus({
        roleId: props.role?.id,
        menuIds: deepTree
          .filter(item => item.checked || item.indeterminate)
          .map(item => item.id),
        // roleMenuList: deepTree
        //   .filter(item => item.checked || item.indeterminate)
        //   .map(item => {
        //     return {
        //       id: item.id,
        //       menuId: item.id,
        //       checkStatus: item.checked
        //         ? CheckEnum.CHECKED
        //         : item.indeterminate
        //           ? CheckEnum.PARTIAL_CHECKED
        //           : CheckEnum.UNCHECKED
        //     }
        //   })
      })
      if (success) {
        handChanging.value = false
        emit('checkChange', false)
        msgSuccess(msg)
      }
      else {
        msgError(msg)
      }
      loading.value = false
    }
    // 保存
    watch(
      () => props.helpState,
      () => {
        handSaveRoles()
      },
    )

    // onBeforeRouteLeave((from, to, next) => {
    //   if (handChanging.value) {
    //     ElMessageBox.confirm('当前页面未保存，是否确定要离开？', '温馨提醒', {
    //       confirmButtonText: '确定离开',
    //       cancelButtonText: '取消',
    //       type: 'warning'
    //     })
    //       .then(() => {
    //         emit('checkChange', false)
    //         next()
    //       })
    //       .catch(() => {})
    //   }
    //   else {
    //     next()
    //   }
    // })

    return () =>
      state.menuTree.length
        ? (
          <>
            <ElTable
              border
              row-class-name="tree-table-center"
              class-name="role-config-table"
              row-key="id"
              v-loading={loading.value}
              data={state.menuTree || []}
              default-expand-all
              style={{ height: '100%' }}
            >
              <el-table-column label="菜单" sortable min-width="200">
                {{
                  default: ({
                    row,
                  }: {
                    row: {
                      indeterminate: boolean
                      checked: boolean
                      name: string
                    }
                  }) => (
                    <ElCheckbox
                      indeterminate={row.indeterminate}
                      v-model={row.checked}
                      onChange={() => handChangeCheck(row)}
                    >
                      <ElTooltip
                        effect="light"
                        content={row.name}
                        placement="top-start"
                      >
                        {row.name}
                      </ElTooltip>
                    </ElCheckbox>
                  ),
                }}
              </el-table-column>
              <el-table-column label="操作" min-width="600">
                {{
                  default: ({
                    row,
                  }: {
                    row: { __children: any, manuType: string }
                  }) =>
                    row.manuType === MenuBtnEnum.MENU
                      ? null
                      : row?.__children?.map(
                        (item: {
                          checked: any
                          name?: any
                          indeterminate?: any
                          children?: any
                        }) => {
                          return (
                            <div class="w-[160px]">
                              <ElCheckbox
                                v-model={item.checked}
                                onChange={() => handChangeCheck(item)}
                              >
                                <ElTooltip
                                  effect="light"
                                  content={item.name}
                                  placement="top-start"
                                >
                                  {item.name}
                                </ElTooltip>
                              </ElCheckbox>
                            </div>
                          )
                        },
                      ),
                }}
              </el-table-column>
            </ElTable>
          </>
          )
        : loading.value
          ? (
            <>
              <div>
                <ElSkeleton rows={5} animated />
                <ElSkeleton rows={5} animated />
              </div>
            </>
            )
          : <IkEmpty />
  },
})

export default RoleListComfig
