/*
 * @Author       : wfl
 * @LastEditors: fj
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-12-15 09:52:50
 * @LastEditTime: 2023-09-08 13:38:02
 */
import type { Ref } from 'vue'
import type { ISearchForm } from '@main/enums/search'
import { EFormType } from '@main/enums/search'
import { paramDict } from './dict'

export function useFields() {
  const tableKeys: Ref<ISearchForm[]> = ref([])

  const fieldList: Ref<ISearchForm[]> = ref([
    {
      key: 'name',
      label: '参数名称',
      show: true,
      search: true,
    },
    {
      key: 'variableKey',
      label: '参数键名',
      show: true,
      search: true,
    },
    {
      key: 'variableValue',
      label: '参数键值',
      show: true,
      search: true,
    },
    {
      key: 'type',
      label: '类型',
      type: EFormType.select,
      options: paramDict,
      show: true,
      search: true,
      width: 120,
      tableSlot: 'typeName',
    },
    {
      key: '',
      label: '操作',
      show: true,
      width: useOperateWidth(106, 120),
      showOverflowTooltip: false,
      tableSlot: 'operate',
      fixed: 'right',
    },
  ])

  return {
    fieldList,
    tableKeys,
  }
}
