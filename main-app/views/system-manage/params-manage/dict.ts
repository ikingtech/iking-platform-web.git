/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-11-15 15:00:03
 * @LastEditTime : 2022-11-15 15:16:40
 */
export const paramDict = [
  {
    label: '系统类',
    value: 'SYSTEM',
  },
  {
    label: '业务类',
    value: 'BUSINESS',
  },
]

export enum EParam {
  系统类 = 'SYSTEM',
  业务类 = 'BUSINESS',
}
