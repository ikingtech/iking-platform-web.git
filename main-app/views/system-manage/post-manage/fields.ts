/*
 * @Author       : wfl
 * @LastEditors: fj
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-12-15 09:52:50
 * @LastEditTime: 2023-05-30 15:54:38
 */
import type { Ref } from 'vue'
import type { ISearchForm } from '@main/enums/search'

export const useFields = () => {
  const tableKeys: Ref<ISearchForm[]> = ref([])
  const defKeys = useTableKey()
  const loaded = ref(false)
  const fieldList: Ref<ISearchForm[]> = ref([
    {
      key: 'name',
      label: '岗位名称',
      minWidth: 120,
      show: true,
      search: true,
    },
    {
      key: 'remark',
      label: '岗位描述',
      minWidth: 160,
      show: true,
      search: true,
    },
    {
      key: 'createName',
      label: '创建人',
      minWidth: 100,
    },
    ...defKeys,
    {
      key: '',
      label: '操作',
      show: true,
      width: useOperateWidth(106, 120),
      showOverflowTooltip: false,
      tableSlot: 'operate',
      fixed: 'right',
    },
  ])

  return {
    fieldList,
    tableKeys,
    loaded,
  }
}
