/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-12-15 09:52:50
 * @LastEditTime : 2023-12-14 18:27:54
 */
import type { Ref } from 'vue'
import { EFormType } from '@main/enums/search'
import type { ISearchForm } from '@main/enums/search'

export const useFields = () => {
  const fieldList: Ref<ISearchForm[]> = ref([
    {
      key: 'name',
      value: '',
      label: '名称',
      show: true,
      search: false,
    },
    {
      key: 'grade',
      value: '',
      label: '级别',
      type: EFormType.select,
      show: true,
      width: 100,
      tableSlot: 'grade',
      search: false,
    },
    {
      key: 'status',
      value: '',
      label: '状态',
      show: true,
      width: 100,
      tableSlot: 'status',
      search: false,
    },
    {
      key: 'updateName',
      value: '',
      label: '操作人',
      show: true,
      width: 200,
      search: false,
    },
    {
      key: 'updateTime',
      value: '',
      label: '操作时间',
      show: true,
      width: 200,
      search: false,
    },
    {
      key: 'controls',
      value: '',
      label: '操作',
      show: true,
      width: useOperateWidth(110, 160),
      search: false,
      tableSlot: 'controls',
      showOverflowTooltip: false,
    },
  ])
  const rosterList: Ref<ISearchForm[]> = ref([
    {
      key: 'name',
      value: '',
      label: '员工姓名',
      width: 200,
      show: true,
      search: false,
    },
    {
      key: 'postsNames',
      value: '',
      label: '岗位',
      type: EFormType.select,
      show: true,
      tableSlot: 'postsNames',
      search: false,
    },
    {
      key: 'departments',
      value: '',
      label: '部门',
      show: true,
      tableSlot: 'departments',
      search: false,
    },
    {
      key: 'phone',
      value: '',
      label: '手机号',
      show: true,
      width: 150,
      search: false,
    },
    {
      key: 'email',
      value: '',
      label: '邮箱',
      show: true,
      width: 200,
      search: false,
    },
  ])

  return {
    fieldList,
    rosterList,
  }
}
