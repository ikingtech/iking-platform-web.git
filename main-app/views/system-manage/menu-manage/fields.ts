/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-12-15 09:52:50
 * @LastEditTime : 2023-12-14 18:14:15
 */
import type { Ref } from 'vue'
import { EAlign } from '@main/enums/search'
import type { ISearchForm } from '@main/enums/search'

export const useFields = () => {
  const defKeys = useTableKey()
  const tableKeys: Ref<ISearchForm[]> = ref([])

  const fieldList: Ref<ISearchForm[]> = ref([
    {
      key: 'name',
      value: '',
      label: '菜单名称',
      show: true,
      minWidth: 140,
    },
    {
      key: 'menuTypeName',
      value: '',
      label: '菜单类型',
      show: true,
      width: 110,
    },
    {
      key: 'jumpTypeName',
      value: '',
      label: '跳转类型',
      show: true,
      width: 120,
    },
    {
      key: 'link',
      value: '',
      label: '路由地址',
      show: true,
      width: 180,
    },
    {
      key: 'component',
      value: '',
      label: '组件路径',
      show: true,
      width: 180,
    },
    {
      key: 'menuViewTypeName',
      value: '',
      label: '可视类型',
      show: true,
      width: 110,
    },
    {
      key: 'icon',
      value: '',
      label: '图标',
      show: true,
      width: 80,
      align: EAlign.center,
      tableSlot: 'icon',
    },
    {
      key: 'logo',
      value: '',
      label: '菜单LOGO',
      show: true,
      width: 120,
    },
    {
      key: 'remark',
      value: '',
      label: '描述',
      show: true,
    },
    {
      key: 'permissionCode',
      value: '',
      label: 'Code',
      tableSlot: 'permissionCode',
      show: true,
    },
    {
      key: 'visible',
      value: '',
      label: '状态',
      show: true,
      width: 80,
      align: EAlign.center,
      tableSlot: 'visible',
    },
    ...defKeys,
    {
      key: '',
      value: '',
      label: '操作',
      show: true,
      width: useOperateWidth(148, 180),
      tableSlot: 'operate',
      fixed: 'right',
      showOverflowTooltip: false,
    },
  ])

  return {
    fieldList,
    tableKeys,
  }
}
