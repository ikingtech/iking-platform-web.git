/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-11-08 16:17:36
 * @LastEditTime : 2024-03-15 18:59:52
 */
import { MenuBtnEnum, MenuJumpEnum, MenuViewType } from '@main/enums/menu'

export const useMenuDiction = () => {
  const jumpTypes = [
    {
      label: '目录',
      value: MenuJumpEnum.NONE,
    },
    {
      label: '前端路由',
      value: MenuJumpEnum.FRONT_ROUTE,
    },
    {
      label: '外部链接',
      value: MenuJumpEnum.EXTERNAL_LINK,
    },
    {
      label: '内嵌页面',
      value: MenuJumpEnum.IFRAME,
    },
  ]

  const menuTypes = [
    {
      label: '菜单',
      value: MenuBtnEnum.MENU,
    },
    {
      label: '页面Tab',
      value: MenuBtnEnum.PAGE_TAB,
    },
    {
      label: '页面按钮',
      value: MenuBtnEnum.PAGE_BUTTON,
    },
    {
      label: '全局按钮',
      value: MenuBtnEnum.GLOBAL_BUTTON,
    },
  ]

  const menuViewTypes = [
    {
      label: '业务端',
      value: MenuViewType.BUSINESS,
    },
    {
      label: '管理端',
      value: MenuViewType.MANAGE,
    },
    {
      label: '手机端',
      value: MenuViewType.MOBILE,
    },
  ]

  const frameworkTypes = [
    {
      label: '业务路由',
      value: false,
    },
    {
      label: '框架路由',
      value: true,
    },
  ]

  const badgeTypes = [
    {
      label: '无',
      value: '',
    },
    {
      label: '点标记',
      value: 'point',
    },
    {
      label: '数字',
      value: 'number',
    },
    {
      label: '文本',
      value: 'string',
    },
  ]

  return {
    jumpTypes,
    menuTypes,
    menuViewTypes,
    frameworkTypes,
    badgeTypes,
  }
}
