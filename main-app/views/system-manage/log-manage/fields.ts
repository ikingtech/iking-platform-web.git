/*
 * @Author       : wfl
 * @LastEditors: ww
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-12-15 09:52:50
 * @LastEditTime: 2023-05-23 17:41:03
 */
import type { Ref } from 'vue'
import type { ISearchForm } from '@main/enums/search'

export const useFields = () => {
  const loginFields = [
    {
      key: 'username',
      value: '',
      label: '用户账号',
      show: true,
      minWidth: 100,
      search: true,
    }, {
      key: 'userId',
      value: '',
      label: '用户id',
      show: true,
      minWidth: 100,
      search: true,
    }, {
      key: 'ip',
      value: '',
      label: 'IP地址',
      show: true,
      minWidth: 130,
      search: true,
    }, {
      key: 'location',
      value: '',
      label: 'IP归属地',
      show: true,
      minWidth: 100,
      search: true,
    }, {
      key: 'message',
      value: '',
      label: '返回信息',
      show: true,
      minWidth: 100,
    }, {
      key: 'os',
      value: '',
      label: '操作系统',
      show: true,
      minWidth: 100,
      search: true,
    }, {
      key: 'browser',
      value: '',
      label: '浏览器',
      show: true,
      minWidth: 100,
      search: true,
    }, {
      key: 'signTime',
      value: '',
      label: '登录时间',
      show: true,
      minWidth: 120,
    }, {
      key: 'typeName',
      value: '',
      label: '登录类型',
      show: true,
      minWidth: 100,
    }, {
      key: 'success',
      value: '',
      label: '状态',
      width: 80,
      show: true,
      tableSlot: 'success',
    }]
  const operationFieldList: Ref<ISearchForm[]> = ref([
    {
      key: 'operateUsername',
      value: '',
      label: '操作用户名',
      show: true,
      minWidth: 120,
      search: true,
    },
    {
      key: 'operateUserId',
      value: '',
      label: '操作用户id',
      minWidth: 160,
      show: true,
    },
    {
      key: 'requestMethod',
      value: '',
      label: '请求方式',
      minWidth: 100,
      show: true,
      search: true,
    },
    {
      key: 'requestUrl',
      value: '',
      label: '请求地址',
      minWidth: 100,
      show: true,
      search: true,
    }, {
      key: 'method',
      value: '',
      label: '方法名称',
      minWidth: 100,
      show: true,
      search: true,
    }, {
      key: 'ip',
      value: '',
      label: '客户端IP地址',
      minWidth: 100,
      show: true,
      search: true,
    }, {
      key: 'location',
      value: '',
      label: '客户端IP归属地',
      minWidth: 100,
      show: true,
      search: true,
    }, {
      key: 'message',
      value: '',
      label: '返回信息',
      minWidth: 100,
      show: true,
    }, {
      key: 'success',
      value: '',
      label: '状态',
      width: 80,
      show: true,
      tableSlot: 'success',
    },
  ])
  const systemFieldList: Ref<ISearchForm[]> = ref([
    {
      key: 'packageName',
      value: '',
      label: '包名',
      show: true,
      search: true,
      minWidth: 120,
    },
    {
      key: 'traceId',
      value: '',
      label: '追踪编号',
      search: true,
      minWidth: 160,
      show: true,
    },
    {
      key: 'message',
      value: '',
      label: '日志内容',
      minWidth: 100,
      show: true,
    },
    {
      key: 'executeTime',
      value: '',
      label: '执行时间',
      minWidth: 120,
      show: true,
    }, {
      key: 'method',
      value: '',
      label: '方法名称',
      minWidth: 100,
      search: true,
      show: true,
    }, {
      key: 'exceptionStack',
      value: '',
      label: '异常堆栈',
      minWidth: 100,
      show: true,
    }, {
      key: 'operate',
      value: '',
      label: '操作',
      width: useOperateWidth(65, 72),
      showOverflowTooltip: false,
      show: true,
      tableSlot: 'operate',
      align: 'center',
    },
  ])
  return {
    loginFields,
    operationFieldList,
    systemFieldList,
  }
}
