/*
 * @Author       : wfl
 * @LastEditors: zqf
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-12-15 09:52:50
 * @LastEditTime: 2024-06-05 15:53:36
 */
import type { Ref } from 'vue'
import { EAlign, EFormType } from '@main/enums/search'
import type { ISearchForm } from '@main/enums/search'

interface ISex {
  id: string
  label: string
  value: string
}
export const useFields = () => {
  const defKeys = useTableKey()
  const fieldList: Ref<ISearchForm[]> = ref([
    {
      key: 'name',
      value: '',
      label: '部门名称',
      show: true,
      minWidth: 120,
      search: false,
    },
    {
      key: 'manager',
      value: '',
      label: '部门主管',
      type: EFormType.select,
      show: true,
      tableSlot: 'manager',
      search: false,
    },
    {
      key: 'phone',
      value: '',
      label: '联系电话',
      show: true,
      width: 120,
      search: false,
    },
    {
      key: 'userCount',
      value: '',
      label: '用户数量(人)',
      show: true,
      width: 110,
      align: EAlign.right,
      search: false,
      className: 'is-number',
    },
    ...defKeys,
  ])

  const tableKeys: Ref<ISearchForm[]> = ref([])
  const sexList: Ref<ISex[]> = ref([])
  const categoryList: any = ref([])
  if (!sexList?.value?.length) {
    const res: { SEX: ISex[], USER_LOCK_TYPE: any[], DEPT_CATEGORY: any[] }
      = ikStore.local.getItem(ELocal.DICTIONARY)
    sexList.value = res.SEX
    categoryList.value = res.DEPT_CATEGORY
  }
  const postIdList: Ref<any[]> = ref([])
  // 快捷获取岗位
  usePost().then((post: any) => (postIdList.value = post))
  const roleIdList: Ref<any[]> = ref([])
  // 快捷获取角色
  useRole().then((role: any) => (roleIdList.value = role))
  const typeList = [
    { label: '组织', value: 'UNITY' },
    { label: '部门', value: 'SECTION' },
  ]
  const equityList = [
    { label: '全资', value: 'WHOLLY_OWNED' },
    { label: '参股', value: 'PARTICIPATION' },
    { label: '控股', value: 'HOLDING' },
  ]
  const introductionList = [
    { label: '手动录入', value: 'MANUALLY' },
    { label: '外部链接', value: 'EXTERNAL_LINK' },
    { label: '内部路由', value: 'ROUTER' },
  ]
  const statusList = [
    { label: '启用', value: 'ENABLE' },
    { label: '禁用', value: 'DISABLE' },
  ]
  const userFieldList: Ref<ISearchForm[]> = ref([
    {
      key: 'name',
      label: '姓名',
      show: true,
      width: 160,
      tableSlot: 'name',
      search: true,
      showOverflowTooltip: false,
    },
    {
      key: 'sex',
      label: '性别',
      type: EFormType.select,
      options: sexList,
      show: true,
      width: 55,
      align: EAlign.center,
      tableSlot: 'sex',
      search: true,
    },
    {
      key: 'phone',
      label: '联系电话',
      maxlength: 11,
      show: true,
      width: 120,
      search: true,
    },
    {
      key: 'email',
      label: '邮箱',
      show: true,
      search: true,
    },
    {
      key: 'postId',
      label: '岗位',
      type: EFormType.treeselect,
      options: postIdList,
      show: true,
      tableSlot: 'post',
      search: true,
      showOverflowTooltip: false,
    },
    {
      key: 'roleId',
      label: '角色',
      show: true,
      type: EFormType.treeselect,
      options: roleIdList,
      tableSlot: 'role',
      search: true,
    },
    {
      key: 'locked',
      label: '锁定状态',
      show: true,
      width: 95,
      align: 'center',
      tableSlot: 'locked',
    },
    ...defKeys,
  ])
  return {
    fieldList,
    tableKeys,
    typeList,
    equityList,
    introductionList,
    statusList,
    sexList,
    userFieldList,
    categoryList,
  }
}
