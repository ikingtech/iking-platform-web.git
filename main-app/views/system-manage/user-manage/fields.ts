/*
 * @Author       : wfl
 * @LastEditors: zqf
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-12-15 09:52:50
 * @LastEditTime: 2024-04-22 10:08:13
 */
import type { Ref } from 'vue'
import { EAlign, EFormType } from '@main/enums/search'
import type { ISearchForm } from '@main/enums/search'

interface ISex {
  id: string
  label: string
  value: string
}
// 设置字段
export const useFields = () => {
  const tableKeys: Ref<ISearchForm[]> = ref([])
  const sexList: Ref<ISex[]> = ref([])
  const lockList: Ref<any[]> = ref([])
  const defKeys = useTableKey()

  if (!sexList?.value?.length || !lockList?.value?.length) {
    const res: { SEX: ISex[], USER_LOCK_TYPE: any[] } = ikStore.local.getItem(ELocal.DICTIONARY)
    sexList.value = res.SEX
    lockList.value = res.USER_LOCK_TYPE
  }

  const postIdList: Ref<any[]> = ref([])
  // 快捷获取岗位
  usePost().then((post: any) => (postIdList.value = post))

  const defaultExpandedKeys = ref()
  const deptIdList: Ref<any[]> = ref([])
  // 快捷获取部门
  useDep().then((dep: any) => {
    deptIdList.value = dep
    defaultExpandedKeys.value = [dep[0]?.id]
  })
  const roleIdList: Ref<any[]> = ref([])
  // 快捷获取角色
  useRole().then((role: any) => (roleIdList.value = role))

  const fieldList: Ref<ISearchForm[]> = ref([
    {
      key: 'deptId',
      label: '部门',
      show: false,
      width: 160,
      formSlot: 'deptId',
      search: true,
      showOverflowTooltip: false,
    },
    {
      key: 'name',
      label: '姓名',
      show: true,
      minWidth: 180,
      tableSlot: 'name',
      search: true,
      showOverflowTooltip: false,
    },
    {
      key: 'sex',
      label: '性别',
      type: EFormType.select,
      options: sexList,
      show: true,
      width: 80,
      align: EAlign.center,
      tableSlot: 'sex',
      search: true,
    },
    {
      key: 'phone',
      label: '联系电话',
      maxlength: 11,
      show: true,
      width: 200,
      search: true,
    },
    {
      key: 'email',
      label: '邮箱',
      show: true,
      search: true,
    },
    {
      key: 'postId',
      label: '岗位',
      type: EFormType.treeselect,
      options: postIdList,
      show: true,
      tableSlot: 'post',
      search: true,
      minWidth: 220,
      showOverflowTooltip: false,
    },
    {
      key: 'roleId',
      label: '角色',
      show: true,
      type: EFormType.treeselect,
      options: roleIdList,
      tableSlot: 'role',
      search: true,
      minWidth: 160,
    },
    {
      key: 'locked',
      label: '锁定状态',
      show: true,
      width: 120,
      align: 'center',
      tableSlot: 'locked',
    },
    ...defKeys,
    {
      key: '',
      label: '操作',
      show: true,
      width: useOperateWidth(148, 180),
      showOverflowTooltip: false,
      tableSlot: 'operate',
    },
  ])

  return {
    fieldList,
    sexList,
    postIdList,
    lockList,
    defaultExpandedKeys,
    deptIdList,
    tableKeys,
  }
}
