/*
 * @Author       : wfl
 * @LastEditors: zqf
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-12-15 09:52:50
 * @LastEditTime: 2024-03-29 16:37:11
 */
import type { Ref } from 'vue'
import { EAlign, EFormType } from '@main/enums/search'
import type { ISearchForm } from '@main/enums/search'

interface ISex {
  id: string
  label: string
  value: string
}
// 设置字段
export const useFields = (ENABLE_ORGANIZA: boolean | undefined) => {
  const tableKeys: Ref<ISearchForm[]> = ref([])
  const sexList: Ref<ISex[]> = ref([])
  const lockList: Ref<any[]> = ref([])
  const defKeys = useTableKey()

  if (!sexList?.value?.length || !lockList?.value?.length) {
    const res: { SEX: ISex[], USER_LOCK_TYPE: any[] } = ikStore.local.getItem(ELocal.DICTIONARY)
    sexList.value = res.SEX
    lockList.value = res.USER_LOCK_TYPE
  }

  const postIdList: Ref<any[]> = ref([])
  // 快捷获取岗位
  usePost().then((post: any) => (postIdList.value = post))

  const defaultExpandedKeys = ref()
  const deptIdList: Ref<any[]> = ref([])
  // 快捷获取部门
  useDep().then((dep: any) => {
    deptIdList.value = dep
    defaultExpandedKeys.value = [dep[0]?.id]
  })
  const roleIdList: Ref<any[]> = ref([])
  // 快捷获取角色
  useRole().then((role: any) => (roleIdList.value = role))
  const organizaFields = [
    // {
    //   key: 'orgId',
    //   label: '组织',
    //   show: false,
    //   formSlot: 'orgId',
    //   width: 160,
    //   showOverflowTooltip: false
    // },
    {
      key: 'deptId',
      label: '部门',
      show: false,
      width: 160,
      formSlot: 'deptId',
      search: true,
      showOverflowTooltip: false,
    },
  ]

  const fieldList: Ref<ISearchForm[]> = ref([
    ...(ENABLE_ORGANIZA ? organizaFields : []),
    {
      key: 'name',
      label: '姓名',
      show: true,
      width: 160,
      tableSlot: 'name',
      search: true,
      showOverflowTooltip: false,
    },
    {
      key: 'sex',
      label: '性别',
      type: EFormType.select,
      options: sexList,
      show: true,
      width: 55,
      align: EAlign.center,
      tableSlot: 'sex',
      search: true,
    },
    {
      key: 'phone',
      label: '联系电话',
      maxlength: 11,
      show: true,
      width: 120,
      search: true,
    },
    {
      key: 'email',
      label: '邮箱',
      show: true,
      search: true,
    },
    {
      key: 'postId',
      label: '岗位',
      type: EFormType.treeselect,
      options: postIdList,
      show: true,
      tableSlot: 'post',
      search: true,
      showOverflowTooltip: false,
    },
    {
      key: 'roleId',
      label: '角色',
      show: true,
      type: EFormType.treeselect,
      options: roleIdList,
      tableSlot: 'role',
      search: true,
    },
    {
      key: 'locked',
      label: '锁定状态',
      show: true,
      width: 95,
      align: 'center',
      tableSlot: 'locked',
    },
    ...defKeys,
    {
      key: '',
      label: '操作',
      show: true,
      width: useOperateWidth(148, 180),
      showOverflowTooltip: false,
      tableSlot: 'operate',
    },
  ])

  return {
    fieldList,
    sexList,
    postIdList,
    lockList,
    defaultExpandedKeys,
    deptIdList,
    tableKeys,
  }
}
