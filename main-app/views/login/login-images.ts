import bg from '@sys/config/assets/login/bg.webp'
import bgDark from '@sys/config/assets/login/bg_dark.webp'

import lc from '@sys/config/assets/login/lc.webp'
import lcDark from '@sys/config/assets/login/lc_dark.webp'

import dz from '@sys/config/assets/login/dz.webp'
import dzDark from '@sys/config/assets/login/dz_dark.webp'

import loginBg from '@sys/config/assets/login/login_bg.webp'
import loginBgDark from '@sys/config/assets/login/login_bg_dark.webp'

import mb from '@sys/config/assets/login/mb.webp'
import mbDark from '@sys/config/assets/login/mb_dark.webp'

import sj from '@sys/config/assets/login/sj.webp'
import sjDark from '@sys/config/assets/login/sj_dark.webp'

import sp from '@sys/config/assets/login/sp.webp'
import spDark from '@sys/config/assets/login/sp_dark.webp'

import xk from '@sys/config/assets/login/xk.webp'
import xkDark from '@sys/config/assets/login/xk_dark.webp'

import y from '@sys/config/assets/login/y.webp'
import yDark from '@sys/config/assets/login/y_dark.webp'

import yh from '@sys/config/assets/login/yh.webp'
import yhDark from '@sys/config/assets/login/yh_dark.webp'

import zm from '@sys/config/assets/login/zm.webp'
import zmDark from '@sys/config/assets/login/zm_dark.webp'

export default {
  bg,
  bgDark,
  lc,
  lcDark,
  dz,
  dzDark,
  loginBg,
  loginBgDark,
  mb,
  mbDark,
  sj,
  sjDark,
  sp,
  spDark,
  xk,
  xkDark,
  y,
  yDark,
  yh,
  yhDark,
  zm,
  zmDark,
}
