export interface tenant {
  policy?: string
  id?: string
  name?: string
  code?: string
  type?: string
  typeName?: string
  domain?: string
  startDateTime?: string
  endDateTime?: string
  favicon?: string
  faviconName?: string
  element?: string
  arrange?: string
  alignment?: string
  menuName?: string
  logo?: string
  icon?: string
  iconOpen?: string
  status?: string
  statusName?: string
  remark?: string
  sortOrder?: number | string
  delFlag?: boolean | string
  freeze?: boolean
  createBy?: string
  createName?: string
  updateBy?: string
  updateName?: string
  createTime?: string
  updateTime?: string
  normal?: boolean
  homePage?: string
}
