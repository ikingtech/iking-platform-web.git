/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-08-11 11:14:46
 * @LastEditTime : 2023-09-20 11:21:48
 */
// 登录方式
const loginType: Array<{
  icon: string
  name: string
  type: 'password' | 'phone' | 'wx' | 'dd'
}> = [
  {
    icon: 'iksvg_jiesuo',
    name: '密码登录',
    type: 'password',
  },
  {
    icon: 'iksvg_shouji',
    name: '短信登录',
    type: 'phone',
  },
  {
    icon: 'iksvg_gongnengdaohang',
    name: '钉钉登录',
    type: 'dd',
  },
  {
    icon: 'iksvg_erweima',
    name: '微信登录',
    type: 'wx',
  },
]

export default loginType
