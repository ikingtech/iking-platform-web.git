/*
 * @Author: qiye
 * @LastEditors: qiye
 * @description: page description
 * @Date: 2024-02-21 11:49:28
 * @LastEditTime: 2024-02-21 14:02:48
 */
import { pageManage as api } from '../api/manage-design'

const { msgError } = useMessage()

/**
 * 设置模板页面ID
 */
export const setTemplateDsPageId = (templates: any[], pageId: string) => {
  for (const item of templates) {
    (item.jsonData.formConfig.dataSources ?? []).forEach((ds: any) => {
      const url = ds.requestURL

      const searchIndex = url.indexOf('?')
      if (searchIndex > -1) {
        const searchStr = url.substring(searchIndex)
        const searchParams = new URLSearchParams(searchStr)
        searchParams.delete('page')
        searchParams.append('page', pageId)
        const searchString = searchParams.toString()
        ds.requestURL = `${url.substring(0, searchIndex)}?${searchString}`
      }
      else {
        //
        ds.requestURL = `${ds.requestURL}?page=${pageId}`
      }
    })
  }
}

/**
 * 加载模板
 */
export const listTemplate = async (type: string = 'PAGE', pageId: string) => {
  const ret = await api.listTemplateByType(type)
  if (!ret.success) {
    msgError(ret.msg)
    return
  }
  const templates = ret.data.map((item: any) => ({ title: item.title, description: item.description, jsonData: JSON.parse(item.jsonData) }))
  setTemplateDsPageId(templates, pageId)
  return templates
}
