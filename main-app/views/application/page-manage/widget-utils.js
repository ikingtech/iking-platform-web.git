/* eslint-disable array-callback-return */
/*
 * @Author: qiye
 * @LastEditors: qiye
 * @description: page description
 * @Date: 2023-11-15 17:07:29
 * @LastEditTime: 2023-11-15 17:09:19
 */
export function traverseAllWidgets(widgetList, handler) {
  if (!widgetList) {
    return
  }

  widgetList.map((w) => {
    handler(w)

    if (w.type === 'grid') {
      w.cols.map((col) => {
        handler(col)
        traverseAllWidgets(col.widgetList, handler)
      })
    }
    else if (w.type === 'table') {
      w.rows.map((row) => {
        row.cols.map((cell) => {
          handler(cell)
          traverseAllWidgets(cell.widgetList, handler)
        })
      })
    }
    else if (w.type === 'tab') {
      w.tabs.map((tab) => {
        traverseAllWidgets(tab.widgetList, handler)
      })
    }
    else if (w.type === 'sub-form' || w.type === 'grid-sub-form') {
      traverseAllWidgets(w.widgetList, handler)
    }
    else if (w.category === 'container') {
      // 自定义容器
      traverseAllWidgets(w.widgetList, handler)
    }
  })
}

/**
 * 获取表单列表
 * @param {*} widgetList
 * @returns
 */
export function filterForm(widgetList) {
  const result = []
  traverseAllWidgets(widgetList, (item) => {
    if (item.type === 'form') {
      result.push(item)
    }
  })
  return result
}
