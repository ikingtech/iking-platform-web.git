/*
 * @Author       : wfl
 * @LastEditors: qiye
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-12-15 09:52:50
 * @LastEditTime: 2023-11-27 15:19:15
 */
import type { ISearchForm } from '@main/enums/search'

export const useFields = async () => {
  const defKeys = useTableKey()
  // const typeList = ref([])
  // const { success, data } = await getType()
  // if (success) {
  //   typeList.value = data.map((item: any) => {
  //     return {
  //       ...item,
  //       label: item.description,
  //       value: item.type
  //     }
  //   })
  // }

  const fields: ISearchForm[] = [{
    key: 'name',
    value: '',
    label: '模型名称',
    show: true,
    minWidth: 100,
    search: true,
  }, {
    key: 'remark',
    value: '',
    label: '模型描述',
    show: true,
    minWidth: 100,
    search: true,
  }, {
    key: 'code',
    value: '',
    label: '模型标识',
    show: true,
    minWidth: 100,
    search: true,
  },
  {
    key: 'statusName',
    value: '',
    label: '状态',
    show: true,
    minWidth: 100,
    search: false,
  },
  {
    key: 'status',
    value: '',
    label: '状态',
    show: false,
    minWidth: 100,
    search: true,
    type: 'el-select',
    options: [
      { label: '未发布', value: 'UNPUBLISHED' },
      { label: '发布中', value: 'PUBLISHING' },
      { label: '已发布', value: 'PUBLISHED' },
    ],
  },
  ...defKeys,
  {
    key: '',
    label: '操作',
    show: true,
    width: 148,
    tableSlot: 'operate',
    fixed: 'right',
    showOverflowTooltip: false,
  },
  ]

  return fields
}
