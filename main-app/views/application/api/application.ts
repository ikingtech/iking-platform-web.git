/*
 * @Author: qiye
 * @LastEditors: zqf
 * @description: page description
 * @Date: 2023-12-11 12:28:54
 * @LastEditTime: 2024-05-09 15:36:11
 */
import { ikHttp, textHeader } from '@main/api/request'

export const api = {
  // 添加
  add: async (params: any) =>
    await ikHttp.post('/server/application/add', params),
  // 删除
  del: async (id: any) =>
    await ikHttp.post('/server/application/delete', id, textHeader),
  // 改
  update: async (params: any) =>
    await ikHttp.post('/server/application/update', params),
  // 查
  select: async (params: any) =>
    await ikHttp.post('/server/application/list/page', params),
  // 全量查
  selectAll: async () => await ikHttp.post('/server/application/list/all'),
  // 查询当前租户应用获取信息
  selectWithCurrentTenant: async (params: any) =>
    await ikHttp.post(
      '/server/application/application-assign/current-tenant',
      params,
    ),
  // 移除
  remove: async (id: any) =>
    await ikHttp.post('/server/application/remove', id, textHeader),
  // 详情
  details: async (id: any) =>
    await ikHttp.post('/server/application/detail/id', id, textHeader),
  // 获取应用
  acquire: async (id: any) =>
    await ikHttp.post('/server/application/acquire', id, textHeader),
  // 启用
  enable: async (id: any) =>
    await ikHttp.post('/server/application/enable', id, textHeader),
  // 停用
  disable: async (id: any) =>
    await ikHttp.post('/server/application/disable', id, textHeader),
  // 发布
  publish: async (id: any) =>
    await ikHttp.post('/server/application/publish', id, textHeader),
}

// 应用中心-页面管理
export const pageApi = {
  // 新增
  add: async (params: any) =>
    await ikHttp.post('/application/page/add', params),
  // 删除
  del: async (id: string) =>
    await ikHttp.post('/application/page/delete', id, textHeader),
  // 修改
  update: async (params: any) =>
    await ikHttp.post('/application/page/update', params),
  // 查询应用页面列表(应用编号)
  listPageByAppCode: async (appId: string) =>
    await ikHttp.post('/application/page/list/app-code', appId, textHeader),
  //
  details: async (id: string) =>
    await ikHttp.post('/application/page/detail', id, textHeader),
  // 更新主页面信息
  updateMainInfo: async (params: any) =>
    await ikHttp.post('/application/page/item/home-page/update', params),
  // 添加详情页面信息
  addDetailInfo: async (params: any) =>
    await ikHttp.post('/application/page/item/detail-page/add', params),
  // 更新详情页面信息
  updateDetailInfo: async (params: any) =>
    await ikHttp.post('/application/page/item/detail-page/update', params),
  // 删除详情页面信息
  deleteDetailInfo: async (pageId: string) =>
    await ikHttp.post(
      '/application/page/item/detail-page/delete',
      pageId,
      textHeader,
    ),
}
