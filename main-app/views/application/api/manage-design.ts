import { ikHttp, textHeader } from '@main/api/request'

export function getDependency(): Promise<ApiModel> {
  return ikHttp.get('/server/scaffold/project/dependency')
}
export function addProject(params: any): Promise<ApiModel> {
  return ikHttp.post('/server/scaffold/project', params)
}
export function editProject(params: any): Promise<ApiModel> {
  return ikHttp.put('/server/scaffold/project', params)
}
export function addTableDefinition(id: any): Promise<ApiModel> {
  return ikHttp.get(
    `/server/scaffold/module/table/definition?datasourceId=${id}`,
  )
}
export function allDatasource(): Promise<ApiModel> {
  return ikHttp.post('/server/datasource/list/all')
}
export function getfieldTypeList(): Promise<ApiModel> {
  return ikHttp.get('/server/scaffold/module/field/type')
}
export function getfieldList(params: any): Promise<ApiModel> {
  return ikHttp.get('/server/scaffold/module/table/field/definition', params)
}
export function addFunSave(params: any): Promise<ApiModel> {
  return ikHttp.post('/server/scaffold/module', params)
}
export function editFunSave(params: any): Promise<ApiModel> {
  return ikHttp.put('/server/scaffold/module', params)
}
export function getProject(params: any): Promise<ApiModel> {
  return ikHttp.get('/server/scaffold/project/page', params)
}
export function delProject(id: any): Promise<ApiModel> {
  return ikHttp.del(`/server/scaffold/project?projectId=${id}`)
}
export function getFun(params: any): Promise<ApiModel> {
  return ikHttp.get('/server/scaffold/module/page', params)
}
export function getForm(params: any): Promise<ApiModel> {
  return ikHttp.get('/server/scaffold/form/page', params)
}
export function addForm(params: any): Promise<ApiModel> {
  return ikHttp.post('/server/scaffold/form', params)
}
export function editForm(params: any): Promise<ApiModel> {
  return ikHttp.put('/server/scaffold/form', params)
}
export function addList(params: any): Promise<ApiModel> {
  return ikHttp.post('/server/scaffold/list', params)
}
export function editList(params: any): Promise<ApiModel> {
  return ikHttp.put('/server/scaffold/list', params)
}
export function projectDownload(params: any): Promise<ApiModel> {
  return ikHttp.get('/server/scaffold/project/download', params, {
    responseType: 'blob',
  })
}
export function funDownload(params: any): Promise<ApiModel> {
  return ikHttp.get('/server/scaffold/module/download', params, {
    responseType: 'blob',
  })
}
export function syncFun(params: any): Promise<ApiModel> {
  return ikHttp.get('/server/scaffold/module/sync', params)
}
export function deleteFun(id: any): Promise<ApiModel> {
  return ikHttp.del(`/server/scaffold/module?moduleId=${id}`)
}
export function deleteForm(id: any): Promise<ApiModel> {
  return ikHttp.del(`/server/scaffold/form?formId=${id}`)
}
export function deleteList(id: any): Promise<ApiModel> {
  return ikHttp.del(`/server/scaffold/list?listId=${id}`)
}
export function getType(): Promise<ApiModel> {
  return ikHttp.get('/server/scaffold/type')
}
export function getProjectById(id: any): Promise<ApiModel> {
  return ikHttp.get(`/server/scaffold/project/get?projectId=${id}`)
}
export function getFunById(id: any): Promise<ApiModel> {
  return ikHttp.get(`/server/scaffold/module/get?moduleId=${id}`)
}
export function getFormById(id: any): Promise<ApiModel> {
  return ikHttp.get(`/server/scaffold/form/get?formId=${id}`)
}
export function getListById(id: any): Promise<ApiModel> {
  return ikHttp.get(`/server/scaffold/list/get?listId=${id}`)
}
export function getProjectPreview(id: any): Promise<ApiModel> {
  return ikHttp.get(`/server/scaffold/project/preview?projectId=${id}`)
}
export function getModulePreview(id: any): Promise<ApiModel> {
  return ikHttp.get(`/server/scaffold/module/preview?moduleId=${id}`)
}
export function getModuleField(id: any): Promise<ApiModel> {
  return ikHttp.get(`/server/scaffold/module/table/field/all?moduleId=${id}`)
}
export function getInterfaces(id: any): Promise<ApiModel> {
  return ikHttp.get(`/server/scaffold/form/interfaces?moduleId=${id}`)
}
export function getJson(link: any): Promise<ApiModel> {
  return ikHttp.get(`/server/scaffold/form/menuLink?link=${link}`)
}
export function getFieldDefault(): Promise<ApiModel> {
  return ikHttp.get('/server/scaffold/module/table/field/default')
}
export function getJavaType(tableFieldType: string): Promise<ApiModel> {
  return ikHttp.get(
    `/server/scaffold/module/table/field/type?tableFieldType=${tableFieldType}`,
  )
}
export function getFieldIgnore(): Promise<ApiModel> {
  return ikHttp.get('/server/scaffold/module/table/field/length/ignore')
}
export function getSqlPreview(params: any): Promise<ApiModel> {
  return ikHttp.post('/server/scaffold/module/preview', params)
}
export function getFormId(): Promise<ApiModel> {
  return ikHttp.get('/server/scaffold/form/id')
}

/** 页面管理 */
export const pageManage = {
  // 新增
  add: (params: any): Promise<ApiModel> => ikHttp.post('/server/scaffold/page/add', params),
  // 删除
  del: (id: string): Promise<ApiModel> => ikHttp.post('/server/scaffold/page/delete', id, textHeader),
  // 修改
  update: (params: any): Promise<ApiModel> => ikHttp.post('/server/scaffold/page/update', params),
  // 查询
  select: (params: any): Promise<ApiModel> => ikHttp.post('/server/scaffold/page/list/page', params),

  // 发布
  publish: (params: any): Promise<ApiModel> => ikHttp.post('/server/scaffold/page/publish', params),
  // 取消发布
  cancelPublish: (id: string): Promise<ApiModel> => ikHttp.post('/server/scaffold/page/publish/cancel', id, textHeader),
  // 详情
  details: (id: string): Promise<ApiModel> => ikHttp.post('/server/application/page/detail', id, textHeader),
  // 模板列表
  listTemplate: (): Promise<ApiModel> => ikHttp.post('/server/application/page-template/list/all'),
  listTemplateByType: (type: string): Promise<ApiModel> => ikHttp.post('/application/page-template/list/type', type, textHeader),
  // 生成代码
  codeGen: (params: any): Promise<ApiModel> => ikHttp.post('/scaffold/page/code/gen', params, {
    responseType: 'blob',
  }),

}

/** 模型管理 */
export const modelManage = {
  // 新增
  add: (params: any): Promise<ApiModel> => ikHttp.post('/application/model/add', params),
  // 删除
  del: (id: string): Promise<ApiModel> => ikHttp.post('/application/model/delete', id, textHeader),
  // 修改
  update: (params: any): Promise<ApiModel> => ikHttp.post('/application/model/update', params),
  // 查询
  select: (params: any): Promise<ApiModel> => ikHttp.post('/application/model/list/page', params),
  // 全量查询
  selectAll: (showSlaveModelField?: boolean): Promise<ApiModel> => ikHttp.post(`/application/model/list/all?showSlaveModelField=${showSlaveModelField ?? ''}`),
  // 生成代码
  genCode: (params: any): Promise<ApiModel> => ikHttp.post('/application/model/code/gen', params, {
    responseType: 'blob',
  }),
  // 预览建表 sql
  sqlPreview: (params: any): Promise<ApiModel> => ikHttp.post('/application/model/create-table-sql/preview', params),
  // 全量查询数据源
  listAllDatasource: (): Promise<ApiModel> => ikHttp.post('/datasource/list/all'),
  // 查询数据源下对应的表格数据
  listByDatasourceId: (id: string): Promise<ApiModel> => ikHttp.post('/application/model/list/datasource-id', id, textHeader),
  // 模型发布
  publish: (id: string): Promise<ApiModel> => ikHttp.post('/application/model/publish', id, textHeader),
  // 取消模型发布
  publishCancel: (id: string): Promise<ApiModel> => ikHttp.post('/application/model/publish/cancel', id, textHeader),
}
