/*
 * @Author       : wfl
 * @LastEditors: zqf
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-03-13 09:53:04
 * @LastEditTime: 2024-03-14 14:49:24
 */
import type { Ref } from 'vue'
import type { ISearchForm } from '@main/enums/search'
import { EFormType } from '@main/enums/search'
import { tenantDict, tenantStatus } from './dict'

export const useFields = () => {
  const defKeys = useTableKey()
  const tableKeys: Ref<ISearchForm[]> = ref([])

  const fieldList: Ref<ISearchForm[]> = ref([
    {
      key: 'name',
      value: '',
      label: '租户名称',
      show: true,
      search: true,
      width: 120,
    },
    {
      key: 'code',
      value: '',
      label: '租户Code',
      show: true,
      width: 120,
      search: true,
    },
    {
      key: 'adminUsername',
      value: '',
      label: '管理员用户名',
      width: 120,
      show: true,
    },
    {
      key: 'type',
      value: '',
      label: '租户类别',
      type: EFormType.select,
      options: tenantDict,
      show: true,
      width: 170,
      tableSlot: 'type',
      search: true,
    },
    {
      key: 'homePage',
      value: '',
      label: '租户首页',
      show: true,
      width: 170,
    },
    {
      key: 'tenantDomain',
      value: '',
      label: '租户域名',
      show: true,
      search: true,
    },
    {
      key: 'startDate',
      value: '',
      label: '开始时间',
      width: 180,
      valueFormat: 'YYYY-MM-DD',
      type: EFormType.date,
      show: true,
    },
    {
      key: 'endDate',
      value: '',
      label: '结束时间',
      width: 180,
      valueFormat: 'YYYY-MM-DD',
      type: EFormType.date,
      show: true,
    },
    ...defKeys,
    {
      key: 'status',
      value: '',
      label: '租户状态',
      type: EFormType.select,
      options: tenantStatus,
      show: true,
      width: 150,
      tableSlot: 'status',
      search: true,
    },
    {
      key: '',
      value: '',
      label: '操作',
      show: true,
      width: useOperateWidth(148, 180),
      showOverflowTooltip: false,
      tableSlot: 'operate',
      fixed: 'right',
    },
  ])

  return {
    fieldList,
    tableKeys,
  }
}
