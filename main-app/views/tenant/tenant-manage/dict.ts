export const tenantDict = [
  {
    label: '普通租户',
    value: 'NORMAL',
  },
  {
    label: '主租户',
    value: 'MASTER',
  },
]
export const tenantStatus = [
  {
    label: '正常',
    value: 'NORMAL',
  },
  {
    label: '冻结',
    value: 'FREEZE',
  },
]

export const EStatus = {
  NORMAL: '正常',
  FREEZE: '冻结',
  DATA_MIGRATE_FAIL: '初始化失败',
  DATA_MIGRATING: '初始化中',
}
export const EType = {
  NORMAL: '普通租户',
  MASTER: '主租户',
}
export const TElement = [
  {
    label: 'ICON_NAME',
    name: '图标+名称',
  },
  {
    label: 'ICON',
    name: '仅图标',
  },
  {
    label: 'NAME',
    name: '仅名称',
  },
]
export const TArrange = [
  {
    label: 'LEFT_ICON_RIGHT_NAME',
    name: '左侧图标，右侧名称',
  },
  {
    label: 'LEFT_NAME_RIGHT_ICON',
    name: '左侧名称，右侧图标',
  },
]
export const TAlignment = [
  {
    label: 'LEFT',
    name: '左对齐',
    class: 'radio-icon',
  },
  {
    label: 'MIDDLE',
    name: '居中对齐',
    class: 'radio-icon radio-center',
  },
  {
    label: 'RIGHT',
    name: '右对齐',
    class: 'radio-icon radio-right',
  },
]
