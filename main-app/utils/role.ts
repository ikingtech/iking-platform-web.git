/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  : 返回是否有菜单或按钮权限
 * @updateInfo   :
 * @Date         : 2023-08-18 16:25:49
 * @LastEditTime : 2024-03-27 16:17:42
 */
import { ikStore } from 'iking-utils-pro'

// binding： 菜单或按钮的permissionCode
export const judgeRole = (binding: string) => {
  const btnList = ikStore.session.getItem(MenuStorageEnum.BUTTON)
  const btns = btnList?.map((b: any) => b?.permissionCode)
  const menuList = ikStore.session.getItem(MenuStorageEnum.MENU)?.map((v: any) => v.permissionCode) || []
  // code存在且不在权限时
  let flag = true
  if (!btns?.includes(binding) && binding && !menuList?.includes(binding)) {
    flag = false
  }

  return flag
}
