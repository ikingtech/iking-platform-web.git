/*
 * @Author: zqf
 * @Date: 2024-04-30 11:08:03
 * @email: 13289847921@163.com
 * @LastEditors: zqf
 * @LastEditTime: 2024-05-21 16:17:00
 * @Description:
 */
export const findDepUtilsGl = (deps: Array<any>) => {
  if (!Array.isArray) {
    useLog().error('findDepUtils参数不是数组，请修改参数')
    return
  }
  const uniqueDeps = deps.map((dep: any) => {
    return dep.deptType === 'SECTION' ? dep?.unity : dep
  })
  const seen = new Set()
  return uniqueDeps.filter((item: any) => {
    const k = item.deptId
    return seen.has(k) ? false : seen.add(k)
  })
}
