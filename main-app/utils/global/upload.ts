import { paramType } from 'iking-utils-pro'

/**
 * 解析上传响应结果。
 *
 * @param data 上传响应数据，可以是对象或字符串。
 * @param returnStr 当响应结果为单个对象且设置为true时，返回该对象的id作为字符串，否则返回整个对象或数组。
 * @returns 如果输入的是对象，返回该对象的id（如果存在）或整个对象；
 *          如果输入的是字符串，直接返回该字符串；
 *          如果输入的是数组，返回每个元素的response数据（如果存在）或整个元素，当且仅当数组长度为1且returnStr为true时，返回第一个元素的id（如果存在）或第一个元素本身。
 */
export const glParseUploadRes = (data: any, returnStr: boolean = false) => {
  // 如果输入是对象，直接返回对象的id或者整个对象
  if (paramType.isObject(data)) {
    return data?.id || data
  }
  // 如果输入是字符串，直接返回该字符串
  else if (paramType.isString(data)) {
    return data
  }
  // 处理输入是数组的情况，解析每个元素的response中的data字段，如果不存在则返回整个元素
  const res = data?.map((item: any) => {
    return item?.response?.data || item
  }) || []
  // 根据returnStr标志和数组长度决定返回值是整个数组还是数组的第一个元素的id或第一个元素本身
  return (res.length === 1 && returnStr) ? res?.[0]?.id || res?.[0] : res
}
