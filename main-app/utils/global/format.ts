export const glFormatPX = (val: number | string | undefined) => {
  return typeof val === 'number' ? `${val}px` : val
}
