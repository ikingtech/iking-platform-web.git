export const useSysConfig = () => {
  const homePage = ikStore.local.getItem(ELocal.TENANT)?.homePage || systemConfig.homePage
  return {
    homePage,
    config: systemConfig,
  }
}
