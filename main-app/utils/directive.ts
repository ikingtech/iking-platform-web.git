import type { App } from 'vue'
import mediumZoom from 'medium-zoom'
import { reginterDiective } from './reginter-diective'

export default function directive(app: App) {
  reginterDiective(app)
  app.directive('timeago', {
    mounted: (el, binding) => {
      el.textContent = useTimeago().format(binding.value)
      if (binding.modifiers.interval) {
        el.__timeagoSetInterval__ = setInterval(() => {
          el.textContent = useTimeago().format(binding.value)
        }, 1000)
      }
    },
    beforeUnmount: (el, binding) => {
      if (binding.modifiers.interval) {
        clearInterval(el.__timeagoSetInterval__)
      }
    },
  })
  // 注册 medium-zoom 指令
  app.directive('zoomable', {
    mounted: (el: any) => {
      mediumZoom(el, {
        background: 'var(--ik-bg)',
      })
    },
  })
}
