/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-02-29 14:41:18
 * @LastEditTime : 2024-04-10 14:13:45
 */
export function resolveRoutePath(basePath?: string, routePath?: string) {
  const path = basePath ? routePath?.startsWith(basePath) ? routePath : (`${basePath}${routePath}`) : routePath ?? ''
  return path
}

function isObject(value: any) {
  return typeof value === 'object' && !Array.isArray(value)
}
// 比较两个对象，并提取出不同的部分
export function getTwoObjectDiff(originalObj: Record<string, any>, diffObj: Record<string, any>) {
  if (!isObject(originalObj) || !isObject(diffObj)) {
    return diffObj
  }
  const diff: Record<string, any> = {}
  for (const key in diffObj) {
    const originalValue = originalObj[key]
    const diffValue = diffObj[key]
    if (JSON.stringify(originalValue) !== JSON.stringify(diffValue)) {
      if (isObject(originalValue) && isObject(diffValue)) {
        const nestedDiff = getTwoObjectDiff(originalValue, diffValue)
        if (Object.keys(nestedDiff).length > 0) {
          diff[key] = nestedDiff
        }
      }
      else {
        diff[key] = diffValue
      }
    }
  }
  return diff
}

/**
 * 把 src 属性的值赋给 tar 的同名属性
 * @param {*} tar 目标对象
 * @param {*} src 源对象
 */
export const copyValue = (tar: any, src: any) => {
  if (!paramType.isObject(tar) || !paramType.isObject(src)) {
    throw new TypeError('参数异常')
  }

  Object.keys(tar).forEach((key: string) => {
    if (Reflect.has(src, key)) {
      tar[key] = src[key]
    }
  })
}
