/*
 * @Author       : wfl
 * @LastEditors: wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-11-01 14:58:31
 * @LastEditTime: 2024-05-13 17:06:49
 */
import type { App } from 'vue'
import { IkBaseDialog, IkCollapseCard, IkDescriptions, IkEmpty, IkPreviewFile, IkRadioGroup, IkSelectTag, IkSideText, IkStatu, IkStatuOver, IkSvgIcon, IkTinymce, IkTree, IkUploadFile } from 'iking-web-ui-pro'

const { VITE_ENABLE_TINYMCE } = import.meta.env
export function registerComponent(app: App<Element>) {
  // 注册全局iking-web-ui-pro组件
  app.component('IkSvgIcon', IkSvgIcon)
  app.component('IkBaseDialog', IkBaseDialog)
  app.component('IkStatu', IkStatu)
  app.component('IkStatuOver', IkStatuOver)
  app.component('IkCollapseCard', IkCollapseCard)
  app.component('IkTree', IkTree)
  app.component('IkSideText', IkSideText)
  app.component('IkPreviewFile', IkPreviewFile)
  app.component('IkWebPreviewFile', IkPreviewFile)
  app.component('IkRadioGroup', IkRadioGroup)
  app.component('IkUploadFile', IkUploadFile)
  app.component('IkDescriptions', IkDescriptions)
  app.component('IkSelectTag', IkSelectTag)
  app.component('IkEmpty', IkEmpty)
  // 启用Tinymce组件 - 富文本
  VITE_ENABLE_TINYMCE && app.component('IkTinymce', IkTinymce)
}
