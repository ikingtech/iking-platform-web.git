/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-11-03 12:12:16
 * @LastEditTime : 2023-03-20 17:40:19
 */
import type { App } from 'vue'

export function reginterDiective(app: App) {
  const diects: any = import.meta.glob(['../directives/**/*.ts'], { eager: true })
  Object.keys(diects)?.forEach((d: string) => {
    app.directive(diects[d].default.name, diects[d].default)
  })
}
