import type { ComponentInternalInstance } from 'vue'
import { createGlobalState } from '@vueuse/core'

export default function useGlobalProperties() {
  const { appContext } = getCurrentInstance() as ComponentInternalInstance
  return appContext.config.globalProperties
}

export const useGlobalState = createGlobalState(() => {
  const VITE_GLOB_ONLYOFFICE_URL = ref('')
  const VITE_GLOB_ONLYOFFICE_FILE_URL = ref('')
  const VITE_GLOB_ONLYOFFICE_CALLBACK_URL = ref('')

  const setState = (state: any) => {
    VITE_GLOB_ONLYOFFICE_URL.value = state.VITE_GLOB_ONLYOFFICE_URL
    VITE_GLOB_ONLYOFFICE_FILE_URL.value = state.VITE_GLOB_ONLYOFFICE_FILE_URL
    VITE_GLOB_ONLYOFFICE_CALLBACK_URL.value = state.VITE_GLOB_ONLYOFFICE_CALLBACK_URL
  }

  return {
    VITE_GLOB_ONLYOFFICE_URL,
    VITE_GLOB_ONLYOFFICE_FILE_URL,
    VITE_GLOB_ONLYOFFICE_CALLBACK_URL,
    setState,
  }
})
