/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-02-29 14:41:18
 * @LastEditTime : 2024-03-14 10:43:23
 */
import useSettingsStore from '@main/store/modules/settings'
import useMenuStore from '@main/store/modules/menu'

export default function useMenu() {
  const router = useRouter()

  const settingsStore = useSettingsStore()
  const menuStore = useMenuStore()

  function switchTo(index: number | string, route?: any) {
    menuStore.setActived(index)
    if (useCheckMenuLevel(route)) {
      router.push(route.path)
    }
    else if (settingsStore.settings.menu.switchMainMenuAndPageJump) {
      router.push(menuStore.sidebarMenusFirstDeepestPath)
    }
  }

  return {
    switchTo,
  }
}
