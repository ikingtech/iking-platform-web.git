/*
 * @Author       : wfl
 * @LastEditors: ww
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-11-15 10:51:44
 * @LastEditTime: 2023-05-31 09:41:16
 */
export const usePages = (fn?: Function) => {
  const page = ref({
    // 总条数
    total: 0,
    // 每页条数
    rows: 10,
    // 当前页
    page: 1,
    //
    pageSizes: [10, 20, 30, 60, 90, 120],
  })

  const loading = ref(false)

  const handCurrentChange = (pg: number) => {
    page.value.page = pg
    fn?.()
  }

  const handSizeChange = (rw: number) => {
    page.value.rows = rw
    fn?.()
  }
  const handSearch = (data?: any) => {
    page.value.page = 1
    fn?.(data)
  }
  return {
    page,
    loading,
    handCurrentChange,
    handSizeChange,
    handSearch,
  }
}
