/*
 * @Author       : wfl
 * @LastEditors: ww
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-03-29 14:44:27
 * @LastEditTime: 2023-06-27 13:51:12
 */
import { ElMessage } from 'element-plus'
import { formApi } from '@main/api/modules/approve-center'

export async function useGroup(param: any) {
  const res = await formApi.getFormInfo(param)
  if (res.success) {
    // 组排序
    res.data = res.data.sort((a: any, b: any) => {
      // 组内表单排序
      if (a?.forms) {
        a.forms = a.forms.sort(
          (ca: any, cb: any) => ca.sortOrder - cb.sortOrder,
        )
      }
      return a.sortOrder - b.sortOrder
    })
  }
  else {
    ElMessage.error(res.msg)
  }
  return res
}

export function useGroupList() {
  const groupList: Ref<{ [key: string]: any }> = ref([])
  formApi.getFormInfo().then(({ success, data, msg }) => {
    groupList.value = success
      ? data.sort((a: any, b: any) => a.sortOrder - b.sortOrder)
      : []
    !success && ElMessage.error(msg)
  })
  return {
    groupList,
  }
}
