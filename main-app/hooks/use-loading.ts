/*
 * @Author: qiye
 * @LastEditors: qiye
 * @description: page description
 * @Date: 2023-08-03 13:54:04
 * @LastEditTime: 2023-08-03 13:56:34
 */
export const withLoading = async (
  fun: Function,
  loading: Ref,
) => {
  if (isRef(loading)) {
    try {
      loading.value = true
      return await fun()
    }
    finally {
      loading.value = false
    }
  }
}
