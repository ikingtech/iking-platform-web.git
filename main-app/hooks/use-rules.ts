/*
 * @Author       : wfl
 * @LastEditors: wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-11-03 10:24:39
 * @LastEditTime: 2023-04-18 14:06:28
 */
function checkValue(val: any) {
  return val !== null && val !== undefined && val !== ''
}

const validators = {
  number: (value: any, required: boolean, typeMsg: string, msg: string, cb: any) => {
    if (required) {
      if (!checkValue(value)) {
        cb(msg)
      }
      else if (typeof value !== 'number') {
        cb(typeMsg)
      }
      else { cb() }
    }
    else {
      if (checkValue(value) && typeof value !== 'number') {
        cb(typeMsg || msg)
      }
      else { cb() }
    }
  },
  phone: (value: any, required: boolean, typeMsg: string, msg: string, cb: any) => {
    if (required) {
      if (!checkValue(value)) {
        cb(msg)
      }
      else if (!ikValidate.phone.test(value) || value.startsWith('10') || value.startsWith('11') || value.startsWith('12')) {
        cb(typeMsg)
      }
      else {
        cb()
      }
    }
    else {
      if (checkValue(value) && !ikValidate.phone.test(value)) {
        cb(typeMsg || msg)
      }
      else { cb() }
    }
  },
  landline: (value: any, required: boolean, typeMsg: string, msg: string, cb: any) => {
    if (required) {
      if (!checkValue(value)) {
        cb(msg)
      }
      else if (!ikValidate.telPhone.test(value)) {
        cb(typeMsg)
      }
      else { cb() }
    }
    else {
      if (checkValue(value) && !ikValidate.telPhone.test(value)) {
        cb(typeMsg || msg)
      }
      else { cb() }
    }
  },
  telephone: (value: any, required: boolean, typeMsg: string, msg: string, cb: any) => {
    const flag = !ikValidate.phone.test(value) && !ikValidate.telPhone.test(value)
    if (required) {
      if (!checkValue(value)) {
        cb(msg)
      }
      else if (flag) {
        cb(typeMsg)
      }
      else { cb() }
    }
    else {
      if (checkValue(value) && flag) {
        cb(typeMsg || msg)
      }
      else { cb() }
    }
  },
  email: (value: any, required: boolean, typeMsg: string, msg: string, cb: any) => {
    if (required) {
      if (!checkValue(value)) {
        cb(msg)
      }
      else if (!ikValidate.emil.test(value)) {
        cb(typeMsg)
      }
      else { cb() }
    }
    else {
      if (checkValue(value) && !ikValidate.emil.test(value)) {
        cb(typeMsg || msg)
      }
      else { cb() }
    }
  },
}

export function useFormRules(rules: Array<{
  key: string
  required: boolean
  trigger: 'change' | 'blur' | 'focus' | 'select'
  msg: string
  // 数据校验不通过时信息
  typeMsg?: string
  type?: string | 'number' | 'phone' | 'email' | 'word' | 'password' | 'chinese'
  reg?: RegExp
  min?: number
  max?: number
  // 超出最大最小值时信息
  overMessage?: string
  overTrigger?: 'change' | 'blur' | 'focus' | 'select'
  validator?: (rule: any, value: any, callback: any) => void
}>) {
  const __ru: any = {}
  rules.forEach((rule: any) => {
    __ru[rule.key] = [{ ...rule }]
    if (rule.type) {
      switch (rule.type) {
        case 'number':
        case 'phone':
        case 'landline':
        case 'telephone':
        case 'email':
          __ru[rule.key][0].validator = (rule: any, value: any, cb: any) => {
            (validators as any)[rule.type](value, rule.required, rule.typeMsg, rule.msg, cb)
          }
          break
        default:
          break
      }
    }
    else {
      __ru[rule.key][0].message = rule.msg
      const _true = rule.min ?? false
      if (_true) {
        (__ru as any)[rule.key][0].message = rule.overMessage ?? rule?.message
      }
    }
  })

  return __ru
}
