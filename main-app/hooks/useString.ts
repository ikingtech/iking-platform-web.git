export function useString() {
  function toCamelCase(str: string): string {
    return str.replace(/_+(.)/g, (_, chr) => {
      return chr.toUpperCase()
    }).replace(/^./, (chr) => {
      return chr.toLowerCase()
    })
  }

  return {
    toCamelCase,
  }
}
