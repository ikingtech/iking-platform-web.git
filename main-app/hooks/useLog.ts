/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-03-06 10:05:02
 * @LastEditTime : 2024-03-14 16:25:12
 */
/* eslint-disable no-console */
const isDev = import.meta.env.DEV
export function useLog() {
  const log = (...data: any[]) => {
    isDev && console.log(...data)
  }

  const info = (...data: any[]) => {
    isDev && console.info(...data)
  }

  const warn = (...data: any[]) => {
    isDev && console.warn(...data)
  }

  const error = (...data: any[]) => {
    isDev && console.error(...data)
  }

  return {
    log,
    info,
    warn,
    error,
  }
}
