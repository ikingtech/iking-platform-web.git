/*
 * @Author       : wfl
 * @LastEditors: ww
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-03-02 11:56:10
 * @LastEditTime: 2023-06-27 12:08:19
 */
import { ElMessage, ElMessageBox } from 'element-plus'

export function useMessage() {
  // ElMessage
  const msgSuccess = (message: string, duration = 3000) => {
    ElMessage.closeAll()
    ElMessage.success({
      message,
      duration,
    })
  }
  const msgInfo = (message: string, duration = 3000) => {
    ElMessage.closeAll()
    ElMessage({
      message,
      duration,
    })
  }
  const msgWarning = (message: string, duration = 3000) => {
    ElMessage.closeAll()
    ElMessage({
      message,
      duration,
      type: 'warning',
    })
  }
  const msgError = (message: string, duration = 4500) => {
    ElMessage.closeAll()
    ElMessage({
      message,
      duration,
      type: 'error',
    })
  }

  // ElMessageBox
  const msgBoxSuccess = (title: string, msg: string, cb: Function, cbErr?: Function) => {
    ElMessageBox.confirm(
      msg,
      title,
      {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        draggable: true,
        customClass: 'ik-msg-box',
        type: 'success',
      },
    )
      .then(() => {
        cb()
      })
      .catch(() => {
        cbErr?.()
      })
  }
  const msgBoxInfo = () => (title: string, msg: string, cb: Function, cbErr?: Function) => {
    ElMessageBox.confirm(
      msg,
      title,
      {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        customClass: 'ik-msg-box',
        draggable: true,
      },
    )
      .then(() => {
        cb()
      })
      .catch(() => {
        cbErr?.()
      })
  }
  const msgBoxWarning = (title: string, msg: string, cb: Function, cbErr?: Function) => {
    ElMessageBox.confirm(
      msg,
      title,
      {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        customClass: 'ik-msg-box',
        draggable: true,
        type: 'warning',
      },
    )
      .then(() => {
        cb()
      })
      .catch(() => {
        cbErr?.()
      })
  }
  const msgBoxError = (title: string, msg: string, cb: Function, cbErr?: Function) => {
    ElMessageBox.confirm(
      msg,
      title,
      {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        customClass: 'ik-msg-box',
        draggable: true,
        type: 'error',
      },
    )
      .then(() => {
        cb()
      })
      .catch(() => {
        cbErr?.()
      })
  }

  // ElMessageBox.confirm
  const msgBoxSuccessAsync = (title: string, msg: string, cb: Function, cbErr?: Function) => {
    ElMessageBox.confirm(
      msg,
      title,
      {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        customClass: 'ik-msg-box',
        type: 'success',
        draggable: true,
        beforeClose: async (action, instance, done) => {
          if (action !== 'confirm') {
            done()
            return
          }
          instance.confirmButtonLoading = true
          cb().then(() => {
            done()
            instance.confirmButtonLoading = false
          })
        },
      },
    )
      .catch(() => {
        cbErr?.()
      })
  }
  /**
   * @description:
   * @param {string} title
   * @param {string} msg
   * @param {Function} cb
   * @param {Function} cbErr
   * @return {*}
   */
  const msgBoxWarnAsync = (title: string, msg: string, cb: Function, cbErr?: Function) => {
    ElMessageBox.confirm(
      msg,
      title,
      {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        customClass: 'ik-msg-box',
        type: 'warning',
        draggable: true,
        beforeClose: async (action, instance, done) => {
          if (action !== 'confirm') {
            done()
            return
          }
          instance.confirmButtonLoading = true
          cb().then(() => {
            done()
            instance.confirmButtonLoading = false
          })
        },
      },
    )
      .catch(() => {
        cbErr?.()
      })
  }
  /**
   * @description:
   * @param {string} title
   * @param {string} msg
   * @param {Function} cb
   * @param {Function} cbErr
   * @return {*}
   */
  const msgBoxInfoAsync = (title: string, msg: string, cb: Function, cbErr?: Function) => {
    ElMessageBox.confirm(
      msg,
      title,
      {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        customClass: 'ik-msg-box',
        draggable: true,
        type: 'info',
        beforeClose: async (action, instance, done) => {
          if (action !== 'confirm') {
            done()
            return
          }
          instance.confirmButtonLoading = true
          cb().then(() => {
            done()
            instance.confirmButtonLoading = false
          })
        },
      },
    )
      .catch(() => {
        cbErr?.()
      })
  }
  const msgBoxErrorAsync = (title: string, msg: string, cb: Function, cbErr?: Function) => {
    ElMessageBox.confirm(
      msg,
      title,
      {
        type: 'error',
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        customClass: 'ik-msg-box',
        draggable: true,
        beforeClose: async (action, instance, done) => {
          if (action !== 'confirm') {
            done()
            return
          }
          instance.confirmButtonLoading = true
          cb().then(() => {
            done()
            instance.confirmButtonLoading = false
          })
        },
      },
    )
      .catch(() => {
        cbErr?.()
      })
  }

  // 当需要用户输入内容时，可以使用 Prompt 类型的消息框
  /**
   * @description:
   * @param {string} title
   * @param {string} msg
   * @param {object} option inputValidator 来指定验证方法 inputErrorMessage，用来提示用户错误原因 inputPlaceholder
   * @param {Function} cb
   * @param {Function} cbErr
   * @return {*}
   */
  const msgBoxPromptAsync = (title: string, msg: string, option: { inputPattern?: RegExp, inputErrorMessage?: string, inputPlaceholder?: string } = {}, cb: Function, cbErr?: Function) => {
    ElMessageBox.prompt(
      msg,
      title,
      {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        customClass: 'ik-msg-box',
        draggable: true,
        ...option as any,
        beforeClose: async (action, instance, done) => {
          if (action !== 'confirm') {
            done()
            return
          }
          instance.confirmButtonLoading = true
          cb()?.then(() => {
            done()
            instance.confirmButtonLoading = false
          })
        },
      },
    )
      .catch(() => {
        cbErr?.()
      })
  }
  return {
    msgSuccess,
    msgInfo,
    msgWarning,
    msgError,

    msgBoxSuccess,
    msgBoxInfo,
    msgBoxWarning,
    msgBoxError,

    msgBoxSuccessAsync,
    msgBoxWarnAsync,
    msgBoxInfoAsync,
    msgBoxErrorAsync,

    msgBoxPromptAsync,
  }
}
