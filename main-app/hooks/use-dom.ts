/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-07-25 16:13:46
 * @LastEditTime : 2024-04-19 15:53:11
 */

/**
 * @description: 获取文本宽度
 * @param {string} text
 * @param {*} font 字体样式（一般只传入字体font-size值） 也可以是'14px sans-serif'等
 * @return {number}
 */
export const useTextWidth = (text: string, font = '14px') => {
  const canvas = document.createElement('canvas')
  const context = canvas.getContext('2d')
  if (!context) {
    return 0
  }

  context.font = font
  const metrics = context.measureText(text)
  return metrics.width
}

/**
 * 将指定的错误表单项滚动到视图中。
 * @param option 配置对象，可选参数，用于定制滚动行为。
 * @param option.behavior 滚动行为，默认为 'smooth'，可选值包括 'auto', 'instant', 'smooth'。
 * @param option.block 指定滚动到哪个块级元素，默认为 'end'，可选值包括 'start', 'end', 'nearest'。
 * @param option.inline 指定在行内方向上滚动到哪个位置，默认为 'nearest'，可选值包括 'start', 'end', 'nearest'。
 * @returns 无返回值。
 */
export const intoErrorView = (option: {
  behavior?: string
  block?: string
  inline?: string
} = {}) => {
  // 定义默认配置项
  const defaultOpts = {
    behavior: 'smooth',
    block: 'end',
    inline: 'nearest',
  }
  // 获取当前页面中第一个错误的表单项
  const dom = document.querySelector('.el-form-item.is-error')
  // 如果找到了错误的表单项，则将其滚动到视图中，合并使用了默认配置和用户自定义配置
  dom && dom.scrollIntoView({
    ...defaultOpts,
    ...option,
  } as any)
}
