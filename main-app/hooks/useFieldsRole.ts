/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-04-25 11:25:01
 * @LastEditTime : 2023-07-03 10:38:08
 */
import { EApproveCategory, ESelectRange } from '@main/views/approve-workflow/enum'

export const useFieldsRole = (props: Ref<any>) => {
  const showEmptyApprove = computed(() => {
    return props.value.initiatorSpecifiedScopeType === ESelectRange.按角色指定
      || [EApproveCategory.指定成员, EApproveCategory.指定角色, EApproveCategory.发起人部门主管, EApproveCategory.联系人控件关联成员, EApproveCategory.部门控件关联角色].includes(props.value.approvalCategory)
  })

  return {
    showEmptyApprove,
  }
}
