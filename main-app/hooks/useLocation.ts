export function getSearchParam(key: string) {
  const searchStr = window.location.search
  const objParams = new URLSearchParams(searchStr)
  return objParams.get(key)
}

// 从url中获取必要的请求头信息
export function setDomainHeader(headers: any) {
  const xTenant = getSearchParam('xTenant') || ikStore.local.getItem(ELocal.TENANT)?.code
  if (xTenant) {
    //
    headers['X-TENANT'] = xTenant
  }
  const menuId = ikStore.local.getItem(ELocal.MENU_ID)
  headers['menu-id'] = menuId

  const domain = getSearchParam('domain')
  if (domain) {
    headers['X-DOMAIN'] = domain
    const code = getSearchParam('code')
    if (domain === 'APPLICATION') {
      //
      headers['X-APP'] = code
    }
  }
}
