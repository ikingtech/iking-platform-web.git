/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-03-04 15:24:43
 * @LastEditTime : 2024-03-14 10:08:30
 */
export function useCheckMenuLevel(route: {
  [x: string]: string
  path: any
  children: any
  lenth: any
}) {
  return route?.path && (!route.children || !route.lenth) && (route.jumpType !== 'NONE')
}
