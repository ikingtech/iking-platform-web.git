const { loadUrl } = useHttpUrl()

const EStatus = {
  发起: 'INITIATE',
  等待: 'WAIT',
  同意: 'PASS',
  撤销: 'REVOKE',
  拒绝: 'REJECT',
  转交: 'TRANSFER',
  加签: 'APPEND',
  退回: 'BACK',
  评论: 'COMMENT',
  待执行: 'WAITING',
  执行中: 'RUNNING',
  结束: 'END',
}
export const ENodeType = {
  发起人节点: 'INITIATOR',
  审批节点: 'APPROVE',
  抄送节点: 'CARBON_COPY',
  办理节点: 'DISPOSE',
  分支节点: 'BRANCH',
  条件节点: 'CONDITION',
  结束节点: 'END',
}
export const setProcessNode = (dataList: any) => {
  const listData = {
    data: [],
    returnNode: [],
    downloadUrl: loadUrl,
  }
  if (!dataList?.instanceNodes) {
    dataList.instanceNodes = []
  }

  if (!dataList?.recordNodes) {
    dataList.recordNodes = []
  }

  for (const item of dataList?.recordNodes) {
    item.lineType = 'solid'
    item.username
      = (item.executorUsers && item.executorUsers[0]?.userName) || ''
  }
  dataList?.instanceNodes?.forEach((element: any, index: any) => {
    element.lineType = 'dashed'
    element.sortOrder = dataList?.recordNodes.length + index + 1
    element.username
      = (element.executorUsers && element.executorUsers[0]?.userName) || ''
    // if (index < 1) {
    element.status = EStatus.等待
    // }
    // element.statusName = '审批中'
  })

  listData.data = [...dataList?.recordNodes, ...dataList?.instanceNodes] as any
  listData.data = listData.data?.sort(
    (a: any, b: any) => a.sortOrder - b.sortOrder,
  )

  listData?.data?.map((item: any) => {
    if (item.backToRecordNodeId) {
      // 计算退回节点下标
      const obj: any = listData?.data?.find(
        (it: any) => it.id === item.backToRecordNodeId,
      )
      item.returnNodeName = obj.name
      const arr = [obj.sortOrder, item.sortOrder];
      (listData.returnNode as any).push(arr)
    }
    if (item.type === ENodeType.抄送节点) {
      // 计算抄送时，已读未读情况
      const readList = item.executorUsers.filter((it: any) => {
        return it.status === EStatus.结束
      })
      item.read = readList.length
      item.allRead = readList.length === item.executorUsers.length
    }
    // 图片附件列表
    item.attachments = item?.attachments?.map((it: any) => {
      return {
        ...it,
        id: it.id,
        originName: it.attachmentName,
        url: it.attachmentId, // formatUrl(it.attachmentId),
        suffix: it.attachmentSuffix,
        size: Number(it.attachmentSize),
      }
    })
    item.attachment = item?.attachments?.[0] || null
    item.attachmentImg = item?.images?.map((it: any) => {
      return it // formatUrl(it)
    })
    return {
      ...item,
    }
  })
  return listData
}
