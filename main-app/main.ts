/*
 * @Author       : wfl
 * @LastEditors: zqf
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-02-29 14:41:18
 * @LastEditTime: 2024-05-10 14:57:08
 */
import '@main/utils/system.copyright'

import FloatingVue from 'floating-vue'
import 'floating-vue/dist/style.css'

import VWave from 'v-wave'

import 'overlayscrollbars/styles/overlayscrollbars.css'
import { setupI18n } from '@as/locales'
import pinia from '@main/store'
import router from '@main/router'
import ui from '@main/ui-provider'
import { registerComponent } from '@main/utils/register-component'

// 自定义指令
import directive from '@main/utils/directive'

// 错误日志上报
import errorLog from '@main/utils/error.log'
import App from '@main/App.vue'

// 加载 svg 图标
import 'virtual:svg-icons-register'
import 'virtual:uno.css'
import 'iking-web-ui-pro/dist/style.css'

import ikingForm from 'iking-form-pro'
import 'iking-form-pro/dist/designer.style.css'

// 全局样式
import '@main/assets/styles/globals.scss'

// import '@main/assets/styles/application-develop.scss'

export default function startApp() {
  const app = createApp(App)
  app.use(FloatingVue, {
    distance: 12,
  })
  app.use(VWave, {})
  app.use(pinia)
  app.use(router)
  app.use(ui)
  app.use(setupI18n())
  registerComponent(app)
  directive(app)
  errorLog(app)
  app.use(ikingForm)

  // 全局关闭 Element Plus 的警告信息
  app.config.warnHandler = (msg) => {
    // 可以选择在这里记录警告信息，但不显示在控制台
    if (
      !/ElementPlus/.test(msg)
      // && !msg?.startsWith('Invalid pro')
      // && !msg?.startsWith('[plugin:vite')
      && !msg?.includes('has already been registered')
    ) {
      console.warn(`[Iking Admin警告拦截]: `, msg)
    }
  }

  app.config.errorHandler = (err) => {
    console.error(`[Iking Admin错误拦截]:`, err)
  }

  const mount = () => {
    app.mount('#app')

    // TODO 在Vue应用初始化时注册PerformanceObserver
    // const observer = new PerformanceObserver((list) => {
    //   const entries = list.getEntriesByType('resource');
    //   console.log('entries: ', entries);

    // });

    // observer.observe({ type: 'resource', buffered: true });
  }

  return {
    app,
    mount,
  }
}
