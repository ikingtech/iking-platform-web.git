/*
 * @Author: ww
 * @LastEditors: ww
 * @description:
 * @updateInfo:
 * @Date: 2023-05-19 14:26:22
 * @LastEditTime: 2023-06-29 13:46:00
 */
export const EStatus = {
  发起: 'INITIATE',
  等待: 'WAIT',
  同意: 'PASS',
  撤销: 'REVOKE',
  拒绝: 'REJECT',
  转交: 'TRANSFER',
  加签: 'APPEND',
  退回: 'BACK',
  评论: 'COMMENT',
  待执行: 'WAITING',
  执行中: 'RUNNING',
}
export const ENodeType = {
  发起人节点: 'INITIATOR',
  审批节点: 'APPROVE',
  抄送节点: 'CARBON_COPY',
  办理节点: 'DISPOSE',
  分支节点: 'BRANCH',
  条件节点: 'CONDITION',
  结束节点: 'END',
}
export const EFiledMap = {
  id: 'id',
  nodeId: 'sortOrder',
  nodeType: 'type',
  nodeTypeName: 'name',
  approveStatus: 'status',
  approveStatusName: 'statusName',
  // 发起人操作时间
  operationTime: 'createTime',
  approveType: 'multiExecutorType',
  approveTypeName: 'multiExecutorTypeName',
  approveOpinion: 'approveComment',
  countersignPer: 'appendExecutorUsers',
  originator: 'username',
  attachment: 'attachment',
  attachmentImg: 'attachmentImg',
  approvePersonList: 'executorUsers',
  isShowAdd: 'initiatorSpecify',
  lineType: 'lineType',
}

export const ApprovalBtnType = {
  passTo: '转交',
  endorse: '加签处理',
  sendBack: '退回处理',
  reject: '审批拒绝',
  agree: '审批同意',
}
export const ApprovalTypeLabel = {
  passTo: '转交理由',
  endorse: '加签理由',
  sendBack: '退回理由',
  reject: '拒绝理由',
  agree: '同意理由',
}
export const ApprovalTypePlaceholder = {
  passTo: '请输入转交理由',
  endorse: '请输入加签理由',
  sendBack: '请输入退回理由',
  reject: '请输入拒绝理由',
  agree: '请输入同意理由',
}
export const rejectReply = [
  '拒绝',
  '不同意',
  '取消',
  '作废',
  '重复提交',
  '未解决',
  '请核实',
  '补充材料',
  '时间不对',
]
export const aggreeReply = [
  '同意',
  '确认',
  'OK',
  '情况属实',
  '已审核',
  '好的',
  '通过',
  '已核实',
  '可以',
]
