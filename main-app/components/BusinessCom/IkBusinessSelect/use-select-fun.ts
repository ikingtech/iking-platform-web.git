/*
 * @Author: wfl
 * @LastEditors: wfl
 * @description:
 * @updateInfo:
 * @Date: 2023-04-14 14:11:04
 * @LastEditTime: 2023-04-14 14:25:03
 */
export const useSelect = (value: Ref<any>) => {
  const handCheck = (node: any, checkNode: { checkedKeys: any }) => {
    value.value = checkNode.checkedKeys
  }
  const handSelect = (node: any) => {
    value.value = node.id
  }

  return {
    handCheck,
    handSelect,
  }
}
