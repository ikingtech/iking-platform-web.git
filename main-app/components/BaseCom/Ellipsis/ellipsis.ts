/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-11-24 11:52:31
 * @LastEditTime : 2022-11-24 11:53:35
 */
import type { AllowedComponentProps, ComponentCustomProps, ComponentOptionsMixin, ComputedOptions, DefineComponent, ExtractPropTypes, MethodOptions, VNodeProps } from 'vue'
import Ellipsis from './Ellipsis.vue'

export default {
  install: (app: { component: (arg0: string, arg1: DefineComponent<{}, {}, any, ComputedOptions, MethodOptions, ComponentOptionsMixin, ComponentOptionsMixin, {}, string, VNodeProps & AllowedComponentProps & ComponentCustomProps, Readonly<ExtractPropTypes<{}>>, {}>) => void } | any) => {
    app.component('Ellipsis', Ellipsis)
  },
}
