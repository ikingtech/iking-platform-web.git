/*
 * @Author       : wfl
 * @LastEditors: ln
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-12-16 11:12:10
 * @LastEditTime: 2023-08-11 17:46:03
 */
export const props = {
  // 按钮类型
  type: {
    type: String as PropType<'primary' | 'success' | 'warning' | 'danger' | 'info' | 'text'>,
    default: 'primary',
  },
  // 按钮类型
  btnType: {
    type: String as PropType<'icon' | 'button'>,
    default: 'button',
  },
  // 默认是圆形按钮
  circle: {
    type: Boolean,
    default: true,
  },
  // 角色
  role: {
    type: String,
    default: '',
  },
}
