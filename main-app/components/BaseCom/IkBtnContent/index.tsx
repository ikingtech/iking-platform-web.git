/*
 * @Author       : wfl
 * @LastEditors: fj
 * @description  : 按钮包裹组件
 * @updateInfo   :
 * @Date         : 2022-11-02 14:57:18
 * @LastEditTime: 2023-09-08 17:37:26
 */
import { defineComponent } from 'vue'
import { MoreFilled } from '@element-plus/icons-vue'
import type { PropType } from 'vue'
import { SYSTEM_ICON_TYPE } from '@main/hooks/use-app'
import './index.scss'
import { systemConfig } from '@micro/config/system.config.ts'

const { tableMoreText } = systemConfig

const IkBtnContent = defineComponent({
  name: 'IkBtnContent',
  components: {
    MoreFilled,
  },
  props: {
    // 外部显示数量
    num: {
      type: Number,
      default: 3,
    },
    // 如何触发展开
    trigger: {
      type: String as PropType<'click' | 'hover'>,
      default: 'click',
    },
    // 展开位置
    placement: {
      type: String as PropType<'top' | 'top-start' | 'top-end' | 'bottom-start' | 'bottom-end' | 'bottom'>,
      default: 'bottom',
    },
  },
  setup(props, { slots }) {
    // 不在此解析slot dom; 会导致slot更新失效，虽然很多情况并不需要它更新，但这样写会导致Vue报一个警告。
    // const _slots = slots.default?.()
    const nodeSlot: Ref<any> = ref(null)
    const slotLen = ref(0)

    const filterSlot = (val: any) => {
      return val?.filter((sl: { children: any }) => typeof sl.children !== 'string')
    }

    const getSlotLen = () => {
      nodeSlot.value = filterSlot(slots.default?.())
      slotLen.value = nodeSlot.value?.length || 0
    }

    onUpdated(() => {
      getSlotLen()
    })

    watchEffect(() => {
      getSlotLen()
    })

    return () => (
      <div class="flex">
        {
          // 未超出限制按钮渲染
          filterSlot(slots.default?.())?.map((sl: any, index: number) => {
            return (index + 1) > props.num ? null : sl
          })
        }
        {
          // 超出限制按钮渲染
          slotLen.value > props.num
            ? (
              <el-dropdown trigger={props.trigger} popper-class="gl-btn-content">
                {{
                  default: () => (
                    <el-icon
                      title="更多"
                      class={['[ml-[12px]', 'more-operate', SYSTEM_ICON_TYPE === ETableIconType.文字 ? 'only-title' : '']}
                    >
                      {
                        tableMoreText || <MoreFilled />
                      }
                    </el-icon>
                  ),
                  dropdown: () => (
                    <el-dropdown-menu>
                      {
                        slotLen.value > props.num
                          ? filterSlot(slots.default?.())?.map((sl: any, index: number) => {
                            return index < props.num
                              ? null
                              : (
                                <el-dropdown-item>
                                  <p {...sl?.props} class={[sl?.props.type]} v-role={sl?.dirs?.[0]?.value}>
                                    {sl?.props?.title || sl?.props?.label}
                                  </p>
                                </el-dropdown-item>
                                )
                          })
                          : null
                      }
                    </el-dropdown-menu>
                  ),
                }}
              </el-dropdown>
              )
            : null
        }
      </div>
    )
  },
})
export default IkBtnContent
