/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-02-29 14:41:18
 * @LastEditTime : 2024-04-12 09:24:17
 */
import { defaultsDeep } from 'lodash-es'
import settingsDefault from '@main/settings.default'
import type { RecursiveRequired, Settings } from '#/global'

const globalSettings: any = {
  app: {
    enablePermission: true,
    enableDynamicTitle: true,
    enableErrorLog: true,
    theme: 'default',
    elementSize: 'default',
    defaultLang: 'zh-cn',
    storagePrefix: 'iking_',
    toggleAnimation: 'slide-fade',
  },
  layout: {
    enableMobileAdaptation: true,
  },
  menu: {
    menuFillStyle: 'radius',
    enableSubMenuCollapseButton: true,
    switchMainMenuAndPageJump: true,
    showTopMenuIcon: true,
    enableHotkeys: true,
    baseOn: 'backend',
    menuMode: 'head',
    isRounded: true,
  },
  topbar: {
    mode: 'fixed',
    switchTabbarAndToolbar: false,
  },
  tabbar: {
    enableHotkeys: true,
    style: 'card',
  },
  toolbar: {
    enableNotification: true,
    enableI18n: true,
    enableFullscreen: true,
    enablePageReload: true,
    enableColorScheme: true,
    enableAppSetting: true,
    favorites: true,
    notification: true,
    fullscreen: true,
    pageReload: true,
    colorScheme: true,
  },
  breadcrumb: {
    style: '',
    enableMainMenu: true,
    enable: true,
  },
  mainPage: {
    iframeCacheMax: 9,
    transitionMode: 'slide-top',
  },
  navSearch: {
    enable: true,
  },
  copyright: {
    enable: false,
    dates: '2020-2023',
    company: '成熟中台，首选金合，高效交付，赋能企业智能化未来！',
    website: 'http://www.ikingtech.com/',
  },
}

export default defaultsDeep(globalSettings, settingsDefault) as RecursiveRequired<Settings.all>
