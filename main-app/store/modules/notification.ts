/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-02-29 14:41:18
 * @LastEditTime : 2024-03-29 16:14:10
 */
const useNotificationStore = defineStore(
  // 唯一ID
  'notification',
  () => {
    ikNotification.checkPermission()
    // 消息
    const message = ref(0)
    // 待办
    const todo = ref(0)
    // 总计
    const total = computed(() => message.value + todo.value)

    function init() {
      getUnreadMessage()
      getUnreadTodo()
    }
    // 获取未读消息数
    function getUnreadMessage() {
      message.value = 0
    }
    // 获取未读待办数
    function getUnreadTodo() {
      todo.value = 0
    }

    // 设置未读数量
    function setCount(msgCount?: number, todoCount?: number) {
      if (typeof msgCount === 'number') {
        message.value = msgCount
      }
      if (typeof todoCount === 'number') {
        todo.value = todoCount
      }
    }

    return {
      message,
      todo,
      total,
      init,
      setCount,
    }
  },
)

export default useNotificationStore
