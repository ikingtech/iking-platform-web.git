/*
 * @Author       : wfl
 * @LastEditors: zqf
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-11-23 16:09:29
 * @LastEditTime: 2024-05-30 14:31:01
 */
import { defineStore } from 'pinia'
interface IAdvance {
  // 同一审批人在流程中多次出现时，自动去重
  approveDistinct?: boolean
  // 审批人和发起人是同一个人，审批自动通过
  sameApproveSponsorAutoPass?: boolean
  // 审批人去重类型可用值: KEEP_FIRST, CONTINUOUS_APPEARS_ONLY
  approveDistinctType?: string
  // 是否允许加签
  allowAddApprove?: boolean
  // 是否允许撤销已通过的审批
  allowRevokePassedApprove?: boolean
  // 允许提交人撤销xx天内已通过的审批单
  allowRevokeTime?: number
  // 是否允许撤销审批中的审批
  allowRevokeApproving?: boolean
  // 是否允许修改已通过的审批
  allowModifyPassedApprove?: boolean
  allowModifyTime?: number
  // 审批意见是否必填
  approveOpinionRequired?: string
  // 审批意见
  approveOpinion?: string
  // 评语是否仅管理员和审批人可见
  approveCommentInvisible?: string
  // 是否允许代他人提交
  allowSubmitByOther?: string
  // 待提交人员
  otherPerson?: any[]
  // 标题模板
  titleTemplate?: string
  // 摘要字段名称
  summaryFieldNames?: string
  // 提醒发送日期
  notifyDay?: string
  // 打印模板 ID
  reportTemplateId?: string
  isPrintTemplateEdit?: boolean
  approveFormConfigInvisible: boolean
  passedApproveRevokeExpire: number
  passedApproveModifyExpire: number
  allowAppendApprove: boolean
  triggerConditions: any[]
  approveCommentRequired: boolean
  approveCommentMissingTip: string
}

function buildDefaultAdvance(): IAdvance {
  return {
    // 表单配置是否仅管理员可见
    approveFormConfigInvisible: false,
    // 审批过程中是否可以添加人
    allowAppendApprove: true,
    // 工作流条件组信息
    triggerConditions: [],
    // 同一审批人在流程中多次出现时，自动去重
    approveDistinct: false,
    // 审批人和发起人是同一个人，审批自动通过
    sameApproveSponsorAutoPass: true,
    // 审批人去重类型可用值: KEEP_FIRST, CONTINUOUS_APPEARS_ONLY
    approveDistinctType: '',
    // 是否允许加签
    allowAddApprove: true,
    // 是否允许撤销已通过的审批
    allowRevokePassedApprove: false,
    // 允许提交人撤销xx天内已通过的审批单
    passedApproveRevokeExpire: 30,
    allowRevokeTime: 30,
    // 是否允许撤销审批中的审批
    allowRevokeApproving: true,
    // 是否允许修改已通过的审批
    allowModifyPassedApprove: false,
    passedApproveModifyExpire: 30,
    allowModifyTime: 30,
    // 审批意见是否必填
    approveOpinionRequired: '',
    approveCommentRequired: false,
    // 审批意见
    approveOpinion: '',
    approveCommentMissingTip: '',
    // 评语是否仅管理员和审批人可见
    approveCommentInvisible: '',
    // 是否允许代他人提交
    allowSubmitByOther: '',
    // 待提交人员
    otherPerson: [],
    // 标题模板
    titleTemplate: '',
    // 摘要字段名称
    summaryFieldNames: '',
    // 提醒发送日期
    notifyDay: '',

    // 打印模板 ID
    reportTemplateId: '',
    isPrintTemplateEdit: false,
  }
}

const DEFAULT_FORM = {
  formConfig: {
    modelName: 'formData',
    refName: 'vForm',
    rulesName: 'rules',
    labelWidth: 80,
    labelPosition: 'left',
    urlName: '',
    size: '',
    labelAlign: 'label-left-align',
    cssCode: '',
    customClass: '',
    functions: '',
    layoutType: 'PC',
    jsonVersion: 3,
    dataSources: [],
    onFormCreated: '',
    onFormMounted: '',
    onFormDataChange: '',
  },
  widgetList: [],
}

export const useFlowStore = defineStore('flow', {
  state: () => {
    return {
      nodeMap: new Map(),
      isEdit: null,
      selectedNode: {} as any,
      selectFormItem: null,
      design: {
        advance: buildDefaultAdvance(),
      } as {
        step: { currentStep: number }
        base: any
        formItems: any
        process: any
        advance: IAdvance
      } as any,
      designForm: DEFAULT_FORM as any,
    }
  },
  actions: {
    setSelectedNode(val: (val: any) => void) {
      this.selectedNode = val
    },
    loadForm(val: { formItems: any, [key: string]: any }) {
      this.design = val
      if (!val.advance) {
        this.design.advance = buildDefaultAdvance()
      }
    },
    setIsEdit(val: any = null) {
      this.isEdit = val
    },
    resetForm() {
      this.designForm = _.cloneDeep(DEFAULT_FORM)
      this.design.formItems = []
    },
    setForm(val: any) {
      this.designForm = val
      this.design.formItems = val.widgetList
    },
    setAdvance(val: any) {
      this.design.advance = { ...buildDefaultAdvance(), ...(val || {}) }
    },
  },
})
