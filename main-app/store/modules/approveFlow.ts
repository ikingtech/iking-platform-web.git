/*
 * @Author: ww
 * @LastEditors: ww
 * @description:
 */
import { defineStore } from 'pinia'

export const useApproveFlowStore = defineStore('approveFlow', {
  state: () => {
    return {
      showDetail: false,
      rowData: null as any,
      socketInfo: {} as {
        id: string
        tabId: string // EApproveListTab
        tabInnerId?: string
      },
    }
  },
  actions: {
    setShowDetail(val: boolean) {
      this.showDetail = val
    },
    setRowData(val: any) {
      this.rowData = val
    },
    setSocketInfo(val: any) {
      this.socketInfo = val
    },
  },
})

export default useApproveFlowStore
