/*
 * @Author       : wfl
 * @LastEditors: zqf
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-07-26 11:10:34
 * @LastEditTime: 2024-04-29 16:25:19
 */
interface ITemptype {
  businessKey: string
  businessName: string
}

const defaultConfig = {
  // ---
  receiver: null,
  sendTypes: [],
  systemMessageDefinition: {
    content: null,
    copyToMail: false,
    copyToShortMessage: false,
    redirectTo: null,
    voice: false,
    templateId: null,
  },
  wechatMiniMessageDefinition: {
    miniId: null,
    miniMessageTemplateContent: null,
    miniMessageTemplateId: null,
    miniName: null,
    paramDefinitionList: [],
    templateId: null,
  },
  contentType: '',
}

const defaultTemplateForm = {
  businessKey: '',
  businessName: '',
  messageTemplateKey: '',
  messageTemplateTitle: '',
  channelNames: '',
  receiverType: '',
}
const useNewsStore = defineStore(
  // 唯一ID
  'news',
  () => {
    const activeTemplate = ref<ITemptype | null>(null)
    const businessTypeList = ref<ITemptype[]>([])
    // 消息模板
    const templateForm = ref({ ...defaultTemplateForm })
    // 是否启用
    const isEnabled = ref(false)

    // 消息体定义
    const msgContentForm: Ref<any> = ref({ ...defaultConfig })

    const setIsEnabled = (val: any) => {
      isEnabled.value = val
    }

    const setActiveTemplate = (item: ITemptype) => {
      activeTemplate.value = item
    }

    const setBusinessType = (item: ITemptype[]) => {
      businessTypeList.value = item
    }

    const setTemplateForm = (item: any) => {
      templateForm.value = item || { ...defaultTemplateForm }
    }

    const setMsgContentForm = (item: any) => {
      msgContentForm.value
        = typeof item === 'boolean' ? { ...defaultConfig } : item
    }

    return {
      templateForm,
      setTemplateForm,
      activeTemplate,
      setActiveTemplate,
      businessTypeList,
      setBusinessType,
      msgContentForm,
      setMsgContentForm,
      isEnabled,
      setIsEnabled,
    }
  },
)

export default useNewsStore
