/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-03-06 11:43:58
 * @LastEditTime : 2023-03-06 11:44:45
 */
import type { TDate } from 'timeago.js'

const timeAgo = {
  name: 'time-ago',
  mounted: (el: any, binding: { value: TDate, modifiers: { interval: any } }) => {
    el.textContent = useTimeago().format(binding.value)
    if (binding.modifiers.interval) {
      el.__timeagoSetInterval__ = setInterval(() => {
        el.textContent = useTimeago().format(binding.value)
      }, 1000)
    }
  },
  beforeUnmount: (el: { __timeagoSetInterval__: string | number | NodeJS.Timeout | undefined }, binding: { modifiers: { interval: any } }) => {
    if (binding.modifiers.interval) {
      clearInterval(el.__timeagoSetInterval__)
    }
  },
}

export default timeAgo
