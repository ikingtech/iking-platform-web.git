/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-11-10 10:39:04
 * @LastEditTime : 2022-11-10 10:41:31
 */
// v-focus
const focus = {
  name: 'focus',
  mounted: (el: HTMLElement) => {
    el.focus()
  },
}

export default focus
