/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-11-03 12:09:01
 * @LastEditTime : 2022-11-03 13:42:46
 */
import Waves from 'node-waves'
import 'node-waves/dist/waves.min.css'
import type { DirectiveBinding } from 'vue'

/**
 * 使用示例
 *
 * v-waves
 *
 */

const waves = {
  name: 'waves',
  created: () => {
    Waves.init()
  },
  mounted: (el: Waves.ElementSelector, binding: DirectiveBinding<any>) => {
    const classes = ['button', 'circle', 'block', 'float', 'light', 'classic'].filter(cls => binding.modifiers[cls]).map(cls => `waves-${cls}`)
    Waves.attach(el, classes)
  },
}

// 必须有export default 才会自动注册
export default waves
