/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-11-03 13:44:19
 * @LastEditTime : 2023-06-06 15:22:48
 */

/**
 * 只允许输入数字
 *
 * 使用示例
 * const userForm = ref({
 *    phone: ''
 * })
 *
 * v-number="{form: userForm, key: 'phone'}"
 */
const reg = /\D+/
const number = {
  name: 'number',
  updated(el: any, binding: any) {
    if (!binding.value) {
      return
    }
    const vl = binding.value.form[binding.value.key]
    if (reg.test(vl)) {
      binding.value.form[binding.value.key] = vl && vl.replace(reg, '')
    }
  },
}

export default number
