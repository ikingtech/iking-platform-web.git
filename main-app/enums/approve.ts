/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-03-05 15:32:01
 * @LastEditTime : 2024-03-06 09:43:54
 */
/**
 * @description: tabId
 * @return {*}
 * @demo EApproveListTab.发起审批   EApproveListTab.我审批的 ...
 */
export enum EApproveListTab {
  发起审批 = 'InitiateApproval',
  我审批的 = 'IApproved',
  我提交的 = 'ISubmitted',
  抄送我的 = 'CopyToMe',
  分享给我 = 'ShareWithMe',
}

/**
 * @description: tabInnerId
 * @return {*}
 * @demo EApprovedType.待审列表   EApprovedType.已审列表
 */
export enum EApprovedType {
  待审列表 = 'Wait',
  已审列表 = 'Down',
}
