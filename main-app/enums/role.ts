export interface RoleFormState {
  name: string
  remark: string
  sortOrder: number
  dataScopeType: string
  parentId?: string
  id?: string
  dept?: string
  dataScopeTypeName?: string
  dataScopeCodeList?: string[]
  dataScopeCodes?: Array<string>
}

export interface MenuFormState {
  name: string
  menuType: string
  jumpType: string
  code: string
  icon: string | null
  logo: string
  remark: string
  visible: boolean
  keepAlive: boolean
  link: string
  parentId?: string
  id?: string
}
