/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2022-10-31 17:57:10
 * @LastEditTime : 2024-03-13 14:07:23
 */
export enum MenuStorageEnum {
  ALL = 'menu-all',
  MENU = 'menu',
  BUTTON = 'menu-button',
  TRANSLATE = 'translate',
}

export enum MenuBtnEnum {
  MENU = 'MENU',
  PAGE_TAB = 'PAGE_TAB',
  PAGE_BUTTON = 'PAGE_BUTTON',
  GLOBAL_BUTTON = 'GLOBAL_BUTTON',
}

export enum MenuJumpEnum {
  EXTERNAL_LINK = 'EXTERNAL_LINK',
  FRONT_ROUTE = 'FRONT_ROUTE',
  IFRAME = 'IFRAME',
  NONE = 'NONE',
}

export enum MenuViewType {
  BUSINESS = 'BUSINESS',
  MANAGE = 'MANAGE',
  MOBILE = 'MOBILE',
}

// 'side' | 'head' | 'single' | 'only-side' | 'only-head'
export enum EMenuModel {
  顶部精简 = 'only-head',
  侧边栏含主导航 = 'side',
  顶部 = 'head',
  侧边栏 = 'single',
  侧边栏精简 = 'only-side',
}
