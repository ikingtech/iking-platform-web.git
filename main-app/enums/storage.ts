/*
 * @Author       : wfl
 * @LastEditors: zqf
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-06-06 14:24:26
 * @LastEditTime: 2024-04-16 12:19:55
 */
export enum ELocal {
  // token
  TOKEN = 'token',
  // 图标
  IK_SVG = 'ik-svg',
  // 主题配置
  SETTING = 'setting',
  // 主题配置id
  SETTING_ID = 'setting_id',
  // tabbar
  TABBAR_PIN_DATA = 'tabbar_pin_data',
  // 用户名
  ACCOUNT = 'account',
  // 用户信息
  USER_INFO = 'user_info',
  // 失效时间
  FAILURE_TIME = 'failure_time',
  // 字典
  DICTIONARY = 'dictionary',
  // 修改密码弹框状态
  PWD_VISIBLE = 'pwd_visible',
  // tabbar滚动提示
  TABBAR_SCROLL_TIP = 'tabbar_scroll_tip',
  // tabbar超出数量提示
  TABBAR_MAXIMIZE_TIP = 'tabbar_maximize_tip',
  // 租户
  TENANT = 'tenant',
  // 记住密码
  REMBER = 'rember',
  // 组织信息，当前已选  组织列表
  ORGANIZA = 'organiza',
  // 页面灰度值
  GRRY = 'grry',
  // menuId
  MENU_ID = 'menuid',
  // 系统版本
  version = 'open',
}
